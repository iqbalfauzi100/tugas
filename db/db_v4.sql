-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 30 Jul 2018 pada 15.25
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_etugas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `user` tinytext NOT NULL,
  `pass` tinytext NOT NULL,
  `nama` tinytext,
  `tingkat` tinytext NOT NULL,
  `pelajaran` tinytext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `user`, `pass`, `nama`, `tingkat`, `pelajaran`) VALUES
(1, 'admin', 'admin', 'Admin', '3', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `idLevel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `username`, `password`, `idLevel`) VALUES
(1, 'admin', 'admin', 1),
(2, 'tugasu', 'tugasu', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE `fakultas` (
  `id` int(11) NOT NULL,
  `namaFk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`id`, `namaFk`) VALUES
(1, 'Tarbiyah dan Ilmu Keguruan'),
(2, 'Syari’ah'),
(3, 'Humaniora'),
(4, 'Psikologi'),
(5, 'Ekonomi'),
(6, 'Sains dan Teknologi'),
(7, 'Kedokteran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `username` tinytext NOT NULL,
  `nama` tinytext NOT NULL,
  `pass` text NOT NULL,
  `id_kelompok` int(12) NOT NULL,
  `foto` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `username`, `nama`, `pass`, `id_kelompok`, `foto`) VALUES
(63, 'munir', 'Misbahul Munir', 'munir', 2, 'Selection_002.png'),
(62, 'fahmi', 'Muhammad Fahmi', 'fahmi', 1, 'Selection_001.png'),
(64, 'mahbub', 'Muhammad Zainul Mahbubi', 'mahbub', 3, 'Selection_003.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hp`
--

CREATE TABLE `hp` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hp`
--

INSERT INTO `hp` (`id`, `nama`, `no`) VALUES
(1, 'Fahmi', '085814294695'),
(2, 'Iqbal', '085231633962'),
(3, 'Muslim', '085735307194');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `jurusan` text NOT NULL,
  `idFk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `jurusan`, `idFk`) VALUES
(1, 'Matematika', 6),
(2, 'Biologi', 6),
(3, 'Ahwalul Syahsiyah', 2),
(4, 'Hukum Bisnis Syariah', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok`
--

CREATE TABLE `kelompok` (
  `id_kelompok` int(3) NOT NULL,
  `kelompok` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelompok`
--

INSERT INTO `kelompok` (`id_kelompok`, `kelompok`) VALUES
(1, '1. Kucing'),
(2, '2. Kuda'),
(3, '3. Kambing'),
(4, '4. Gajah'),
(5, '5. Kerbau'),
(6, '6. Banteng');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kotak_masuk`
--

CREATE TABLE `kotak_masuk` (
  `id_kotakmasuk` int(11) NOT NULL,
  `id_tugas` int(11) NOT NULL,
  `m_file` text,
  `nis` text,
  `nama_siswa` text,
  `id_kelompok` int(12) DEFAULT NULL,
  `jurusan` int(30) NOT NULL,
  `tahun_angkatan` varchar(30) NOT NULL,
  `nama_guru` text,
  `keterangan` int(3) NOT NULL COMMENT '0 = Belum_verifikasi ; 1 = verifikasi',
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `judul` text NOT NULL,
  `judul_tugas` text NOT NULL,
  `pokok_1` text NOT NULL,
  `nilai` int(3) NOT NULL COMMENT 'Nilai antara 1-100'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kotak_masuk`
--

INSERT INTO `kotak_masuk` (`id_kotakmasuk`, `id_tugas`, `m_file`, `nis`, `nama_siswa`, `id_kelompok`, `jurusan`, `tahun_angkatan`, `nama_guru`, `keterangan`, `tanggal`, `judul`, `judul_tugas`, `pokok_1`, `nilai`) VALUES
(126, 42, '', '1', 'Muhammad Dian Agustina', 1, 0, '2017', '62', 1, '2018-07-26 06:45:08', '', 'Indonesia', '<p>ini adalah itu</p>\r\n', 24),
(127, 43, '', '1', 'Muhammad Dian Agustina', 1, 0, '2017', '62', 1, '2018-07-26 06:46:13', '', 'Rangkuman Pondok Pesantren', '<p>contoh harga</p>\r\n', 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id`, `level`) VALUES
(1, 'Admin'),
(2, 'Penugasan Universitas'),
(3, 'Pendamping Universitas'),
(4, 'Penugasan Fakultas'),
(5, 'Pendamping Fakultas'),
(6, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Stand-in structure for view `nilai_mahasiswa`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `nilai_mahasiswa` (
`nis` text
,`niss` text
,`jurusan` tinytext
,`nama` tinytext
,`id_kelompok` int(3)
,`kelompok` varchar(20)
,`id_tugas` int(11)
,`judul` text
,`nilai` int(3)
,`id_pendamping` int(11)
,`nama_pendamping` tinytext
,`total_nilai` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penugasan`
--

CREATE TABLE `penugasan` (
  `id` int(11) NOT NULL,
  `user` tinytext NOT NULL,
  `pass` tinytext NOT NULL,
  `nama` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penugasan`
--

INSERT INTO `penugasan` (`id`, `user`, `pass`, `nama`) VALUES
(1, 'tugas', 'tugas', 'Fahudi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting_sistem`
--

CREATE TABLE `setting_sistem` (
  `id` int(11) NOT NULL,
  `fitur` text NOT NULL,
  `status` int(3) NOT NULL COMMENT '0 = Tidak Aktif ; 1 = Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setting_sistem`
--

INSERT INTO `setting_sistem` (`id`, `fitur`, `status`) VALUES
(1, 'Tambah Anggota Kelompok (Laman Pendamping)', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `nis` text,
  `nama` tinytext NOT NULL,
  `jurusan` tinytext NOT NULL,
  `tahunajaran` tinytext,
  `jenis_kelamin` tinytext,
  `foto` text,
  `password` text,
  `id_kelompok` int(12) NOT NULL,
  `id_pendamping` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `nis`, `nama`, `jurusan`, `tahunajaran`, `jenis_kelamin`, `foto`, `password`, `id_kelompok`, `id_pendamping`) VALUES
(102, '1', 'Muhammad Dian Agustina', 'Biologi', '2017', 'Perempuan', 'Selection_003.png', 'dian', 1, 62),
(103, '2', 'muslim', 'Biologi', '2017', 'Perempuan', 'Selection_002.png', 'muslim', 2, 63),
(108, '4', 'Denny', 'Biologi', '2017', 'Perempuan', 'Selection_003.png', 'denny', 1, 62),
(107, '3', 'achmad', 'Biologi', '2017', 'Perempuan', 'Selection_002.png', 'achmad', 2, 63);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id_tahun` int(3) NOT NULL,
  `tahun` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id_tahun`, `tahun`) VALUES
(1, '2017'),
(3, '2018');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tugas`
--

CREATE TABLE `tbl_tugas` (
  `id_tugas` int(23) NOT NULL,
  `judul` text,
  `nama_guru` text,
  `id_kelompok` text,
  `tgl` date DEFAULT NULL,
  `m_file` text,
  `berlaku` datetime DEFAULT NULL,
  `nilai` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_tugas`
--

INSERT INTO `tbl_tugas` (`id_tugas`, `judul`, `nama_guru`, `id_kelompok`, `tgl`, `m_file`, `berlaku`, `nilai`) VALUES
(40, 'Pancasila', NULL, NULL, '2018-07-09', 'beasiswa.pdf', '2018-07-31 08:59:00', 0),
(41, 'Beasiswa', NULL, NULL, '2018-07-09', 'topsis.pdf', '2018-07-31 08:50:00', 0),
(42, 'Indonesia', NULL, NULL, '2018-07-09', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '2018-07-31 20:00:00', 24),
(43, 'Rangkuman Pondok Pesantren', NULL, NULL, '2018-07-24', '88360-ID-sistem-pendukung-keputusan-untuk-menentu.pdf', '2018-07-31 00:00:00', 12);

-- --------------------------------------------------------

--
-- Struktur untuk view `nilai_mahasiswa`
--
DROP TABLE IF EXISTS `nilai_mahasiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nilai_mahasiswa`  AS  select `siswa`.`nis` AS `nis`,`siswa`.`nis` AS `niss`,`siswa`.`jurusan` AS `jurusan`,`siswa`.`nama` AS `nama`,`kelompok`.`id_kelompok` AS `id_kelompok`,`kelompok`.`kelompok` AS `kelompok`,`kotak_masuk`.`id_tugas` AS `id_tugas`,`kotak_masuk`.`judul_tugas` AS `judul`,`kotak_masuk`.`nilai` AS `nilai`,`guru`.`id_guru` AS `id_pendamping`,`guru`.`nama` AS `nama_pendamping`,(select sum(`kotak_masuk`.`nilai`) from (`kotak_masuk` left join `siswa` on((`kotak_masuk`.`nis` = `siswa`.`nis`))) where (`kotak_masuk`.`nis` = `niss`)) AS `total_nilai` from (((`siswa` left join `kelompok` on((`siswa`.`id_kelompok` = `kelompok`.`id_kelompok`))) left join `kotak_masuk` on((`siswa`.`nis` = `kotak_masuk`.`nis`))) left join `guru` on((`guru`.`id_guru` = `kotak_masuk`.`nama_guru`))) order by `siswa`.`nis` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idLevel` (`idLevel`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `hp`
--
ALTER TABLE `hp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`),
  ADD KEY `idFk` (`idFk`);

--
-- Indexes for table `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indexes for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  ADD PRIMARY KEY (`id_kotakmasuk`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penugasan`
--
ALTER TABLE `penugasan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_sistem`
--
ALTER TABLE `setting_sistem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  ADD PRIMARY KEY (`id_tugas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `hp`
--
ALTER TABLE `hp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `id_kelompok` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  MODIFY `id_kotakmasuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `penugasan`
--
ALTER TABLE `penugasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting_sistem`
--
ALTER TABLE `setting_sistem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id_tahun` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  MODIFY `id_tugas` int(23) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD CONSTRAINT `akses_ibfk_1` FOREIGN KEY (`idLevel`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `jurusan_ibfk_1` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
