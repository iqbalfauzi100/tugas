-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 10 Agu 2018 pada 00.51
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_etugas`
--

-- --------------------------------------------------------

--
-- Struktur untuk view `nilai_mahasiswa`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `nilai_mahasiswa`  AS  select `kotak_masuk`.`nis` AS `nis`,`kotak_masuk`.`nis` AS `niss`,`kotak_masuk`.`nama_siswa` AS `nama_mhs`,`kotak_masuk`.`id_tugas` AS `id_tugas`,`kotak_masuk`.`judul_tugas` AS `judul_tugas`,`kotak_masuk`.`keterangan` AS `keterangan`,`kotak_masuk`.`nilai` AS `nilai`,`kotak_masuk`.`status_tugas` AS `status_tugas`,`kotak_masuk`.`jurusan` AS `idJurusan`,`jurusan`.`jurusan` AS `jurusan`,`kelompok`.`id_kelompok` AS `idKelompokUniv`,`kelompok`.`kelompok` AS `namaKelompokUniv`,`kelompok_fk`.`id` AS `idKelompokFk`,`kelompok_fk`.`namaKelompok` AS `namaKelompokFk`,`pendamping_univ`.`id` AS `idPendampingUniv`,`pendamping_univ`.`nama` AS `namaPendampingUniv`,`pendamping_fk`.`id` AS `idPendampingFk`,`pendamping_fk`.`nama` AS `namaPendampingFk`,(select sum(`kotak_masuk`.`nilai`) from (`kotak_masuk` left join `mahasiswa` on((`kotak_masuk`.`nis` = `mahasiswa`.`nim`))) where (`kotak_masuk`.`nis` = `niss`)) AS `total_nilai` from (((((`kotak_masuk` left join `jurusan` on((`kotak_masuk`.`jurusan` = `jurusan`.`id_jurusan`))) left join `kelompok` on((`kotak_masuk`.`id_kelompok` = `kelompok`.`id_kelompok`))) left join `kelompok_fk` on((`kotak_masuk`.`idKelompokFk` = `kelompok_fk`.`id`))) left join `pendamping_univ` on((`kotak_masuk`.`nama_guru` = `pendamping_univ`.`id`))) left join `pendamping_fk` on((`kotak_masuk`.`idPendampungFk` = `pendamping_fk`.`id`))) ;

--
-- VIEW  `nilai_mahasiswa`
-- Data: Tidak ada
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
