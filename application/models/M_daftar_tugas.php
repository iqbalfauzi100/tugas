<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_daftar_tugas extends CI_Model {
  var $table = "tbl_tugas";
  var $select_column = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan");
  
  var $order_column = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan",null);
  var $column_search = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan");
  var $default_order = "tbl_tugas.id_tugas";
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function make_query()
  { 
    $id_sess = $this->session->userdata('username');
    $sql = "SELECT jurusan.idFk FROM mahasiswa,jurusan,akses WHERE mahasiswa.idJurusan=jurusan.id_jurusan AND akses.username=mahasiswa.nim AND akses.username='$id_sess'";
    $getData = $this->db->query($sql)->row();
    $idFk = $getData->idFk;
    $relation = 'tbl_tugas.keterangan = 0 or tbl_tugas.keterangan='.$idFk.'';

    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->where($relation);
    $this->db->order_by('id_tugas','DESC');

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }
  function make_datatables(){
    $this->make_query();
    if($_POST['length'] != -1){
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id_bea($id)
  {
    $this->db->from($this->table);
    $this->db->where('bea.id',$id);
    $query = $this->db->get();
    return $query->row();
  }
  public function get_scoring()
  {
    $this->db->select('*');
    $this->db->from('kategori_skor');
    $query = $this->db->get();
    return $query->result();
  }
  public function save_bea($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }
  public function save_sub_bea($data)
  {
    $this->db->insert_batch('set_bea_kategori_skor', $data);
    return $this->db->insert_id();
  }
  public function get_skor_by_idBea($id)
  {
    $this->db->select('set_bea_kategori_skor.idKategoriSkor, set_bea_kategori_skor.id');
    $this->db->from('set_bea_kategori_skor');
    $this->db->where('set_bea_kategori_skor.idBea',$id);
    $query = $this->db->get();
    return $query->result();
  }
  public function update_setting_bea($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }
  public function update_setting_sub_bea($where, $data)
  {
    $this->db->update('set_bea_kategori_skor', $data, $where);
    return $this->db->affected_rows();
  }
  public function insert_setting_sub_bea($data)
  {
    $this->db->insert('set_bea_kategori_skor', $data);
    return $this->db->insert_id();
  }
  public function delete_setting_sub_bea($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('set_bea_kategori_skor');
  }
  public function delete_by_id($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    $this->db->where('idBea', $id);
    $this->db->delete('set_bea_kategori_skor');
  }
  public function get_id_tugas($id)
  {
    $nim  = $this->session->userdata('username');

    $this->db->select('id_tugas');    
    $this->db->from('kotak_masuk');
    $this->db->where('kotak_masuk.id_tugas',$id);
    $this->db->where('kotak_masuk.nis',$nim);
    return $this->db->count_all_results();
  }
  public function get_by_id_daftar_tugas($id)
  {
    $this->db->from($this->table);
    $this->db->where('tbl_tugas.id_tugas',$id);
    $query = $this->db->get();
    return $query->row();
  }
  public function dataFakultas() {
    $data = $this->db->query("SELECT * from fakultas");
    if ($data) {
      return $data->result();
    } else {
      return false;
    }
  }
  function get_jurusan($fakultas) {
    $getjur ="select j.id_jurusan,j.jurusan,f.namaFk from jurusan j,fakultas f where j.idFk=f.id and f.id='$fakultas' order by j.id_jurusan asc";
    return $this->db->query($getjur)->result();
  }

  public function dataIdentitas($key)
  {
    $this->db->from('mahasiswa');
    $this->db->where('mahasiswa.nim',$key);
    $query = $this->db->get();
    return $query->row();
  }
  public function getInsert($data) {
    $this->db->insert('akses',$data);
    return $this->db->insert_id();
  }

  public function insertMahasiswa($data) {
    $this->db->insert('mahasiswa', $data);
    return $this->db->insert_id();
  }
  public function insertPenugasanFk($data) {
    $this->db->insert('penugasan_fk', $data);
    return $this->db->insert_id();
  }
  public function insertPenugasanUniv($data) {
    $this->db->insert('penugasan_univ', $data);
    return $this->db->insert_id();
  }
  public function insertPendampingUniv($data) {
    $this->db->insert('pendamping_univ', $data);
    return $this->db->insert_id();
  }
  public function getUpdate($where, $data)
  {
    $this->db->update('akses', $data, $where);
    return $this->db->affected_rows();
  }
  public function updateMahasiswa($where, $data)
  {
    $this->db->update('mahasiswa', $data, $where);
    return $this->db->affected_rows();
  }
  public function updatePenugasanFk($where, $data)
  {
    $this->db->update('penugasan_fk', $data, $where);
    return $this->db->affected_rows();
  }

  public function updatePendampingUniv($where, $data)
  {
    $this->db->update('pendamping_univ', $data, $where);
    return $this->db->affected_rows();
  }
}