<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_daftar_tugas_penugasan extends CI_Model {
  var $table = "tbl_tugas";
  var $select_column = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan");
  var $order_column = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan", null);
  var $column_search = array("tbl_tugas.id_tugas","tbl_tugas.judul", "tbl_tugas.tgl", "tbl_tugas.berlaku","tbl_tugas.m_file","tbl_tugas.nilai","tbl_tugas.keterangan");
  var $relasi;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    
  }

  private function make_query()
  { 
    $id =  $this->session->userdata('id');

    $query = "SELECT pendamping_fk.*,kelompok_fk.idFk from akses,pendamping_fk,kelompok_fk where akses.id = pendamping_fk.idAkses and pendamping_fk.idKelompokFk=kelompok_fk.id AND akses.id='$id'";
    $sql   = $this->db->query($query)->row();

    $idFk = $sql->idFk;

    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->where("tbl_tugas.keterangan",$idFk);
    $this->db->order_by('id_tugas','desc');

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }
  function make_datatables(){
    $this->make_query();
    if($_POST["length"] != -1)
    {
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id_tugas',$id);
    $query = $this->db->get();

    return $query->row();
  }
  public function delete_by_id($id)
  {
    $this->db->where('id_tugas', $id);
    $this->db->delete($this->table);
  }
}