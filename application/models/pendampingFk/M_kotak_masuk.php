<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kotak_masuk extends CI_Model {

	var $table = 'kotak_masuk';//nama table database

	var $select_column = array('kotak_masuk.id_kotakmasuk','kotak_masuk.id_tugas','kotak_masuk.m_file','kotak_masuk.nis','kotak_masuk.nama_siswa','kotak_masuk.id_kelompok','kotak_masuk.tahun_angkatan','kotak_masuk.nama_guru','kotak_masuk.keterangan','kotak_masuk.tanggal','kotak_masuk.judul_tugas','kotak_masuk.pokok_1','kotak_masuk.nilai','kotak_masuk.idKelompokFk','kotak_masuk.idPendampungFk','kelompok_fk.namaKelompok');

	var $order_column = array('kotak_masuk.id_kotakmasuk','kotak_masuk.id_tugas','kotak_masuk.m_file','kotak_masuk.nis','kotak_masuk.nama_siswa','kotak_masuk.id_kelompok','kotak_masuk.tahun_angkatan','kotak_masuk.nama_guru','kotak_masuk.keterangan','kotak_masuk.tanggal','kotak_masuk.judul_tugas','kotak_masuk.pokok_1','kotak_masuk.nilai','kotak_masuk.idKelompokFk','kotak_masuk.idPendampungFk','kelompok_fk.namaKelompok',null);

	var $column_search = array('kotak_masuk.id_kotakmasuk','kotak_masuk.id_tugas','kotak_masuk.m_file','kotak_masuk.nis','kotak_masuk.nama_siswa','kotak_masuk.id_kelompok','kotak_masuk.tahun_angkatan','kotak_masuk.nama_guru','kotak_masuk.keterangan','kotak_masuk.tanggal','kotak_masuk.judul_tugas','kotak_masuk.pokok_1','kotak_masuk.nilai','kotak_masuk.idKelompokFk','kotak_masuk.idPendampungFk','kelompok_fk.namaKelompok');
	var $default_order = 'kotak_masuk.id_kotakmasuk';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$id =  $this->session->userdata('id');
		$query = "SELECT pendamping_fk.*,kelompok_fk.idFk from akses,pendamping_fk,kelompok_fk where akses.id = pendamping_fk.idAkses and pendamping_fk.idKelompokFk=kelompok_fk.id AND akses.id='$id'";
		$sql   = $this->db->query($query)->row();
		
		$id = $sql->id;
		$idKelompokFk = $sql->idKelompokFk;

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->join('kelompok_fk', 'kelompok_fk.id = kotak_masuk.idKelompokFk', 'left');
		$this->db->join('pendamping_fk', 'kotak_masuk.idPendampungFk = pendamping_fk.id', 'left');
		$this->db->where('pendamping_fk.id',$id);
		$this->db->where('kotak_masuk.idKelompokFk',$idKelompokFk);
		$this->db->order_by($this->default_order,'DESC');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST["order"])){
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else{
			$this->db->order_by($this->default_order, 'DESC');
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idAgama)
	{
		$this->db->from($this->table);
		$this->db->where('idAgama',$idAgama);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id_kotakmasuk', $id);
		$this->db->delete($this->table);
	}
	public function change_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
	public function getdata($key)
	{
		$this->db->from('kelompok');
		$this->db->where('kelompok.id_kelompok',$key);
		$query = $this->db->get();
		return $query->row();
	}
	public function change_status($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

}
