<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_daftar_anggota2 extends CI_Model {

  var $table = 'mahasiswa';//nama table database
  var $select_column =  array('mahasiswa.id','mahasiswa.nim','mahasiswa.nama','mahasiswa.tahun','mahasiswa.jenis_kelamin','mahasiswa.foto','mahasiswa.idJurusan','mahasiswa.idAkses','mahasiswa.idKelompokFk','mahasiswa.idPendampungFk','jurusan.jurusan','kelompok_fk.namaKelompok','(pendamping_fk.nama) nama_pendamping');

  var $order_column = array('mahasiswa.id','mahasiswa.nim','mahasiswa.nama','mahasiswa.tahun','mahasiswa.jenis_kelamin','mahasiswa.foto','mahasiswa.idJurusan','mahasiswa.idAkses','mahasiswa.idKelompokFk','mahasiswa.idPendampungFk','jurusan.jurusan','kelompok_fk.namaKelompok','(pendamping_fk.nama) nama_pendamping',null);
  var $column_search = array('mahasiswa.id','mahasiswa.nim','mahasiswa.nama','mahasiswa.tahun','mahasiswa.jenis_kelamin','mahasiswa.foto','mahasiswa.idJurusan','mahasiswa.idAkses','mahasiswa.idKelompokFk','mahasiswa.idPendampungFk','jurusan.jurusan','kelompok_fk.namaKelompok');
  var $default_order = 'mahasiswa.id';

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function _get_datatables_query()
  {
    $id =  $this->session->userdata('id');
    $query = "SELECT pendamping_fk.*,kelompok_fk.idFk from akses,pendamping_fk,kelompok_fk where akses.id = pendamping_fk.idAkses and pendamping_fk.idKelompokFk=kelompok_fk.id AND akses.id='$id'";
    $sql   = $this->db->query($query)->row();
    $idKelompokFk = $sql->idKelompokFk;

    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('jurusan', 'mahasiswa.idJurusan = jurusan.id_jurusan', 'left');
    $this->db->join('kelompok_fk', 'mahasiswa.idKelompokFk = kelompok_fk.id', 'left');
    $this->db->join('pendamping_fk', 'mahasiswa.idPendampungFk = pendamping_fk.id', 'left');
    $this->db->where('mahasiswa.idKelompokFk',$idKelompokFk);

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }

  function get_datatables()
  {
    $this->_get_datatables_query();
    if($_POST['length'] != -1)
      $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
  }

  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  public function get_by_id($idAgama)
  {
    $this->db->from($this->table);
    $this->db->where('idAgama',$idAgama);
    $query = $this->db->get();

    return $query->row();
  }

  public function save($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

  public function delete_by_id($idAgama)
  {
    $this->db->where('idAgama', $idAgama);
    $this->db->delete($this->table);
  }
  public function change_id($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }
  public function getdata($key)
  {
    $this->db->from('kelompok');
    $this->db->where('kelompok.id_kelompok',$key);
    $query = $this->db->get();
    return $query->row();
  }
  public function change_status($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

}
