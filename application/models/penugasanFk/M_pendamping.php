<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pendamping extends CI_Model {
  var $table = "pendamping_fk";
  var $select_column = array("pendamping_fk.id","pendamping_fk.idAkses", "pendamping_fk.nama", "pendamping_fk.idKelompokFk","kelompok_fk.namaKelompok","akses.username","akses.password");
  var $order_column = array("pendamping_fk.id","pendamping_fk.idAkses", "pendamping_fk.nama", "pendamping_fk.idKelompokFk","kelompok_fk.namaKelompok","akses.username","akses.password",null);
  var $column_search = array("pendamping_fk.id","pendamping_fk.idAkses", "pendamping_fk.nama", "pendamping_fk.idKelompokFk","kelompok_fk.namaKelompok","akses.username","akses.password");
  var $relasi;
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    
  }

  private function make_query()
  { 
    $id_sess = $this->session->userdata('id');
    $sql = "SELECT akses.*,penugasan_fk.* FROM penugasan_fk,akses where akses.id= penugasan_fk.idAkses and akses.id='$id_sess'";
    $getData = $this->db->query($sql)->row();
    $idFk = $getData->idFk;

    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->join('akses', 'pendamping_fk.idAkses = akses.id', 'left');
    $this->db->join('kelompok_fk', 'kelompok_fk.id = pendamping_fk.idKelompokFk', 'left');
    $this->db->where('kelompok_fk.idFk',$idFk);
    $this->db->order_by('pendamping_fk.id','desc');

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }
  function make_datatables(){
    $this->make_query();
    if($_POST["length"] != -1)
    {
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id($id)
  {
    $this->db->from($this->table);
    $this->db->where('id',$id);
    $query = $this->db->get();

    return $query->row();
  }
  public function delete_by_id($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('akses');
  }
  public function getUpdate($where, $data)
  {
    $this->db->update('akses', $data, $where);
    return $this->db->affected_rows();
  }
  public function pendamping_fk($where, $data)
  {
    $this->db->update('pendamping_fk', $data, $where);
    return $this->db->affected_rows();
  }
   public function getInsert($data) {
    $this->db->insert('akses',$data);
    return $this->db->insert_id();
  }
  public function insertPendampingFk($data) {
    $this->db->insert('pendamping_fk', $data);
    return $this->db->insert_id();
  }
}