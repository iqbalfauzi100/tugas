<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_item_terkirim extends CI_Model {
  var $table = "kotak_masuk";
  var $select_column = array('kotak_masuk.id_kotakmasuk','kotak_masuk.id_tugas','kotak_masuk.nis','kotak_masuk.nama_siswa','kotak_masuk.jurusan','kotak_masuk.tahun_angkatan','kotak_masuk.tanggal','kotak_masuk.m_file','kotak_masuk.judul_tugas','kotak_masuk.pokok_1','kotak_masuk.nilai','kotak_masuk.keterangan','kotak_masuk.status_tugas');

  var $order_column = array('kotak_masuk.id_kotakmasuk','kotak_masuk.id_tugas','kotak_masuk.nis','kotak_masuk.nama_siswa','kotak_masuk.jurusan','kotak_masuk.tahun_angkatan','kotak_masuk.tanggal','kotak_masuk.m_file','kotak_masuk.judul_tugas','kotak_masuk.pokok_1','kotak_masuk.nilai','kotak_masuk.keterangan','kotak_masuk.status_tugas',null);
  var $column_search = array('kotak_masuk.id_kotakmasuk','kotak_masuk.id_tugas','kotak_masuk.nis','kotak_masuk.nama_siswa','kotak_masuk.jurusan','kotak_masuk.tahun_angkatan','kotak_masuk.tanggal','kotak_masuk.m_file','kotak_masuk.judul_tugas','kotak_masuk.pokok_1','kotak_masuk.nilai','kotak_masuk.keterangan','kotak_masuk.status_tugas');
  var $default_order = 'kotak_masuk.id_kotakmasuk';
  
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function make_query()
  { 
    $nim=$this->session->userdata('username');
    $this->db->select($this->select_column);
    $this->db->from($this->table);
    $this->db->where('nis',$nim);
    $this->db->order_by('id_kotakmasuk','desc');

    $i = 0;
    foreach ($this->column_search as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($this->column_search) - 1 == $i)
          $this->db->group_end();
      }
      $i++;
    }

    if(isset($_POST["order"])){
      $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else{
      $this->db->order_by($this->default_order, 'DESC');
    }
  }
  function make_datatables(){
    $this->make_query();
    if($_POST["length"] != -1)
    {
      $this->db->limit($_POST['length'], $_POST['start']);
    }
    $query = $this->db->get();
    return $query->result();
  }
  function get_filtered_data(){
    $this->make_query();
    $query = $this->db->get();
    return $query->num_rows();
  }
  function get_all_data()
  {
    $this->db->select("*");
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
  public function get_by_id_bea($id)
  {
    $this->db->from($this->table);
    $this->db->where('bea.id',$id);
    $query = $this->db->get();
    return $query->row();
  }
  public function get_scoring()
  {
    $this->db->select('*');
    $this->db->from('kategori_skor');
    $query = $this->db->get();
    return $query->result();
  }
  public function save_bea($data)
  {
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }
  public function save_sub_bea($data)
  {
    $this->db->insert_batch('set_bea_kategori_skor', $data);
    return $this->db->insert_id();
  }
  public function get_skor_by_idBea($id)
  {
    $this->db->select('set_bea_kategori_skor.idKategoriSkor, set_bea_kategori_skor.id');
    $this->db->from('set_bea_kategori_skor');
    $this->db->where('set_bea_kategori_skor.idBea',$id);
    $query = $this->db->get();
    return $query->result();
  }
  public function update_setting_bea($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }
  public function update_setting_sub_bea($where, $data)
  {
    $this->db->update('set_bea_kategori_skor', $data, $where);
    return $this->db->affected_rows();
  }
  public function insert_setting_sub_bea($data)
  {
    $this->db->insert('set_bea_kategori_skor', $data);
    return $this->db->insert_id();
  }
  public function delete_setting_sub_bea($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('set_bea_kategori_skor');
  }
  public function delete_by_id($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    $this->db->where('idBea', $id);
    $this->db->delete('set_bea_kategori_skor');
  }
  public function get_total_nilai_mhs()
  {
    $nis = $this->session->userdata('username');
    $this->db->distinct();
    $this->db->select('nilai_mahasiswa.total_nilai');
    $this->db->from('nilai_mahasiswa');
    $this->db->where('nilai_mahasiswa.nis', $nis);
    $query = $this->db->get();
    return $query->row();
  }
  public function count_score($nis)
  {
    $sql = 'SELECT SUM(kotak_masuk.nilai) nilai FROM kotak_masuk WHERE kotak_masuk.keterangan=1 AND kotak_masuk.nis='.$nis.'';
    $query = $this->db->query($sql);
    return $query;
  }
  public function view_detail_upload($nim,$id_tugas)
  {
    $sql = 'SELECT * FROM kotak_masuk_upload where nim= '.$nim.' && id_tugas = '.$id_tugas;
    $res = $this->db->query($sql);
    return $res->result();
  }
}