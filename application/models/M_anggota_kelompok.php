<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_anggota_kelompok extends CI_Model {

	var $table = 'mahasiswa';//nama table database
	var $select_column =  array('mahasiswa.id','mahasiswa.idAkses','mahasiswa.nim','mahasiswa.nama','mahasiswa.idJurusan','mahasiswa.idKelompokU','mahasiswa.idPendampungU','jurusan.jurusan','kelompok.kelompok');
	var $order_column = array('mahasiswa.id','mahasiswa.idAkses','mahasiswa.nim','mahasiswa.nama','mahasiswa.idJurusan','mahasiswa.idKelompokU','mahasiswa.idPendampungU','jurusan.jurusan','kelompok.kelompok',null);
	var $column_search = array('mahasiswa.id','mahasiswa.idAkses','mahasiswa.nim','mahasiswa.nama','mahasiswa.idJurusan','mahasiswa.idKelompokU','mahasiswa.idPendampungU','jurusan.jurusan','kelompok.kelompok');
	var $default_order = 'mahasiswa.id';
	var $relasi;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$id = $this->session->userdata('id');
		$query = "SELECT pendamping_univ.id,pendamping_univ.idKelompok from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$id'";
		$sql   = $this->db->query($query)->row();
		$idKelompok = $sql->idKelompok;

		$relation = 'mahasiswa.idKelompokU = 0 or mahasiswa.idKelompokU='.$idKelompok.'';

		$this->db->select($this->select_column);
		$this->db->from($this->table);
		$this->db->join('jurusan', 'mahasiswa.idJurusan = jurusan.id_jurusan', 'left');
		$this->db->join('kelompok', 'mahasiswa.idKelompokU = kelompok.id_kelompok', 'left');
		$this->db->where($relation);
		$this->db->order_by($this->default_order,'DESC');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST["order"])){
			$this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else{
			$this->db->order_by($this->default_order, 'DESC');
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($idAgama)
	{
		$this->db->from($this->table);
		$this->db->where('idAgama',$idAgama);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($idAgama)
	{
		$this->db->where('idAgama', $idAgama);
		$this->db->delete($this->table);
	}
	public function change_id($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
	public function update_id($where, $data)
	{
		$this->db->update('mahasiswa', $data, $where);
		return $this->db->affected_rows();
	}
	public function getdata($key)
	{
		$this->db->from('kelompok');
		$this->db->where('kelompok.id_kelompok',$key);
		$query = $this->db->get();
		return $query->row();
	}
	public function getKelompokFk($key)
	{
		$this->db->from('kelompok_fk');
		$this->db->where('kelompok_fk.id',$key);
		$query = $this->db->get();
		return $query->row();
	}
	public function get_jurusan()
	{
		$sql = 'SELECT * FROM `jurusan`';
		$res = $this->db->query($sql);
		return $res->result();
	}


}
