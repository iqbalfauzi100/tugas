<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SiswaKirimFk extends CI_Controller {
	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));

	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('m_anggota_kelompok','model');
	// 		$this->load->model('m_item_terkirim','mod');
	// 		$this->load->model('m_daftar_tugas','mdl');
	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		
		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('m_anggota_kelompok','model');
		$this->load->model('m_item_terkirim','mod');
		$this->load->model('m_daftar_tugas','mdl');
	}

	public function login(){
		$this->load->view('view_login');
	}

	public function proses_kirim(){
		$this->load->library('form_validation');

		$id_tugas 		= $this->input->post('id_tugas');
		$nis 			= $this->input->post('nim');
		$nama_guru 		= $this->input->post('nama_guru');
		$nama_siswa		= $this->input->post('nama_siswa');
		$id_kelompok	= $this->input->post('id_kelompok');
		$jurusan		= $this->input->post('jurusan');
		$tahun_angkatan	= $this->input->post('tahun_angkatan');
		$judul_tugas	= $this->input->post('judul_tugas');
		$rangkuman		= $this->input->post('rangkuman');
		$nilai			= $this->input->post('nilai');
		$keterangan		= $this->input->post('keterangan');

		$data_judul = $this->db->query("SELECT * FROM kotak_masuk where nis='$nis' and judul_tugas like '$judul_tugas'");
		if($data_judul->num_rows() <> 0){
			?>
			<script type="text/javascript">
				alert('data sudah ada ');window.location='kirim';
			</script>
			<?php

		}else{
			$sql_1 = $this->db->query("SELECT * FROM kotak_masuk where nis='$nis' and pokok_1 LIKE '$rangkuman'");
			if($sql_1->num_rows() <> 0){
				?>
				<script type="text/javascript">
					alert('data sudah ada ');window.location='kirim';
				</script>
				<?php

			}else{
				$m_file = $_FILES['userfile1']['name'];
				if($m_file =='')
				{
					$datas = array(
						'id_tugas' 		=> $id_tugas,
						'nis' 			=> $nis,
						'nama_siswa'    => $nama_siswa,
						'jurusan'  		=> $jurusan,
						'tahun_angkatan'=> $tahun_angkatan,
						'judul_tugas'  	=> $judul_tugas,
						'pokok_1'  		=> $rangkuman,
						'm_file'  		=> $m_file,
						'nilai'  		=> $nilai,
						'status_tugas'  => $keterangan,
						'idPendampungFk'=> $nama_guru,
						'idKelompokFk'  => $id_kelompok
						);
					$sukses = $this->db->insert('kotak_masuk', $datas);
					if($sukses == TRUE){
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Kirim</div>");
						redirect('siswa/item_terkirim','refresh');
					}else{
						return FALSE;
						exit();
						redirect('siswa/kirim_fk','refresh');

					}	
				}else{ 
					$config['upload_path'] = './assets/images/kirim_tugas/';
					$config['allowed_types'] = 'jpg|png|JPG|jpeg|pdf|docx|doc|xlsx|xls|pptx|ppt';

					$this->upload->initialize($config);

					if ($this->upload->do_upload('userfile1'))
					{
						$data = array('upload_data' => $this->upload->data());
						$gbr = $this->upload->data();

						//Compress Image
						$config['image_library']='gd2';
						$config['source_image']='./assets/images/kirim_tugas/'.$gbr['file_name'];
						$config['create_thumb']= FALSE;
						$config['maintain_ratio']= FALSE;
						$config['quality']= '50%';
						$config['width']= 1500;
						$config['height']= 1700;
						$config['new_image']= './assets/images/kirim_tugas/'.$gbr['file_name'];
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();

						$gambar = $gbr['file_name'];

						$datas = array(
							'id_tugas' 		=> $id_tugas,
							'nis' 			=> $nis,
							'nama_siswa'    => $nama_siswa,
							'jurusan'  		=> $jurusan,
							'tahun_angkatan'=> $tahun_angkatan,
							'judul_tugas'  	=> $judul_tugas,
							'pokok_1'  		=> $rangkuman,
							'm_file'  		=> $gambar,
							'nilai'  		=> $nilai,
							'status_tugas'  => $keterangan,
							'idPendampungFk'=> $nama_guru,
							'idKelompokFk'  => $id_kelompok
							);
						$sukses = $this->db->insert('kotak_masuk', $datas);
						if($sukses == TRUE){
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Kirim</div>");
							redirect('siswa/item_terkirim','refresh');

						}else{
							return FALSE;
							exit();
							redirect('siswa/kirim_fk','refresh');
						}
					}
					else
					{
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : jpg, JPG, jpeg, png, pdf, doc, xls ]</div>");
						redirect('siswa/kirim_fk','refresh');
					}
				}
			}
		}
	}


	public function proses_kirim_universitas(){
		$this->load->library('form_validation');

		$id_tugas 		= $this->input->post('id_tugas');
		$nis 			= $this->input->post('nim');
		$nilai			= $this->input->post('nilai');
		$keterangan		= $this->input->post('keterangan');
		$nama_siswa		= $this->input->post('nama_siswa');
		$tahun_angkatan	= $this->input->post('tahun_angkatan');
		$jurusan		= $this->input->post('jurusan');
		$id_kelompok	= $this->input->post('id_kelompok');
		$nama_guru 		= $this->input->post('nama_guru');
		$judul_tugas	= $this->input->post('judul_tugas');
		$rangkuman		= $this->input->post('rangkuman');
		
		$data;
		// $getData = $this->db->get_where('kotak_masuk',array('nis'=>$nis,'id_tugas'=>$id_tugas))->result();
		$getData = $this->db->get_where('kotak_masuk',array('nis'=>14650001,'id_tugas'=>49))->result();
		$count = count($getData);

		if($count=0){

			$data = array(
				'id_tugas' 		=> $id_tugas,
				'nis' 			=> $nis,
				'nama_siswa'    => $nama_siswa,
				'jurusan'  		=> $jurusan,
				'tahun_angkatan'=> $tahun_angkatan,
				'judul_tugas'  	=> $judul_tugas,
				'pokok_1'  		=> $rangkuman,
				'nilai'  		=> $nilai,
				'status_tugas'  => $keterangan,
				'idPendampungFk'=> $nama_guru,
				'idKelompokFk'  => $id_kelompok
				);

			$sukses = $this->db->insert('kotak_masuk', $data);
			if($sukses == TRUE){
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Kirim</div>");
				redirect('siswa/item_terkirim','refresh');
			}else{
				return FALSE;
				exit();
				redirect('siswa/kirim','refresh');

			}
		}else{
			?>
			<script type="text/javascript">
				alert('Data sudah ada');window.location='kirim';
			</script>
			<?php
		}
	}

}
