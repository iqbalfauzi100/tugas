<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siswa extends CI_Controller {
	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url','file'));

	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('m_anggota_kelompok','model');
	// 		$this->load->model('m_item_terkirim','mod');
	// 		$this->load->model('m_daftar_tugas','mdl');
	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		
		$this->load->library('upload');
		$this->load->helper(array('url','file'));
		$this->load->model('m_anggota_kelompok','model');
		$this->load->model('m_item_terkirim','mod');
		$this->load->model('m_daftar_tugas','mdl');
		
	}
	public function index(){
		$data['id'] 		= $this->session->userdata('id');
		$data['username'] 	= $this->session->userdata('username');

		$data['content'] 		= 'v_siswa';
		$this->load->view('view_siswa',$data);
		unset($data);
	}

	public function site(){
		$data['id'] 		= $this->session->userdata('id');
		$data['username'] 	= $this->session->userdata('username');

		$data['content'] 		= 'v_siswa';
		$this->load->view('view_siswa',$data);
		unset($data);
	}

	public function kirim(){
		$id = $this->input->post('idPengaturan');
		$nim = $this->session->userdata('username');
		if ($id != null) {
			$detailTugas= $this->mdl->get_by_id_daftar_tugas($id);
			$dataMhs = $this->mdl->dataIdentitas($nim);
			$data = array(
				'nim' 		=>$this->session->userdata('username'),
				'nama'		=>$dataMhs->nama,
				'tahun'		=>$dataMhs->tahun,
				'idJurusan'	=>$dataMhs->idJurusan,

				'id_tugas' 	=>$id,
				'judul'		=>$detailTugas->judul,
				'berlaku'	=>$detailTugas->berlaku,
				'nilai'		=>$detailTugas->nilai,
				'keterangan'=>$detailTugas->keterangan
				);
		}else{
			$data = array(
				'nim' 		=> $this->session->userdata('username'),
				'nama'		=> "",
				'tahun'		=> "",
				'idJurusan'	=> "",

				'id_tugas' 	=> "",
				'judul'		=> "",
				'berlaku'	=> "",
				'nilai'		=> "",
				'keterangan'=> ""
				);
		}

		// $data['nis'] = $this->session->userdata('username');
		$data['idTugas'] = $id;

		$data['content'] 		= 'v_kirim';
		$this->load->view('view_siswa',$data);
		unset($id,$nim,$detailTugas,$dataMhs,$data);
	}

	//Untuk proses upload foto
	function proses_upload(){

		$config['upload_path']   = FCPATH.'/assets/images/kirim_tugas/';
		$config['allowed_types'] = 'jpg|png|JPG|jpeg|pdf|docx|doc|xlsx|xls|pptx|ppt';

		$this->load->library('upload',$config);

		if($this->upload->do_upload('userfile')){

        	//Compress Image
			$config['image_library']='gd2';
			$config['source_image']= FCPATH.'/assets/images/kirim_tugas/'.$this->upload->data('file_name');
			$config['create_thumb']= FALSE;
			$config['maintain_ratio']= FALSE;
			$config['quality']= '50%';
			$config['width']= 1500;
			$config['height']= 1700;
			$config['new_image']= FCPATH.'/assets/images/kirim_tugas/'.$this->upload->data('file_name');
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			$gambar = $this->upload->data('file_name');

			$data = array(
				'nim' 		=> $this->input->post('nim'),
				'id_tugas'	=> $this->input->post('id_tugas'),
				'title'		=> $gambar,
				'token'		=> $this->input->post('token_foto')
				);

			$this->db->insert('kotak_masuk_upload',$data);
		}
	}


	//Untuk menghapus foto
	function remove_foto(){
		//Ambil token foto
		$token = $this->input->post('token');
		$foto  = $this->db->get_where('kotak_masuk_upload',array('token'=>$token));


		if($foto->num_rows()>0){
			$hasil = $foto->row();
			$nama_foto = $hasil->title;
			if(file_exists($file=FCPATH.'/assets/images/kirim_tugas/'.$nama_foto)){
				unlink($file);
			}
			$this->db->delete('kotak_masuk_upload',array('token'=>$token));

		}
		echo "{}";
	}

	public function kirim_fk(){
		$id = $this->input->post('idPengaturan');
		$nim = $this->session->userdata('username');
		if ($id != null) {
			$detailTugas= $this->mdl->get_by_id_daftar_tugas($id);
			$dataMhs = $this->mdl->dataIdentitas($nim);
			$data = array(
				'nim' 		=>$this->session->userdata('username'),
				'nama'		=>$dataMhs->nama,
				'tahun'		=>$dataMhs->tahun,
				'idJurusan'	=>$dataMhs->idJurusan,

				'id_tugas' 	=>$id,
				'judul'		=>$detailTugas->judul,
				'berlaku'	=>$detailTugas->berlaku,
				'nilai'		=>$detailTugas->nilai,
				'keterangan'=>$detailTugas->keterangan
				);
		}else{
			$data = array(
				'nim' 		=> $this->session->userdata('username'),
				'nama'		=> "",
				'tahun'		=> "",
				'idJurusan'	=> "",

				'id_tugas' 	=> "",
				'judul'		=> "",
				'berlaku'	=> "",
				'nilai'		=> "",
				'keterangan'=> ""
				);
		}

		// $data['nis'] = $this->session->userdata('username');
		$data['idTugas'] = $id;

		$data['content'] 		= 'v_kirim_fk';
		$this->load->view('view_siswa',$data);
		unset($id,$nim,$detailTugas,$dataMhs,$data);
	}
	public function profile(){
		$key = $this->session->userdata('username');

		$this->db->from('akses');
		$this->db->join('mahasiswa', 'mahasiswa.nim = akses.username', 'left');
		$this->db->where('akses.username',$key);
		$cek = $this->db->get()->row();

		$this->db->select('nim');    
		$this->db->from('mahasiswa');
		$this->db->where('mahasiswa.nim',$key);
		$cek_db = $this->db->count_all_results();

		if($cek_db != 0){
			$data =array(
				'nim'      	=> $key,
				'id'    	=> $cek->id,
				'nama'     	=> $cek->nama,
				'tahun'   	=> $cek->tahun,
				'jur_siswa'     => $cek->idJurusan,
				'jenis_kelamin' => $cek->jenis_kelamin,
				'id_kelompok'   => $cek->idKelompokU,
				'id_pendamping' => $cek->idPendampungU,
				'idKelompokFk'  => $cek->idKelompokFk,
				'idPendampungFk'=> $cek->idPendampungFk,
				'password'    	=> $cek->password,
				'foto'    		=> $cek->foto
				);
		}else{
			$data =array(
				'nim'      	=> $key,
				'id'    	=> "",
				'nama'     	=> "",
				'tahun'   	=> "",
				'jur_siswa'     => "",
				'jenis_kelamin' => "",
				'id_kelompok'   => "",
				'id_pendamping' => "",
				'password'    	=> $cek->password,
				'foto'    		=> "",
				);
		}


		$data['content'] 		= 'v_profile_siswa';
		$this->load->view('view_siswa',$data);
		unset($key,$cek,$cek_db,$data);
	}
	public function coba(){
		// $data['nis'] = $this->session->userdata('nis');
		$data['nis'] = $this->session->userdata('nis');

		$data['content'] 		= 'v_profileUjiCoba';
		$this->load->view('view_siswa',$data);
	}
	
	public function zenziva($number, $message){
		$userkey = 'bztxk8';
		$passkey = 'cakmunir';
		$message = urlencode($message);
		$fields_string = "";
		
		$fields = array(
			'userkey' => $userkey,
			'passkey' => $passkey,
			'nohp'    => $number,
			'pesan'   => $message
			);
		
		$url = "https://reguler.zenziva.net/apps/smsapi.php/?";

		foreach($fields as $key=>$value) { 
			$fields_string .= $key.'='.$value.'&'; 
		}
		rtrim($fields_string, '&');

		$curlHandle = curl_init('https://reguler.zenziva.net/apps/smsapi.php/?');
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, true);
		$results = curl_exec($curlHandle);
		curl_close($curlHandle);
		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Message Send Successfully<br/>
			.'$results'.
		</div>");
	}

	public function call()
	{
		$number = $this->input->post('hp');
		$message = $this->input->post('message');

		$count = count($number);
		for ($i=0; $i < $count; $i++) { 
			$this->zenziva($number[$i],$message);
		}
		redirect('Siswa/coba','refresh');
	}

	public function item_terkirim(){
		$nis = $this->session->userdata('username');
		$data['nis'] = $this->session->userdata('username');
		
		$nilai = $this->mod->count_score($nis)->row();
		if($nilai != null){
			$data['total_nilai'] = $nilai->nilai;
		}else{
			$data['total_nilai'] = 0;
		}

		$data['content'] 		= 'v_terkirim';
		$this->load->view('view_siswa',$data);
		unset($nis,$nilai,$data);
	}
	public function datatable_dataTugas()
	{
		$nim = $this->session->userdata('username');

		$list = $this->mdl->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->judul;
			$row[] = $model->tgl;
			$row[] = $model->berlaku;

			$ket = $model->keterangan;
			if($ket==0){
				$row[] = '
				<span class="label label-success"><i class="fa fa-clock-o"></i> Universitas</span>
				<a class="btn btn-sm btn-primary" href="assets/images/penugasan/'.$model->m_file.'" title="Lihat File" target="_blank">
					<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
				</a>
				';
			}else{
				$row[] = '
				<span class="label label-info"><i class="fa fa-clock-o"></i> Fakultas</span>
				<a class="btn btn-sm btn-primary" href="assets/images/penugasan/'.$model->m_file.'" title="Lihat File" target="_blank">
					<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
				</a>
				';
			}

			/*$row[] = '
			<a class="btn btn-sm btn-primary" href="assets/images/penugasan/'.$model->m_file.'" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
			</a>
			';*/

			$cek_idTugas = $this->mdl->get_id_tugas($model->id_tugas);
			date_default_timezone_set("Asia/Jakarta");
			$now = date("Y-m-d H:i:s");
			$deadline = $model->berlaku;
			$start = $model->tgl;

			$nama 			= $this->mdl->dataIdentitas($nim)->nama;
			$tahun 			= $this->mdl->dataIdentitas($nim)->tahun;
			$jenis_kelamin 	= $this->mdl->dataIdentitas($nim)->jenis_kelamin;
			$idJurusan 		= $this->mdl->dataIdentitas($nim)->idJurusan;

			$idKelompokU 	= $this->mdl->dataIdentitas($nim)->idKelompokU;
			$idPendampungU 	= $this->mdl->dataIdentitas($nim)->idPendampungU;
			$idKelompokFk 	= $this->mdl->dataIdentitas($nim)->idKelompokFk;
			$idPendampungFk = $this->mdl->dataIdentitas($nim)->idPendampungFk;

			
			if($cek_idTugas != 0  && $now>=$start && $now<=$deadline || $cek_idTugas != 0  && $now>=$start && $now>=$deadline){
				#Done telah dikerjakan
				$row[] = '
				<a class="btn btn-sm btn-success" name="idPengaturan" title="Done"><i class="glyphicon glyphicon-ok"></i> </a>
				';
			}elseif($cek_idTugas == 0 && $now>=$start && $now<=$deadline){
				#Belum dikerjakan
				if ($nama != null && $tahun != null && $jenis_kelamin != null && $idJurusan != null) {
					#Profil Mahasiswa Sudah di ISI
					if($model->keterangan==0){
						#Tugas Universitas
						if($idKelompokU!=0 && $idPendampungU!=0){
							#Cek Mahasiswa dengan nim ini sudah punya kelompok & pendamping atau belum
							$alamat = base_url('Siswa/kirim');
							$row[] = '
							<form action="'.$alamat.'" method="post">
								<button class="btn btn-sm btn-warning" title="Kerjakan Sekarang" type="submit" name="idPengaturan" value="'.$model->id_tugas.'"><i class="glyphicon glyphicon-pencil"></i></button>
							</form>
							';
						}else{
							$alamat_script = base_url('Siswa/script_data_universitas');
							$row[] = '
							<a href="'.$alamat_script.'" class="btn btn-sm btn-warning" title="Anda Belum terdaftar sebagai anggota kelompok penugasan Universitas"><i class="glyphicon glyphicon-pencil"></i></a>
							';
						}

					}else{
						#Tugas Fakultas
						if($idKelompokFk!=0 && $idPendampungFk!=0){
							#Cek Mahasiswa dengan nim ini sudah punya kelompok & pendamping atau belum
							$alamat = base_url('Siswa/kirim_fk');
							$row[] = '
							<form action="'.$alamat.'" method="post">
								<button class="btn btn-sm btn-warning" title="Kerjakan Sekarang" type="submit" name="idPengaturan" value="'.$model->id_tugas.'"><i class="glyphicon glyphicon-pencil"></i></button>
							</form>
							';
						}else{
							$alamat_script = base_url('Siswa/script_data_fakultas');
							$row[] = '
							<a href="'.$alamat_script.'" class="btn btn-sm btn-warning" title="Anda Belum terdaftar sebagai anggota kelompok penugasan Fakultas"><i class="glyphicon glyphicon-pencil"></i></a>
							';
						}
					}
				}else{
					#Profil Mahasiswa Belum di ISI
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-warning alert-dismissible\">Lengkapi data profile anda</div>");

					$alamat_profile = base_url('Siswa/profile');
					$row[] = '
					<a href="'.$alamat_profile.'" class="btn btn-sm btn-warning" title="Kerjakan Sekarang"><i class="glyphicon glyphicon-pencil"></i></a>
					';
				}

			}elseif($cek_idTugas == 0 && $now>=$start && $now>=$deadline){
				#Belum dikerjakan (Waktu TIME OUT)
				$row[] = '
				<a class="btn btn-sm btn-danger" name="idPengaturan" title="Time Out (Selesaikan pekerjaan tepat waktu)"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}else{
				// }elseif($cek_idTugas == 0 && $now<=$start && $now<=$deadline){
				#Belum Waktunya mengerjakan
				$row[] = '
				<a class="btn btn-sm btn-info" name="idPengaturan" title="Belum waktunya mengerjakan"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mdl->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function script_data_universitas(){ 
		?>
		<script type="text/javascript">
			alert("Anda Belum terdaftar sebagai anggota kelompok penugasan Universitas");window.location="<?php echo base_url(); ?>Siswa/kotak_masuk";
		</script>
		<?php
	}
	public function script_data_fakultas(){ 
		?>
		<script type="text/javascript">
			alert("Anda Belum terdaftar sebagai anggota kelompok penugasan Fakultas");window.location="<?php echo base_url(); ?>Siswa/kotak_masuk";
		</script>
		<?php
	}


	public function datatable_itemTerkirim()
	{
		$nim = $this->session->userdata('username');

		$list = $this->mod->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->judul_tugas;
			$row[] = $model->pokok_1;
			$row[] = '
			<a class="btn btn-sm btn-primary" title="Lihat File" target="_blank" onclick="view_detail_upload('.$nim.','.$model->id_tugas.')">
				<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
			</a>
			';

			/*$row[] = '
			<a class="btn btn-sm btn-primary" href="assets/images/kirim_tugas/'.$model->m_file.'" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
			</a>
			';*/
			$keterangan = $model->keterangan;
			$status_tugas = $model->status_tugas;

			if($keterangan == 1){
				if($status_tugas==0){
					$row[] = '
					<span class="label label-primary"><i class="fa fa-clock-o"></i> Universitas</span>
					<span class="label label-success"><i class="fa fa-clock-o"></i> Verifikasi</span>
					';
				}else{
					$row[] = '
					<span class="label label-info"><i class="fa fa-clock-o"></i> Fakultas</span>
					<span class="label label-success"><i class="fa fa-clock-o"></i> Verifikasi</span>
					';
				}
			}else{
				// $row[] = '
				// <span class="label label-danger"><i class="fa fa-clock-o"></i> Belum Verifikasi</span>
				// ';
				if($status_tugas==0){
					$row[] = '
					<span class="label label-primary"><i class="fa fa-clock-o"></i> Universitas</span>
					<span class="label label-danger"><i class="fa fa-clock-o"></i> Belum Verifikasi</span>
					';
				}else{
					$row[] = '
					<span class="label label-info"><i class="fa fa-clock-o"></i> Fakultas</span>
					<span class="label label-danger"><i class="fa fa-clock-o"></i> Belum Verifikasi</span>
					';
				}
			}
			$row[] = $model->nilai;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mod->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function view_detail_upload($nim, $id_tugas)
	{
		$data = $this->mod->view_detail_upload($nim, $id_tugas);
		echo json_encode($data);
		unset($nim,$id_tugas,$data);
	}

	public function proses_kirim(){
		$this->load->library('form_validation');

		$id_tugas 		= $this->input->post('id_tugas');
		$nis 			= $this->input->post('nim');
		$nama_guru 		= $this->input->post('nama_guru');
		$nama_siswa		= $this->input->post('nama_siswa');
		$id_kelompok	= $this->input->post('id_kelompok');
		$jurusan		= $this->input->post('jurusan');
		$tahun_angkatan	= $this->input->post('tahun_angkatan');
		$judul_tugas	= $this->input->post('judul_tugas');
		$rangkuman		= $this->input->post('rangkuman');
		$nilai			= $this->input->post('nilai');
		$keterangan		= $this->input->post('keterangan');

		$data_judul = $this->db->query("SELECT * FROM kotak_masuk where nis='$nis' and judul_tugas like '$judul_tugas'");
		if($data_judul->num_rows() <> 0){
			?>
			<script type="text/javascript">
				alert('data sudah ada ');window.location='kirim';
			</script>
			<?php

		}else{
			$sql_1 = $this->db->query("SELECT * FROM kotak_masuk where nis='$nis' and pokok_1 LIKE '$rangkuman'");
			if($sql_1->num_rows() <> 0){
				?>
				<script type="text/javascript">
					alert('data sudah ada ');window.location='kirim';
				</script>
				<?php

			}else{
				$m_file = $_FILES['userfile1']['name'];
				if($m_file =='')
				{
					$datas = array(
						'id_tugas' 		=> $id_tugas,
						'nis' 			=> $nis,
						'nama_guru' 	=> $nama_guru,
						'nama_siswa'    => $nama_siswa,
						'id_kelompok'  	=> $id_kelompok,
						'jurusan'  		=> $jurusan,
						'tahun_angkatan'=> $tahun_angkatan,
						'judul_tugas'  	=> $judul_tugas,
						'pokok_1'  		=> $rangkuman,
						'm_file'  		=> $m_file,
						'nilai'  		=> $nilai,
						'status_tugas'  => $keterangan
						);
					$sukses = $this->db->insert('kotak_masuk', $datas);
					if($sukses == TRUE){
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Kirim</div>");
						redirect('Siswa/item_terkirim','refresh');
					}else{
						return FALSE;
						exit();
						redirect('Siswa/kirim','refresh');

					}	
				}else{ 
					$config['upload_path'] = './assets/images/kirim_tugas/';
					$config['allowed_types'] = 'jpg|png|JPG|jpeg|pdf|docx|doc|xlsx|xls|pptx|ppt';

					$this->upload->initialize($config);

					if ($this->upload->do_upload('userfile1'))
					{
						$data = array('upload_data' => $this->upload->data());
						$gbr = $this->upload->data();

						//Compress Image
						$config['image_library']='gd2';
						$config['source_image']='./assets/images/kirim_tugas/'.$gbr['file_name'];
						$config['create_thumb']= FALSE;
						$config['maintain_ratio']= FALSE;
						$config['quality']= '50%';
						$config['width']= 1500;
						$config['height']= 1700;
						$config['new_image']= './assets/images/kirim_tugas/'.$gbr['file_name'];
						$this->load->library('image_lib', $config);
						$this->image_lib->resize();

						$gambar = $gbr['file_name'];

						$datas = array(
							'id_tugas' 		=> $id_tugas,
							'nis' 			=> $nis,
							'nama_guru' 	=> $nama_guru,
							'nama_siswa'    => $nama_siswa,
							'id_kelompok'  	=> $id_kelompok,
							'jurusan'  		=> $jurusan,
							'tahun_angkatan'=> $tahun_angkatan,
							'judul_tugas'  	=> $judul_tugas,
							'pokok_1'  		=> $rangkuman,
							'm_file'  		=> $gambar,
							'nilai'  		=> $nilai,
							'status_tugas'  => $keterangan
							);
						$sukses = $this->db->insert('kotak_masuk', $datas);
						if($sukses == TRUE){
							$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Kirim</div>");
							redirect('Siswa/item_terkirim','refresh');

						}else{
							return FALSE;
							exit();
							redirect('Siswa/kirim','refresh');
						}
					}
					else
					{
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : jpg, JPG, jpeg, png, pdf, doc, xls ]</div>");
						redirect('Siswa/kirim','refresh');
					}
				}
			}
		}}

		function upload_image(){
			$config['upload_path'] = './assets/images/kirim_tugas/';
			$config['allowed_types'] = 'gif|jpg|png|JPG|jpeg|pdf|docx|doc|dock|xls';

			$this->upload->initialize($config);
			if(!empty($_FILES['filefoto']['name'])){

				if ($this->upload->do_upload('filefoto')){
					$gbr = $this->upload->data();

	            //Compress Image
					$config['image_library']='gd2';
					$config['source_image']='./assets/images/kirim_tugas/'.$gbr['file_name'];
					$config['create_thumb']= FALSE;
					$config['maintain_ratio']= FALSE;
					$config['quality']= '50%';
					$config['width']= 600;
					$config['height']= 400;
					$config['new_image']= './assets/images/kirim_tugas/'.$gbr['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$gambar=$gbr['file_name'];
					$judul=$this->input->post('xjudul');
					$this->m_upload->simpan_upload($judul,$gambar);
					echo "Image berhasil diupload";
				}

			}else{
				echo "Image yang diupload kosong";
			}

		}
		public function kotak_masuk(){
			$data['nis'] = $this->session->userdata('nis');
			$data['content'] 		= 'daftar_tugas_siswa';
			$this->load->view('view_siswa',$data);
			unset($data);

		}

		public function update_profile()
		{ 	
			$idAccess = $this->session->userdata('id');
			$key = $this->session->userdata('username');

			$this->db->select('nim');    
			$this->db->from('mahasiswa');
			$this->db->where('mahasiswa.nim',$key);
			$cek_db = $this->db->count_all_results();

			$nim = $this->input->post('nim');


			$nmfile 				= $_FILES['filefoto']['name'];
			$path   				= './assets/images/profile_mhs/';
			$config['upload_path'] 	= $path;
			$config['allowed_types']= 'jpg|png|jpeg|bmp|gif';
			$config['max_size']		= '200';
			$config['file_name'] 	= $nmfile;

			$this->upload->initialize($config);

			//JIKA ADA DI DATABASE
			if($cek_db != 0){
			//UPDATE data dengan upload Foto
				if($_FILES['filefoto']['name'])
				{
					if ($this->upload->do_upload('filefoto'))
					{	
						$gbr = $this->upload->data();

						$data_akses = array(
							'password' => $this->input->post('password')
							);

						$this->mdl->getUpdate(array('id' => $idAccess), $data_akses);

						$data = array(
							'foto' 			=> $gbr['file_name'],
							'nim'			=> $this->input->post('nim'),
							'tahun' 		=> $this->input->post('angkatan'),
							'nama' 			=> $this->input->post('nama'),
							'idJurusan' 	=> $this->input->post('jurusan'),
							'jenis_kelamin' => $this->input->post('jenis_kelamin'),
							'idAkses' 		=> $idAccess
							);

						$row = $this->db->where('nim',$nim)->get('mahasiswa')->row();
						unlink('assets/images/profile_mhs/'.$row->foto);

						$this->mdl->updateMahasiswa(array('nim' => $nim), $data);

						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di update</div>");
						redirect('Siswa/profile');
					}else{
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload Foto [Max.Size 200Kb] [Types : jpg, png, jpeg, bmp, gif ]</div>");
						redirect('Siswa/profile');
					}
				}
			//UPDATE data TANPA upload Foto
				else if(empty($_FILES['filefoto']['name']))
				{
					$data_akses = array(
						'password' => $this->input->post('password')
						);

					$this->mdl->getUpdate(array('id' => $idAccess), $data_akses);

					$data = array(
						// 'nim'			=> $this->input->post('nim'),
						'tahun' 		=> $this->input->post('angkatan'),
						'nama' 			=> $this->input->post('nama'),
						'idJurusan' 	=> $this->input->post('jurusan'),
						'jenis_kelamin' => $this->input->post('jenis_kelamin'),
						'idAkses' 		=> $idAccess
						);
					// echo json_encode($data_akses);
					// echo json_encode($data);
					// exit();

					$this->mdl->updateMahasiswa(array('nim' => $nim), $data);

					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di update</div>");
					redirect('Siswa/profile');
				}
			}
			//JIKA TIDAK ADA DI DATABASE
			else{
				//UPDATE data dengan upload Foto
				if($_FILES['filefoto']['name'])
				{
					if ($this->upload->do_upload('filefoto'))
					{	
						$gbr = $this->upload->data();

						// $data = array(
						// 	'foto' 			=> $gbr['file_name'],
						// 	'nim'			=> $this->input->post('nim'),
						// 	'tahun' 		=> $this->input->post('angkatan'),
						// 	'nama' 			=> $this->input->post('nama'),
						// 	'idJurusan' 	=> $this->input->post('jurusan'),
						// 	'jenis_kelamin' => $this->input->post('jenis_kelamin'),
						// 	'password' 		=> $this->input->post('password')
						// 	);

						// $this->db->insert('mahasiswa',$data);

						$data = array(
							'foto' 			=> $gbr['file_name'],
							'nim'			=> $this->input->post('nim'),
							'tahun' 		=> $this->input->post('angkatan'),
							'nama' 			=> $this->input->post('nama'),
							'idJurusan' 	=> $this->input->post('jurusan'),
							'jenis_kelamin' => $this->input->post('jenis_kelamin'),
							'idAkses' 		=> $idAccess
							);

						$this->db->insert('mahasiswa',$data);

						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di simpan</div>");
						redirect('Siswa/profile');
					}else{
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload Foto [Max.Size 200Kb] [Types : jpg, png, jpeg, bmp, gif ]</div>");
						redirect('Siswa/profile');
					}
				}
			//UPDATE data TANPA upload Foto
				else if(empty($_FILES['filefoto']['name']))
				{
					$data = array(
						'nim'			=> $this->input->post('nim'),
						'tahun' 		=> $this->input->post('angkatan'),
						'nama' 			=> $this->input->post('nama'),
						'idJurusan' 	=> $this->input->post('jurusan'),
						'jenis_kelamin' => $this->input->post('jenis_kelamin'),
						'idAkses' 		=> $idAccess
						);

					$this->db->insert('mahasiswa',$data);
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di simpan</div>");
					redirect('Siswa/profile');
				}
			}
			unset($idAccess,$key,$cek_db,$nim,$data);
		}

		public function proses_kirim_universitas(){
			$this->load->library('form_validation');

			$id_tugas 		= $this->input->post('id_tugas');
			$nis 			= $this->input->post('nim');
			$nilai			= $this->input->post('nilai');
			$keterangan		= $this->input->post('keterangan');
			$nama_siswa		= $this->input->post('nama_siswa');
			$tahun_angkatan	= $this->input->post('tahun_angkatan');
			$jurusan		= $this->input->post('jurusan');
			$id_kelompok	= $this->input->post('id_kelompok');
			$nama_guru 		= $this->input->post('nama_guru');
			$judul_tugas	= $this->input->post('judul_tugas');
			$rangkuman		= $this->input->post('rangkuman');

			$data;
			$getData = $this->db->get_where('kotak_masuk',array('nis'=>$nis,'id_tugas'=>$id_tugas))->result();
			// $getData = $this->db->get_where('kotak_masuk',array('nis'=>14650001,'id_tugas'=>49))->result();
			$count = count($getData);

			if($count==0){

				$data = array(
					'id_tugas' 		=> $id_tugas,
					'nis' 			=> $nis,
					'nama_guru' 	=> $nama_guru,
					'nama_siswa'    => $nama_siswa,
					'id_kelompok'  	=> $id_kelompok,
					'jurusan'  		=> $jurusan,
					'tahun_angkatan'=> $tahun_angkatan,
					'judul_tugas'  	=> $judul_tugas,
					'pokok_1'  		=> $rangkuman,
					'nilai'  		=> $nilai,
					'status_tugas'  => $keterangan
					);

				$sukses = $this->db->insert('kotak_masuk', $data);
				if($sukses == TRUE){
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Kirim</div>");
					redirect('Siswa/item_terkirim','refresh');
					unset($id_tugas,$nis,$nilai,$keterangan,$nama_siswa,$tahun_angkatan,$jurusan,$id_kelompok,$nama_guru,$judul_tugas,$rangkuman,$getData,$count,$data);
				}else{
					return FALSE;
					exit();
					redirect('Siswa/kirim','refresh');

				}
			}else{
				?>
				<script type="text/javascript">
					alert('Gagal kirim tugas..Data sudah ada');window.location='kotak_masuk';
				</script>
				<?php
			}
		}

		public function proses_kirim_fakultas(){
			$this->load->library('form_validation');

			$id_tugas 		= $this->input->post('id_tugas');
			$nis 			= $this->input->post('nim');
			$nilai			= $this->input->post('nilai');
			$keterangan		= $this->input->post('keterangan');
			$nama_siswa		= $this->input->post('nama_siswa');
			$tahun_angkatan	= $this->input->post('tahun_angkatan');
			$jurusan		= $this->input->post('jurusan');
			$id_kelompok	= $this->input->post('id_kelompok');
			$nama_guru 		= $this->input->post('nama_guru');
			$judul_tugas	= $this->input->post('judul_tugas');
			$rangkuman		= $this->input->post('rangkuman');

			$data;
			$getData = $this->db->get_where('kotak_masuk',array('nis'=>$nis,'id_tugas'=>$id_tugas))->result();
			// $getData = $this->db->get_where('kotak_masuk',array('nis'=>14650001,'id_tugas'=>49))->result();
			$count = count($getData);

			if($count==0){

				$data = array(
					'id_tugas' 		=> $id_tugas,
					'nis' 			=> $nis,
					'nama_siswa'    => $nama_siswa,
					'jurusan'  		=> $jurusan,
					'tahun_angkatan'=> $tahun_angkatan,
					'judul_tugas'  	=> $judul_tugas,
					'pokok_1'  		=> $rangkuman,
					'm_file'  		=> $m_file,
					'nilai'  		=> $nilai,
					'status_tugas'  => $keterangan,
					'idPendampungFk'=> $nama_guru,
					'idKelompokFk'  => $id_kelompok
					);

				$sukses = $this->db->insert('kotak_masuk', $data);
				if($sukses == TRUE){
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Kirim</div>");
					redirect('Siswa/item_terkirim','refresh');
					unset($id_tugas,$nis,$nilai,$keterangan,$nama_siswa,$tahun_angkatan,$jurusan,$id_kelompok,$nama_guru,$judul_tugas,$rangkuman,$getData,$count,$data);
				}else{
					return FALSE;
					exit();
					redirect('Siswa/kirim_fk','refresh');

				}
			}else{
				?>
				<script type="text/javascript">
					alert('Gagal kirim tugas..Data sudah ada');window.location='kotak_masuk';
				</script>
				<?php
			}
		}
	}
