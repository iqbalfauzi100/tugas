<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class PendampingFk extends CI_Controller {

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));
	
	
	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('pendampingFk/m_anggota_kelompok','model');
	// 		$this->load->model('pendampingFk/m_daftar_anggota2','mdl');
	// 		// $this->load->model('m_daftar_tugas','mod');
	
	// 		$this->load->model('pendampingFk/m_daftar_tugas_penugasan','mod');
	// 		$this->load->model('pendampingFk/m_total_nilai_pendamping','model_total');

	// 		$this->load->model('pendampingFk/m_kotak_masuk','kotak');
	// 		$this->load->model('pendampingFk/m_daftar_kotak_masuk','daftar_kotak');

	// 		$this->load->model('m_item_terkirim','item_terkirim');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('pendampingFk/m_anggota_kelompok','model');
		$this->load->model('pendampingFk/m_daftar_anggota2','mdl');
			// $this->load->model('m_daftar_tugas','mod');
		
		$this->load->model('pendampingFk/m_daftar_tugas_penugasan','mod');
		$this->load->model('pendampingFk/m_total_nilai_pendamping','model_total');

		$this->load->model('pendampingFk/m_kotak_masuk','kotak');
		$this->load->model('pendampingFk/m_daftar_kotak_masuk','daftar_kotak');

		$this->load->model('m_item_terkirim','item_terkirim');
	}

	public function index()
	{	
		$data['username']=$this->session->userdata('username');

		$data['content'] 	 = 	'pendampingFk/v_homeguru';
		$this->load->view('pendampingFk/view_admin',$data);
	}
	public function home()
	{	
		$data['username']=$this->session->userdata('username');

		$data['content'] 	 = 	'pendampingFk/v_homeguru';
		$this->load->view('pendampingFk/view_admin',$data);
	}
	public function anggota()
	{	
		$data['username'] = $this->session->userdata('username');

		$id =  $this->session->userdata('id');

		$sql = "SELECT pendamping_fk.*,kelompok_fk.idFk,kelompok_fk.namaKelompok from akses,pendamping_fk,kelompok_fk where akses.id = pendamping_fk.idAkses and pendamping_fk.idKelompokFk=kelompok_fk.id AND akses.id='$id'";
		$getData = $this->db->query($sql)->row();

		$namaKelompok = $getData->namaKelompok;

		if($namaKelompok != null){
			$data['namaKelompok'] = $namaKelompok;
		}else{
			$data['namaKelompok'] = "ok";
		}

		$data['content'] 	 = 	'pendampingFk/v_anggota';
		$this->load->view('pendampingFk/view_admin',$data);
	}
	public function daftar_anggota()
	{	
		$data['username'] = $this->session->userdata('username');
		$data['content'] 	 = 	'pendampingFk/v_daftar_anggota';
		$this->load->view('pendampingFk/view_admin',$data);
	}

	public function daftar_tugas(){
		$data['username']	= $this->session->userdata('username');
		$data['content'] 	 = 	'pendampingFk/daftar_tugas_pendamping';
		$this->load->view('pendampingFk/view_admin',$data);
	}
	
	public function verifikasi(){
		$data['username']	= $this->session->userdata('username');
		$data['content'] 	 = 	'pendampingFk/kotak_masukguru';
		$this->load->view('pendampingFk/view_admin',$data);
	}
	public function total_nilai_pendamping(){
		$data['username']	= $this->session->userdata('username');
		$data['content'] 	 = 	'pendampingFk/total_nilai_pendamping';
		$this->load->view('pendampingFk/view_admin',$data);
	}

	public function datatable()
	{
		$list = $this->model->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->jurusan;
			$row[] = $model->idKelompokFk;

			$kel = $model->idKelompokFk;
			if ($kel != 0) {
				$row[] = $model->namaKelompok;
			}elseif ($kel == 0) {
				$row[] = 'Belum ada kelompok';
			}
			

			$id =  $this->session->userdata('id');
			$query = "SELECT pendamping_fk.*,kelompok_fk.idFk from akses,pendamping_fk,kelompok_fk where akses.id = pendamping_fk.idAkses and pendamping_fk.idKelompokFk=kelompok_fk.id AND akses.id='$id'";
			$sql   = $this->db->query($query)->row();
			$idKelompokFk = $sql->idKelompokFk;

			if ($model->idKelompokFk == $idKelompokFk) {
				$row[] = '
				<a class="btn btn-sm btn-success" href="javascript:void()" name="idPengaturan" title="Change" onclick="seleksi('."'".$model->id."'".','."'".$model->idKelompokFk."'".');"><i class="glyphicon glyphicon-ok"></i> </a>
				';
			}else{
				$row[] = '
				<a class="btn btn-sm btn-danger" href="javascript:void()" name="idPengaturan" title="Change" onclick="seleksi('."'".$model->id."'".','."'".$model->idKelompokFk."'".');"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function datatable2()
	{
		$sess = $this->session->userdata('id_kelompok');
		$list = $this->mdl->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->jurusan;
			$row[] = $model->namaKelompok;
			$row[] = $model->nama_pendamping;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mdl->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function datatable22()
	{
		$fetch_data = $this->mdl->get_datatables();
		$data = array();
		$nmr = 0;
		foreach ($fetch_data as $row) {
			$nmr += 1;
			$sub_array = array();
			$sub_array[] = $nmr;
			$sub_array[] = $row->nim;
			$sub_array[] = $row->nama;
			$sub_array[] = $row->jurusan;
			$sub_array[] = $row->namaKelompok;
			$sub_array[] = $row->nama_pendamping;
			$data[] = $sub_array;
		}

		$output = array(
			"draw" => intval($_POST["draw"]),
			"recordsTotal" => $nmr,
			"recordsFiltered" => $this->mdl->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function datatable_dataTugas()
	{
		$list = $this->mod->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->judul;
			$row[] = $model->tgl;
			$row[] = $model->berlaku;
			$row[] = '
			<a class="btn btn-sm btn-primary" href="assets/images/penugasan/'.$model->m_file.'" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
			</a>
			';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mod->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function datatable_kotak_masuk()
	{
		$list = $this->kotak->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nis;
			$row[] = $model->nama_siswa;
			$row[] = $model->namaKelompok;
			$row[] = $model->judul_tugas;
			$row[] = $model->pokok_1;
			$row[] = '
			<a class="btn btn-sm btn-primary" onclick="view_detail_upload('.$model->nis.','.$model->id_tugas.')" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i>
			</a>
			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus"onclick="delete_person('.$model->id_kotakmasuk.','.$model->nis.','.$model->id_tugas.')" >
				<i class="glyphicon glyphicon-trash"></i>
			</a>
			
			';
			if ($model->keterangan == 1) {
				$row[] = '
				<a class="btn btn-sm btn-success" href="javascript:void()" name="idPengaturan" title="Verifikasi" onclick="seleksi('."'".$model->id_kotakmasuk."'".','."'".$model->keterangan."'".');"><i class="glyphicon glyphicon-ok"></i> </a>
				';
			}elseif ($model->keterangan == 0) {
				$row[] = '
				<a class="btn btn-sm btn-warning" href="javascript:void()" name="idPengaturan" title="Verifikasi" onclick="seleksi('."'".$model->id_kotakmasuk."'".','."'".$model->keterangan."'".');"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->kotak->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function view_detail_upload($nim, $id_tugas)
	{
		$data = $this->item_terkirim->view_detail_upload($nim, $id_tugas);
		echo json_encode($data);
	}
	public function datatable_data_kotak_masuk()
	{
		$list = $this->daftar_kotak->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nis;
			$row[] = $model->nama_siswa;
			$row[] = $model->namaKelompok;
			$row[] = $model->judul_tugas;
			$row[] = $model->pokok_1;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->daftar_kotak->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function addAnggota($idAkses, $id_kelompok)
	{
		$id =  $this->session->userdata('id');

		$query = "SELECT pendamping_fk.*,kelompok_fk.idFk from akses,pendamping_fk,kelompok_fk where akses.id = pendamping_fk.idAkses and pendamping_fk.idKelompokFk=kelompok_fk.id AND akses.id='$id'";
		$sql   = $this->db->query($query)->row();

		$idPen = $sql->id; //101
		$idKelompok = $sql->idKelompokFk; //1

		$change_id;
		if ($id_kelompok==$idKelompok) {
			$change_id = 0;
			$idPen = 0;
		}else if ($id_kelompok!=$idKelompok) {
			$change_id = $idKelompok;
			$idPen = $idPen;
		}
		$data = array(
			'idKelompokFk' => $change_id,
			'idPendampungFk' => $idPen
			);
		// echo json_encode($data);
				// echo json_encode(array('idAkses' => $idAkses));
		$this->model->update_id(array('id' => $idAkses), $data);
		echo json_encode(array("status" => TRUE));
	}
	public function hapus_kelompok($id,$nim,$id_tugas)
	{	
				//delete file
		$data;
		$getData = $this->db->get_where('kotak_masuk_upload',array('nim'=>$nim,'id_tugas'=>$id_tugas));
		foreach ($getData->result() as $row) {
			$data[] = $row->title;
		}

		$count = count($data);

		for ($i=0; $i < $count; $i++) {
			if(file_exists('assets/images/kirim_tugas/'.$data[$i]) && $data[$i])
				unlink('assets/images/kirim_tugas/'.$data[$i]);
		}

				// $this->kotak->delete_by_id($id);
		$this->db->delete('kotak_masuk', array('id_kotakmasuk' => $id));
		$this->db->delete('kotak_masuk_upload', array('nim' => $nim,'id_tugas' => $id_tugas));
		echo json_encode(array("status" => TRUE));
	}

	public function profile_pendampingfk(){
		$key = $this->session->userdata('id');

		$query = "SELECT pendamping_fk.*,kelompok_fk.idFk,kelompok_fk.namaKelompok,akses.username,akses.password from akses,pendamping_fk,kelompok_fk where akses.id = pendamping_fk.idAkses and pendamping_fk.idKelompokFk=kelompok_fk.id AND akses.id='$key'";
		$sql   = $this->db->query($query)->row();

		$this->db->select('id');    
		$this->db->from('pendamping_fk');
		$this->db->where('pendamping_fk.idAkses',$key);
		$cek_db = $this->db->count_all_results();

		if($cek_db != 0){
			$data =array(
				'id'		=> $sql->id,
				'idAkses'	=> $sql->idAkses,
				'username'	=> $sql->username,
				'password'	=> $sql->password,
				'nama'		=> $sql->nama,
				'idKelompok' => $sql->idKelompokFk
				);
		}else{
			$data =array(
				'id'		=> "",
				'idAkses'	=> "",
				'username'	=> "",
				'password'	=> "",
				'nama'		=> "",
				'idKelompok' => ""
				);
		}
		$data['content'] 		= 'pendampingFk/v_profile_pendamping';
		$this->load->view('pendampingFk/view_admin',$data);
	}

	public function update_profile(){
		$this->load->library('form_validation');
		
		$id 		= $this->input->post('id');
		$idAkses 	= $this->input->post('idAkses');
		$nama 		= $this->input->post('nama');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$idKelompok 	= $this->input->post('idKelompok');

		$data_akses = array(
			'username' => $username,
			'password' => $password
			);

		$this->model->getUpdate(array('id' => $idAkses), $data_akses);

		$data = array(
			'nama' => $nama
			);

		$this->model->pendamping_fk(array('id' => $id), $data);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Update</div>");
		redirect('pendampingFk/PendampingFk/profile_pendampingfk','refresh');
	}
	public function datatable_total_nilai()
	{
		
		$list = $this->model_total->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->jurusan;
			$count = $this->model_total->count_score($model->nim)->nilai;
			$row[] = '<a href="javascript:void(0)" onclick="view_detail_score('.$model->nim.')">'.$count.'</a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model_total->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function view_detail_score($nis)
	{
		$data = $this->model_total->view_detail_score($nis);
		echo json_encode($data);
	}
}


