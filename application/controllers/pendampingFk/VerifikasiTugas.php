<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class VerifikasiTugas extends CI_Controller {

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));
		
	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 	$this->load->model('pendampingFk/m_anggota_kelompok','model');
	// 	$this->load->model('pendampingFk/m_daftar_anggota2','mdl');
	// 		// $this->load->model('m_daftar_tugas','mod');
		
	// 	$this->load->model('pendampingFk/m_daftar_tugas_penugasan','mod');
	// 	$this->load->model('pendampingFk/m_total_nilai_pendamping','model_total');

	// 	$this->load->model('pendampingFk/m_kotak_masuk','kotak');
	// 	$this->load->model('pendampingFk/m_daftar_kotak_masuk','daftar_kotak');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('pendampingFk/m_anggota_kelompok','model');
		$this->load->model('pendampingFk/m_daftar_anggota2','mdl');
			// $this->load->model('m_daftar_tugas','mod');
		
		$this->load->model('pendampingFk/m_daftar_tugas_penugasan','mod');
		$this->load->model('pendampingFk/m_total_nilai_pendamping','model_total');

		$this->load->model('pendampingFk/m_kotak_masuk','kotak');
		$this->load->model('pendampingFk/m_daftar_kotak_masuk','daftar_kotak');
	}

	public function index(){
		$data['content'] 	 = 	'pendampingFk/kotak_masukguru';
		$this->load->view('pendampingFk/view_admin',$data);
	}
	public function datatable_kotak_masuk()
	{
		$list = $this->kotak->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nis;
			$row[] = $model->nama_siswa;
			$row[] = $model->idKelompokFk;
			$row[] = $model->judul_tugas;
			$row[] = $model->pokok_1;
			$row[] = '
			<a class="btn btn-sm btn-primary" href="assets/images/kirim_tugas/'.$model->m_file.'" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i>
			</a>
			
			';
			if ($model->keterangan == 1) {
				$row[] = '
				<a class="btn btn-sm btn-success" href="javascript:void()" name="idPengaturan" title="Verifikasi" onclick="seleksi('."'".$model->id_kotakmasuk."'".','."'".$model->keterangan."'".');"><i class="glyphicon glyphicon-ok"></i> </a>
				';
			}elseif ($model->keterangan == 0) {
				$row[] = '
				<a class="btn btn-sm btn-warning" href="javascript:void()" name="idPengaturan" title="Verifikasi" onclick="seleksi('."'".$model->id_kotakmasuk."'".','."'".$model->keterangan."'".');"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->kotak->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function datatable_data_kotak_masuk()
	{
		$list = $this->daftar_kotak->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nis;
			$row[] = $model->nama_siswa;
			$row[] = $model->idKelompokFk;
			$row[] = $model->judul_tugas;
			$row[] = $model->pokok_1;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->daftar_kotak->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	
	public function changeKeterangan($id, $status)
	{
		$change_status;
		if ($status==1) {
			$change_status = 0;
		}else if ($status==0) {
			$change_status = 1;
		}
		$data = array(
			'keterangan' => $change_status
			);
		$this->kotak->change_status(array('id_kotakmasuk' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}

}


