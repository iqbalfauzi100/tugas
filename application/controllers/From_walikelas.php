<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class From_walikelas extends CI_Controller {

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));
	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('m_anggota_kelompok','model');
	// 		$this->load->model('m_daftar_anggota2','mdl');
	// 		// $this->load->model('m_daftar_tugas','mod');

	// 		$this->load->model('m_daftar_tugas_penugasan','mod');
	// 		$this->load->model('m_total_nilai_pendamping','model_total');

	// 		$this->load->model('m_kotak_masuk','kotak');
	// 		$this->load->model('m_daftar_kotak_masuk','daftar_kotak');

	// 		$this->load->model('m_item_terkirim','item_terkirim');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();

		$this->load->library('Loginauth');
		$this->loginauth->view_page();

		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('m_anggota_kelompok','model');
		$this->load->model('m_daftar_anggota2','mdl');
			// $this->load->model('m_daftar_tugas','mod');
		$this->load->model('m_daftar_tugas_penugasan','mod');
		$this->load->model('m_total_nilai_pendamping','model_total');

		$this->load->model('m_kotak_masuk','kotak');
		$this->load->model('m_daftar_kotak_masuk','daftar_kotak');

		$this->load->model('m_item_terkirim','item_terkirim');
	}

	public function index()
	{	
		$data['username']=$this->session->userdata('username');

		$data['content'] 	 = 	'v_homeguru';
		$this->load->view('view_admin',$data);
	}
	public function home()
	{	
		$data['username']=$this->session->userdata('username');

		$data['content'] 	 = 	'v_homeguru';
		$this->load->view('view_admin',$data);
	}
	public function anggota()
	{	
		$data['username'] = $this->session->userdata('username');
		$sess_idKelompok = $this->session->userdata('id_kelompok');
		if($sess_idKelompok !=0){
			$data['nama_kelompok'] = $this->model->getdata($sess_idKelompok)->kelompok;
		}else{
			$data['nama_kelompok'] = "";
		}

		$data['content'] 	 = 	'v_anggota';
		$this->load->view('view_admin',$data);
	}
	public function daftar_anggota()
	{	
		$data['username'] = $this->session->userdata('username');
		$data['content'] 	 = 	'v_daftar_anggota';
		$this->load->view('view_admin',$data);
	}
	public function datatable()
	{
		$sess = $this->session->userdata('id_kelompok');
		$sess_idPendamping = $this->session->userdata('id');
		
		$list = $this->model->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->jurusan;
			$row[] = $model->idKelompokU;

			$kel = $model->idKelompokU;
			if ($kel != 0) {
				$row[] = $this->model->getdata($kel)->kelompok;
			}elseif ($kel == 0) {
				$row[] = 'Belum ada kelompok';
			}
			

			$id = $this->session->userdata('id');
			$query = "SELECT pendamping_univ.id,pendamping_univ.idKelompok from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$id'";
			$sql   = $this->db->query($query)->row();
			$idKelompok = $sql->idKelompok;

			if ($model->idKelompokU == $idKelompok) {
				$row[] = '
				<a class="btn btn-sm btn-success" href="javascript:void()" name="idPengaturan" title="Change" onclick="seleksi('."'".$model->id."'".','."'".$model->idKelompokU."'".');"><i class="glyphicon glyphicon-ok"></i> </a>
				';
			}else{
				$row[] = '
				<a class="btn btn-sm btn-danger" href="javascript:void()" name="idPengaturan" title="Change" onclick="seleksi('."'".$model->id."'".','."'".$model->idKelompokU."'".');"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function datatable2()
	{
		$sess = $this->session->userdata('id_kelompok');
		$list = $this->mdl->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->jurusan;
			$row[] = $model->kelompok;
			$row[] = $model->nama_pendamping;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mdl->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function datatable22()
	{
		$fetch_data = $this->mdl->get_datatables();
		$data = array();
		$nmr = 0;
		foreach ($fetch_data as $row) {
			$nmr += 1;
			$sub_array = array();
			$sub_array[] = $nmr;
			$sub_array[] = $row->nim;
			$sub_array[] = $row->nama;
			$sub_array[] = $row->jurusan;
			$sub_array[] = $row->kelompok;
			$sub_array[] = $row->nama_pendamping;
			$data[] = $sub_array;
		}

		$output = array(
			"draw" => intval($_POST["draw"]),
			"recordsTotal" => $nmr,
			"recordsFiltered" => $this->mdl->count_filtered(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function datatable_kotak_masuk()
	{
		$sess = $this->session->userdata('id_kelompok');
		$list = $this->kotak->get_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nis;
			$row[] = $model->nama_siswa;
			$row[] = $model->kelompok;
			$row[] = $model->judul_tugas;
			$row[] = $model->pokok_1;
			$row[] = '
			<a class="btn btn-sm btn-primary" onclick="view_detail_upload('.$model->nis.','.$model->id_tugas.')" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i>
			</a>
			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus"
			onclick="delete_person('.$model->id_kotakmasuk.','.$model->nis.','.$model->id_tugas.')" title="Lihat File" target="_blank">
			<i class="glyphicon glyphicon-trash"></i>
		</a>
		';
		if ($model->keterangan == 1) {
			$row[] = '
			<a class="btn btn-sm btn-success" href="javascript:void()" name="idPengaturan" title="Verifikasi" onclick="seleksi('."'".$model->id_kotakmasuk."'".','."'".$model->keterangan."'".');"><i class="glyphicon glyphicon-ok"></i> </a>
			';
		}elseif ($model->keterangan == 0) {
			$row[] = '
			<a class="btn btn-sm btn-warning" href="javascript:void()" name="idPengaturan" title="Verifikasi" onclick="seleksi('."'".$model->id_kotakmasuk."'".','."'".$model->keterangan."'".');"><i class="glyphicon glyphicon-remove"></i> </a>
			';
		}
		$data[] = $row;
	}

	$output = array(
		"draw" => $_POST['draw'],
		"recordsTotal" => $no,
		"recordsFiltered" => $this->kotak->count_filtered(),
		"data" => $data,
		);
	echo json_encode($output);
}
	/*
	<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" 
			onclick="delete_person('."'".$model->id_kotakmasuk."'".')">
				<i class="glyphicon glyphicon-trash"></i>
			</a>
	<a class="btn btn-sm btn-primary" href="assets/images/kirim_tugas/'.$model->m_file.'" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i>
			</a>
	<a class="btn btn-sm btn-danger" href="index.php/from_walikelas/hapus_kotakmasuk/'.$model->id_kotakmasuk.'" title="Hapus">
				<i class="glyphicon glyphicon-trash"></i>
			</a>*/

			public function view_detail_upload($nim, $id_tugas)
			{
				$data = $this->item_terkirim->view_detail_upload($nim, $id_tugas);
				echo json_encode($data);
			}

			public function datatable_data_kotak_masuk()
			{
				$sess = $this->session->userdata('id_kelompok');
				$list = $this->daftar_kotak->get_datatables();
				$data = array();
				$no = 0;
				foreach ($list as $model) {
					$no+=1;
					$row = array();
					$row[] = $no;
					$row[] = $model->nis;
					$row[] = $model->nama_siswa;
					$row[] = $model->kelompok;
					$row[] = $model->judul_tugas;
					$row[] = $model->pokok_1;
					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $no,
					"recordsFiltered" => $this->daftar_kotak->count_filtered(),
					"data" => $data,
					);
				echo json_encode($output);
			}


			public function datatable_dataTugas()
			{
				$list = $this->mod->get_datatables();
				$data = array();
				$no = 0;
				foreach ($list as $model) {
					$no+=1;
					$row = array();
					$row[] = $no;
					$row[] = $model->judul;
					$row[] = $model->tgl;
					$row[] = $model->berlaku;
					$row[] = '
					<a class="btn btn-sm btn-primary" href="assets/images/penugasan/'.$model->m_file.'" title="Lihat File" target="_blank">
						<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
					</a>
					';
					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $no,
					"recordsFiltered" => $this->model->count_filtered(),
					"data" => $data,
					);
				echo json_encode($output);
			}

			/*<a class="btn btn-sm btn-warning" href="index.php/from_walikelas/edit_tugas/'.$model->id_tugas.'" title="Edit">
				<i class="glyphicon glyphicon-pencil"></i>
			</a>

			<a class="btn btn-sm btn-danger" href="index.php/from_walikelas/hapus_tugas/'.$model->id_tugas.'" title="Hapus">
				<i class="glyphicon glyphicon-trash"></i>
			</a>*/

			public function addAnggota($idAkses, $id_kelompok)
			{
				// $sess = $this->session->userdata('id_kelompok');
				// $sess_idPendamping = $this->session->userdata('id');

				$id = $this->session->userdata('id');
				$query = "SELECT pendamping_univ.id,pendamping_univ.idKelompok from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$id'";

				$sql   = $this->db->query($query)->row();

				$idKelompok = $sql->idKelompok;
				$idPen 		= $sql->id;

				$change_id;
				if ($id_kelompok==$idKelompok) {
					$change_id = 0;
					$idPen = 0;
				}else if ($id_kelompok != $idKelompok) {
					$change_id = $idKelompok;
					$idPen = $idPen;
				}
				$data = array(
					'idKelompokU' => $change_id,
					'idPendampungU' => $idPen
					);
				// echo json_encode(array('idAkses' => $idAkses));
				$this->model->update_id(array('id' => $idAkses), $data);
				echo json_encode(array("status" => TRUE));
			}
			public function changeKeterangan($id, $status)
			{
				$change_status;
				if ($status==1) {
					$change_status = 0;
				}else if ($status==0) {
					$change_status = 1;
				}
				$data = array(
					'keterangan' => $change_status
					);
				$this->kotak->change_status(array('id_kotakmasuk' => $id), $data);
				echo json_encode(array("status" => TRUE));
			}

			public function hapus_kelompok($id,$nim,$id_tugas)
			{	
				//delete file
				$data;
				$getData = $this->db->get_where('kotak_masuk_upload',array('nim'=>$nim,'id_tugas'=>$id_tugas));
				foreach ($getData->result() as $row) {
					$data[] = $row->title;
				}

				$count = count($data);

				for ($i=0; $i < $count; $i++) {
					if(file_exists('assets/images/kirim_tugas/'.$data[$i]) && $data[$i])
						unlink('assets/images/kirim_tugas/'.$data[$i]);
				}

				// $this->kotak->delete_by_id($id);
				$this->db->delete('kotak_masuk', array('id_kotakmasuk' => $id));
				$this->db->delete('kotak_masuk_upload', array('nim' => $nim,'id_tugas' => $id_tugas));
				echo json_encode(array("status" => TRUE));
			}


			public function laporan(){
				$data['username']=$this->session->userdata('username');
				$this->load->view('laporan_guru',$data);
			}

			public function kotak_masuk(){
				$data['id_kelompok']	= $this->session->userdata('id_kelompok');
				$data['data']		 = $this->db->get_where('kotak_masuk',$data);
				$data['username']	= $this->session->userdata('username');
				$data['content'] 	 = 	'kotak_masukguru';
				$this->load->view('view_admin',$data);
			}
			public function total_nilai_pendamping(){
				$data['id_kelompok']	= $this->session->userdata('id_kelompok');

				$data['username']	= $this->session->userdata('username');
				$data['content'] 	 = 	'total_nilai_pendamping';
				$this->load->view('view_admin',$data);
			}
			public function item_terkirim(){
				if($this->session->userdata('username')){
					$data['nama_guru']	= $this->session->userdata('username');
					$data['data']		 = $this->db->get_where('tbl_tugas',$data);
					$data['username']	= $this->session->userdata('username');
					$data['content'] 	 = 	'item_terkirimguru';
					$this->load->view('view_admin',$data);
				}else{
					$this->login();
				}
			}
			public function daftar_tugas(){
				$data['nama_guru']	= $this->session->userdata('username');

				$data['username']	= $this->session->userdata('username');
				$data['content'] 	 = 	'daftar_tugas_pendamping';
				$this->load->view('view_admin',$data);
			}
			public function edit_tugas($id_tugas){
				$data['id_tugas']    =  $id_tugas;
				$data['data']        =  $this->db->get_where('tbl_tugas',$data);
				$data['username']	 = $this->session->userdata('username');
				$data['content'] 	 = 	'edit_tugas';
				$this->load->view('view_admin',$data);
			}

			public function profile(){
				if($this->session->userdata('nama_login')){
					$data['nama_wali']=$this->session->userdata('nama_login');

					$data['content'] 	 = 	'profile_wali';
					$this->load->view('view_admin',$data);
				}else{
					$this->login();
				}
			}	
			public function daftar_siswa()
			{	
				if($this->session->userdata('nama_login')){
					$data['nama_wali']=$this->session->userdata('nama_login');

					$data['data']		 =	$this->db->get('tbl_siswa');
				//$data['data_wali']		 =	$this->db->get_where('tbl_walikelas',$nama_wali);
					$data['content'] 	 = 	'view_siswatkr';
					$this->load->view('view_admin',$data);
				}else{
					$this->login();
				}
			}

			public function hapus_tugas($id_tugas){

				$data['id_tugas']	= $id_tugas;
				$sql			= $this->db->delete('tbl_tugas',array('id_tugas'=>$id_tugas));
				if($sql){
					?>	
					<script type="text/javascript">
						alert("sukses di hapus data <?php echo $id_tugas ?>");window.location="<?php echo base_url(); ?>index.php/from_walikelas/daftar_tugas";
					</script>
					<?php	
				}else{
					?>	
					<script type="text/javascript">
						alert("GAGAL di hapus data <?php echo $id_kontak ?>");window.location="<?php echo base_url(); ?>index.php/from_walikelas/daftar_tugas";
					</script>
					<?php	
				}

			}


			public function hapus_kotakmasuk($id_kotakmasuk){

				$data['id_kotakmasuk']	= $id_kotakmasuk;
				$sql			= $this->db->delete('kotak_masuk',array('id_kotakmasuk'=>$id_kotakmasuk));
				if($sql){
					?>	
					<script type="text/javascript">
						alert("sukses di hapus data <?php echo $id_kotakmasuk ?>");window.location="<?php echo base_url(); ?>index.php/from_walikelas/kotak_masuk";
					</script>
					<?php	
				}else{
					?>	
					<script type="text/javascript">
						alert("GAGAL di hapus data <?php echo $id_kontak ?>");window.location="<?php echo base_url(); ?>index.php/from_walikelas/kotak_masuk";
					</script>
					<?php	
				}

			}
			
			public function profile_pendamping(){
				$key = $this->session->userdata('id');

				$query = "SELECT akses.username,akses.password,pendamping_univ.* from akses,pendamping_univ where akses.id = pendamping_univ.idAkses and akses.id='$key'";
				$sql   = $this->db->query($query)->row();

				$this->db->select('id');    
				$this->db->from('pendamping_univ');
				$this->db->where('pendamping_univ.idAkses',$key);
				$cek_db = $this->db->count_all_results();

				if($cek_db != 0){
					$data =array(
						'id_guru'	=> $sql->idAkses,
						'username'	=> $sql->username,
						'password'	=> $sql->password,
						'nama'		=> $sql->nama,
						'foto'		=> $sql->foto,
						'idKelompok' 	=> $sql->idKelompok
						);
				}else{
					$data =array(
						'id_guru'	=> "",
						'username'	=> "",
						'password'	=> "",
						'nama'		=> "",
						'foto'		=> "",
						'idKelompok' => ""
						);
				}
				$data['content'] 		= 'v_profile_pendamping';
				$this->load->view('view_admin',$data);
			}

			public function update_profile()
			{ 	
				$id_guru = $this->input->post('id_guru');

				$nmfile 				= $_FILES['filefoto']['name'];
				$path   				= './assets/images/profile_pendamping/';
				$config['upload_path'] 	= $path;
				$config['allowed_types']= 'jpg|png|jpeg|bmp|gif';
				$config['max_size']		= '200';
				$config['file_name'] 	= $nmfile;

				$this->upload->initialize($config);

			//UPDATE data dengan upload Foto
				if($_FILES['filefoto']['name'])
				{
					if ($this->upload->do_upload('filefoto'))
					{	
						$gbr = $this->upload->data();

						$data_akses = array(
							'username' => $this->input->post('username'),
							'password' => $this->input->post('password')
							);

						$this->mod->getUpdate(array('id' => $id_guru), $data_akses);

						$data = array(
							'foto'		=> $gbr['file_name'],
							'nama'		=> $this->input->post('nama')
							);

						$sukses=$this->mod->updatePendampingUniv(array('idAkses' => $id_guru), $data);

						$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di update</div>");
						redirect('From_walikelas/profile_pendamping');
					}else{
						$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload Foto [Max.Size 200Kb] [Types : jpg, png, jpeg, bmp, gif ]</div>");
						redirect('From_walikelas/profile_pendamping');
					}
				}
			//UPDATE data TANPA upload Foto
				else if(empty($_FILES['filefoto']['name']))
				{

					$data_akses = array(
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password')
						);

					$this->mod->getUpdate(array('id' => $id_guru), $data_akses);

					$data = array(
						'nama'		=> $this->input->post('nama')
						);

					$sukses=$this->mod->updatePendampingUniv(array('idAkses' => $id_guru), $data);

					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di update</div>");
					redirect('From_walikelas/profile_pendamping');
				}
			}

			public function datatable_total_nilai()
			{

				$list = $this->model_total->get_datatables();
				$data = array();
				$no = 0;
				foreach ($list as $model) {
					$no+=1;
					$row = array();
					$row[] = $no;
					$row[] = $model->nim;
					$row[] = $model->nama;
					$row[] = $model->jurusan;
					
					$count = $this->model_total->count_score($model->nim)->nilai;
					$row[] = '<a href="javascript:void(0)" onclick="view_detail_score('.$model->nim.')">'.$count.'</a>';
					$data[] = $row;
				}

				$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $no,
					"recordsFiltered" => $this->model_total->count_filtered(),
					"data" => $data,
					);
				echo json_encode($output);
			}
			public function view_detail_score($nis)
			{
				$data = $this->model_total->view_detail_score($nis);
				echo json_encode($data);
			}


		}


