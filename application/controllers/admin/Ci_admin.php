<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0); //mematikan error
class Ci_admin extends CI_Controller {

	// function __construct(){
	// 	parent::__construct();
	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('m_setting_sistem','mdl');
	// 		$this->load->model('m_kelompok_mahasiswa','model');
	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	function __construct(){
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		
		$this->load->model('m_setting_sistem','mdl');
		$this->load->model('m_kelompok_mahasiswa','model');
	}
	
	public function index(){
		$data['user']=$this->session->userdata('username');
		$data['content'] 		= 'welcome_superadmin';
		$this->load->view('view_super_admin',$data);
	}

	public function laporan(){
		$data['content'] = 'v_lapguru';
		$data['data'] = $this->db->get('guru');
		$this->load->view('view_super_admin',$data);
	}
	public function setting_sistem(){
		$data['content'] = 'v_setting_sistem';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function pengelompokan(){
		$data['content'] = 'v_kelompok_mhs';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function redirect(){
		$this->load->view('v_redirect');
	}
	public function redirect_logout(){
		$this->load->view('v_redirect_logout');	
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('ci_admin');	
	}
	public function kontak(){
		$data['data']    = $this->db->get('kontak');
		$data['content'] = 'view_kontak';
		$this->load->view('view_super_admin',$data);
	}

	public function proses_addtugas(){
		$this->load->library('form_validation');

		$judul 	= $this->input->post('judul');
		$nama_guru 	= $this->input->post('nama_guru');
		$pelajaran			= $this->input->post('pelajaran');
		$kelas		= $this->input->post('kelas');
		$tgl			= $this->input->post('tgl');
		$m_file 		= $_FILES['userfile']['name'];

	// Konfigurasi Upload Gambar
		$config['upload_path']          = './application/views/photo';
		$config['allowed_types']        = 'gif|jpg|png|JPG|jpeg';
		$config['max_size']				= '1024';
		$config['max_width']  			= '1600';
		$config['max_height']  			= '1200';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());        
			$data = array(
				'judul' => $judul,
				'nama_guru' => $nama_guru,
				'pelajaran'     => $pelajaran,
				'kelas'  => $kelas,
				'tgl'        => $tgl,
				'm_file'   => $m_file
				);
			$sukses = $this->db->insert('tbl_tugas', $data);
			if($sukses == TRUE){
				redirect('ci_admin/form_barang','refresh');
				return $sukses;
			}else{
				return FALSE;
				exit();
				redirect('ci_admin/view_barang','refresh');

			}
		}

	}

	public function datatable_setting()
	{
		$list = $this->mdl->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->fitur;
			if ($model->status == 1) {
				$row[] = '
				<span class="label label-success"><i class="fa fa-clock-o"></i> Aktif</span>
				';
				$row[] = '
				<a class="btn btn-sm btn-success" href="javascript:void()" name="idPengaturan" title="Non Aktifkan" onclick="seleksi('."'".$model->id."'".','."'".$model->status."'".');"><i class="glyphicon glyphicon-ok"></i> </a>
				';
			}else {
				$row[] = '
				<span class="label label-danger"><i class="fa fa-clock-o"></i> Tidak Aktif</span>
				';
				$row[] = '
				<a class="btn btn-sm btn-danger" href="javascript:void()" name="idPengaturan" title="Aktifkan" onclick="seleksi('."'".$model->id."'".','."'".$model->status."'".');"><i class="glyphicon glyphicon-remove"></i> </a>
				';
			}
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mdl->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}

	public function changeStatus($id, $status)
	{
		$change_status;
		if ($status==1) {
			$change_status = 0;
		}else if ($status==0) {
			$change_status = 1;
		}
		$data = array(
			'status' => $change_status
			);
		$this->mdl->change_status(array('id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function datatable_kelompok()
	{
		$kelompok = $this->input->post('kelompok')?$this->input->post('kelompok'):0;
		$list = $this->model->make_datatables($kelompok);
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->nim;
			$row[] = $model->nama;
			$row[] = $model->jurusan;
			$row[] = $model->kelompok;
			$row[] = $model->nama_guru;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->model->get_filtered_data($kelompok),
			"data" => $data,
			);
		echo json_encode($output);
	}


}


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */