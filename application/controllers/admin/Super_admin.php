<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Super_admin extends CI_Controller {

	// function __construct(){
	// 	parent::__construct();
	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('m_anggota_kelompok','model');
	// 		$this->load->model('m_daftar_tugas','mod');
	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	function __construct(){
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		
		$this->load->model('m_anggota_kelompok','model');
		$this->load->model('m_daftar_tugas','mod');
	}

	public function index(){
		$data['user']	= $this->session->userdata('username');
		$data['content'] = 'welcome_superadmin';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}

	public function input_tugas(){
		$data['content'] = 'input_tugas';
		$this->load->view('view_super_admin',$data);
	}
	public function daftar_tugas(){
		$data['nama_guru']	= $this->session->userdata('user');
		$data['username']	= $this->session->userdata('user');
		$data['content'] 	 = 	'daftar_tugas';
		$this->load->view('view_super_admin',$data);
	}

	public function about(){
		$data['content'] = 'about';
		$this->load->view('view_super_admin',$data);
	}
	public function input_siswa(){
		$data['fakultas'] = $this->mod->dataFakultas();
		$data['content'] = 'v_inputsiswa';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function getJurusan() {
		$fakultas = $_GET['fakultas'];
		$getjur = $this->mod->get_jurusan($fakultas);
		echo json_encode($getjur);
	}
	public function pengelompokan(){
		$this->load->model("pengelompokan",'pnl');

		$data['content'] 	= 'v_pengelompokan';
		$data['mhs']		= $this->pnl->getMhs(false);
		/*$data['mhsMatrix']	= $this->pnl->getMhs();
		$data['jumlahMhs']	= $this->pnl->countMhs();*/
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function input_mapel(){
		$data['content'] = 'v_input_mapel';
		$this->load->view('view_super_admin',$data);
	}
	public function input_guru(){
		$data['content'] = 'v_inputguru';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function input_data_penugasan(){
		$data['content'] = 'admin/v_input_data_penugasan';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function input_data_penugasan_univ(){
		$data['content'] = 'admin/v_input_data_penugasan_univ';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function input_jurusan(){
		$data['content'] = 'v_inputjurusan';
		$this->load->view('view_super_admin',$data);
	}
	public function input_walikelas(){
		$data['content'] = 'v_input_walikelas';
		$this->load->view('view_super_admin',$data);
	}
	public function input_galeri(){
		$data['content'] = 'v_input_galeri';
		$this->load->view('view_super_admin',$data);
	}
	public function input_kelas(){
		$data['content'] = 'v_input_kelas';
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function input_tahun(){
		$data['content'] = 'v_input_tahun';
		$this->load->view('view_super_admin',$data);
	}

	public function proses_inputguru(){
		$this->load->library('form_validation');
		$username = $this->input->post('username');
		$password = $this->input->post('pass');

		$nama = $this->input->post('nama');
		$idKelompok = $this->input->post('id_matapelajaran');

		$m_file 		= $_FILES['userfile']['name'];
		$config['upload_path'] = './assets/images/profile_pendamping'; 
		$config['allowed_types']        = 'gif|jpg|png|JPG|jpeg';
		$config['max_size']				= '4024';
		$config['max_width']  			= '4600';
		$config['max_height']  			= '4200';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			/*$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);*/

			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : gif, jpg, png, JPG, jpeg ]</div>");
			redirect('admin/Super_admin/input_guru','refresh');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$data_akses =array(
				'username' => $username,
				'password' => $password,
				'idLevel' => 3,
				);
			$idAkses= $this->mod->getInsert($data_akses);

			$data = array(
				'idAkses' => $idAkses,
				'nama' => $nama,
				'idKelompok' => $idKelompok,
				'foto' => $m_file
				);
			$sukses=$idAkses= $this->mod->insertPendampingUniv($data);
			if($sukses == TRUE){
				redirect('admin/Super_admin/table_guru','refresh');
				return $sukses;
			}else{
				/*return FALSE;
				exit();
				redirect('super_admin/table_guru','refresh');*/

				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : gif, jpg, png, JPG, jpeg ]</div>");
				redirect('admin/Super_admin/input_guru','refresh');

			}
		}

	}
	public function download_guru($id_guru){
		$sql = $this->db->query("SELECT * FROM guru where id_guru='$id_guru'");
		foreach ($sql->result() as $key => $value) {
		//$d['data'] = $this->db->query("SELECT * FROM kotak_masuk where nama_guru='$value->nama_guru'");
			$d['username'] = $value->username;
		//$d['content'] = 'lap_guru';
			$this->load->view('lap_guru',$d);
		}
	}

	public function proses_inputkelas(){

		$kelas = $this->input->post('kelas');

		$data = array('kelompok' => $kelas );
		$sukses=$this->db->insert('kelompok',$data);
		if($sukses){
			redirect('admin/Super_admin/table_kelas','refresh');
		}else{
			echo "gagal bos";
		}
	}
	public function proses_input_data_penugasan(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$idFk 	= $this->input->post('idFk');

		$data_akses = array(
			'username' => $username,
			'password' => $password,
			'idLevel' => 4
			);

		$idAkses = $this->mod->getInsert($data_akses);

		$data = array(
			'idAkses' => $idAkses,
			'idFk' => $idFk
			);

		$sukses=$this->mod->insertPenugasanFk($data);

		if($sukses){
			redirect('admin/Super_admin/data_penugasan','refresh');
		}else{
			echo "gagal bos";
		}
	}
	public function proses_input_data_penugasan_univ(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data_akses = array(
			'username' => $username,
			'password' => $password,
			'idLevel' => 2
			);

		$idAkses = $this->mod->getInsert($data_akses);

		$data = array(
			'idAkses' => $idAkses
			);

		$sukses=$this->mod->insertPenugasanUniv($data);

		if($sukses){
			redirect('admin/Super_admin/data_penugasan_univ','refresh');
		}else{
			echo "gagal bos";
		}
	}
	public function proses_edit_data_penugasan(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$idFk 	= $this->input->post('idFk');
		$id 	= $this->input->post('id');

		$data_akses = array(
			'username' => $username,
			'password' => $password,
			'idLevel' => 4
			);

		$this->mod->getUpdate(array('id' => $id), $data_akses);

		$data = array(
			'idFk' => $idFk
			);

		$sukses=$this->mod->updatePenugasanFk(array('idAkses' => $id), $data);

		if($sukses){
			redirect('admin/Super_admin/data_penugasan','refresh');
		}else{
			echo "gagal bos";
		}
	}
	public function proses_edit_data_penugasan_univ(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$id 	= $this->input->post('id');

		$data_akses = array(
			'username' => $username,
			'password' => $password,
			'idLevel' => 2
			);

		$sukses = $this->mod->getUpdate(array('id' => $id), $data_akses);

		if($sukses){
			redirect('admin/Super_admin/data_penugasan_univ','refresh');
		}else{
			echo "gagal bos";
		}
	}

	public function proses_editkelas(){

		$id_kelompok = $this->input->post('id_kelas');
		$kelompok = $this->input->post('kelas');
		
		$data = array('kelompok' => $kelompok );
		$this->db->where('id_kelompok',$id_kelompok);
		$sukses=$this->db->update('kelompok',$data);
		if($sukses){
			redirect('admin/Super_admin/table_kelas','refresh');
		}else{
			echo "gagal bos";
		}
	}

	public function proses_inputtahun(){

		$tahun = $this->input->post('tahun');

		$data = array('tahun' => $tahun );
		$sukses=$this->db->insert('tahun',$data);
		if($sukses){
			redirect('admin/Super_admin/table_tahun','refresh');
		}else{
			echo "gagal bos";
		}
	}

	public function proses_edittahun(){

		$id_tahun = $this->input->post('id_tahun');
		$tahun = $this->input->post('tahun');
		
		$data = array('tahun' => $tahun );
		$this->db->where('id_tahun',$id_tahun);
		$sukses=$this->db->update('tahun',$data);
		if($sukses){
			redirect('admin/Super_admin/table_tahun','refresh');
		}else{
			echo "gagal bos";
		}
	}

	public function proses_inputjurusan(){

		$jurusan = $this->input->post('jurusan');

		$data = array('jurusan' => $jurusan );
		$sukses=$this->db->insert('jurusan',$data);
		if($sukses){
			redirect('admin/Super_admin/table_jurusan','refresh');
		}else{
			echo "gagal bos";
		}
	}


	public function proses_inputmapel(){

		$mapel = $this->input->post('mapel');

		$data = array('matapelajaran' => $mapel );
		$sukses=$this->db->insert('tbl_matapelajaran',$data);
		if($sukses){
			redirect('admin/Super_admin/table_mapel','refresh');
		}else{
			echo "gagal bos";
		}
	}

	public function proses_editjurusan(){

		$id_jurusan = $this->input->post('id_jurusan');
		$jurusan = $this->input->post('jurusan');
		
		$data = array('jurusan' => $jurusan );
		$this->db->where('id_jurusan',$id_jurusan);
		$sukses=$this->db->update('jurusan',$data);
		if($sukses){
			redirect('admin/Super_admin/table_jurusan','refresh');
		}else{
			echo "gagal bos";
		}
	}

	public function proses_editmapel(){

		$id_matapelajaran = $this->input->post('id_matapelajaran');
		$matapelajaran = $this->input->post('matapelajaran');
		
		$data = array('matapelajaran' => $matapelajaran );
		$this->db->where('id_matapelajaran',$id_matapelajaran);
		$sukses=$this->db->update('tbl_matapelajaran',$data);
		if($sukses){
			redirect('admin/Super_admin/table_mapel','refresh');
		}else{
			echo "gagal bos";
		}
	}


	public function proses_editguru(){
		$id = $this->input->post('id');
		$username = $this->input->post('username');
		$password = $this->input->post('pass');

		$nama = $this->input->post('nama');		
		$idKelompok = $this->input->post('idKelompok');

		$data_akses = array(
			'username' 		=> $username,
			'password' 		=> $password,
			'idLevel'		=> 3);
		$this->mod->getUpdate(array('id' => $id), $data_akses);

		$data = array(
			'nama' 			=> $nama,
			'idKelompok' 	=> $idKelompok
			);
		$sukses = $this->mod->updatePendampingUniv(array('idAkses' => $id), $data);

		if($sukses){
			redirect('admin/Super_admin/table_guru','refresh');
		}else{
			echo "gagal bos";
		}

	}


	public function proses_inputsiswa(){

		$this->load->library('form_validation');
		$nim = $this->input->post('nim');
		$password = $this->input->post('password');

		$nama = $this->input->post('nama');
		$tahun = $this->input->post('tahun');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$jurusan = $this->input->post('jurusan');

		$m_file 		= $_FILES['userfile']['name'];
		$config['upload_path'] = './assets/images/profile_mhs';		
		$config['allowed_types']        = 'gif|jpg|png|JPG|jpeg';
		$config['max_size']				= '4024';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			/*$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);*/

			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : gif, jpg, png, JPG, jpeg ]</div>");
			redirect('admin/Super_admin/input_siswa','refresh');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_akses = array(
				'username' 		=> $nim,
				'password' 		=> $password,
				'idLevel'		=> 6);
			$idAkses = $this->mod->getInsert($data_akses);

			$data = array(
				'nim' 			=> $nim,
				'nama' 			=> $nama,
				'tahun' 		=> $tahun,
				'jenis_kelamin' => $jenis_kelamin,
				'foto' 			=> $m_file,
				'idJurusan'		=> $jurusan,
				'idAkses'		=> $idAkses);
			$sukses = $this->mod->insertMahasiswa($data);

			if($sukses == TRUE){
				redirect('admin/Super_admin/table_siswa','refresh');
				return $sukses;
			}else{
				// return FALSE;
				// exit();
				// redirect('super_admin/input_siswa','refresh');

				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : gif, jpg, png, JPG, jpeg ]</div>");
				redirect('admin/Super_admin/input_siswa','refresh');

			}
		}

	}



	public function proses_editsiswa(){
		$this->load->library('form_validation');

		$id 			= $this->input->post('id');
		$nama 			= $this->input->post('nama');
		$password 		= $this->input->post('password');
		$tahun 			= $this->input->post('tahun');
		$jurusan 		= $this->input->post('jurusan');
		$jenis_kelamin	= $this->input->post('jenis_kelamin');
		
		$m_file 		= $_FILES['userfile']['name'];
		$config['upload_path'] = './assets/images/profile_mhs'; 
		$config['allowed_types']        = 'gif|jpg|png|JPG|jpeg';
		$config['max_size']				= '4024';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			/*$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);*/

			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Edit Mahasiswa [format file : gif, jpg, png, JPG, jpeg ]</div>");
			redirect('admin/Super_admin/table_siswa','refresh');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_akses = array(
				'password' 	=> $password);
			$this->mod->getUpdate(array('id' => $id), $data_akses);

			$data = array(
				'nama' 		=> $nama,
				'tahun' 	=> $tahun,
				'jenis_kelamin' => $jenis_kelamin,
				'idJurusan' 	=> $jurusan,
				'foto' 			=> $m_file);
			$sukses = $this->mod->updateMahasiswa(array('idAkses' => $id), $data);

			if($sukses == TRUE){
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Update Berhasil</div>");
				redirect('admin/Super_admin/table_siswa','refresh');
				return $sukses;
			}else{
				/*return FALSE;
				exit();
				redirect('super_admin/table_siswa','refresh');*/

				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Edit Mahasiswa [format file : gif, jpg, png, JPG, jpeg ]</div>");
				redirect('admin/Super_admin/table_siswa','refresh');

			}
		}

	}


	public function hapus_siswa($username){
		$d['username']	= $username;
		$this->db->delete('akses',array('username'=>$username));
		$this->db->delete('mahasiswa',array('nim'=>$username));
		redirect('admin/Super_admin/table_siswa');
	}

	public function hapus_kelas($id_kelas){
		$d['id_kelas']	= $id_kelas;
		$sql	= $this->db->delete('kelompok',array('id_kelompok'=>$id_kelas));
		redirect('admin/Super_admin/table_kelas');
		
	}
	public function hapus_tugas($id_tugas){
		$d['id_tugas']	= $id_tugas;
		$sql	= $this->db->delete('tbl_tugas',array('id_tugas'=>$id_tugas));
		if($sql){
			?>	
			<script type="text/javascript">
				alert("sukses di hapus data <?php echo $id_tugas ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/daftar_tugas";
			</script>
			<?php	
		}else{
			?>	
			<script type="text/javascript">
				alert("GAGAL di hapus data <?php echo $id_tugas ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/daftar_tugas";
			</script>
			<?php	
		}
		
	}

	public function hapus_tahun($id_tahun){
		$d['id_tahun']	= $id_tahun;
		$sql	= $this->db->delete('tahun',array('id_tahun'=>$id_tahun));
		if($sql){
			?>	
			<script type="text/javascript">
				alert("sukses di hapus data <?php echo $id_tahun ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/table_tahun";
			</script>
			<?php	
		}else{
			?>	
			<script type="text/javascript">
				alert("GAGAL di hapus data <?php echo $nik ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/table_siswa";
			</script>
			<?php	
		}
		
	}

	public function hapus_jurusan($id_jurusan){
		$d['id_jurusan']	= $id_jurusan;
		$sql	= $this->db->delete('jurusan',array('id_jurusan'=>$id_jurusan));
		if($sql){
			?>	
			<script type="text/javascript">
				alert("sukses di hapus data <?php echo $id_jurusan ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/table_jurusan";
			</script>
			<?php	
		}else{
			?>	
			<script type="text/javascript">
				alert("GAGAL di hapus data <?php echo $nik ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/table_siswa";
			</script>
			<?php	
		}
		
	}
	public function hapus_mapel($id_matapelajaran){
		$d['id_matapelajaran']	= $id_matapelajaran;
		$sql	= $this->db->delete('tbl_matapelajaran',array('id_matapelajaran'=>$id_matapelajaran));
		if($sql){
			?>	
			<script type="text/javascript">
				alert("sukses di hapus data <?php echo $id_matapelajaran ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/table_mapel";
			</script>
			<?php	
		}else{
			?>	
			<script type="text/javascript">
				alert("GAGAL di hapus data <?php echo $id_matapelajaran ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/table_mapel";
			</script>
			<?php	
		}
		
	}

	public function hapus_guru($id){
		$d['id']	= $id;
		$sql	= $this->db->delete('akses',array('id'=>$id));
		redirect('admin/Super_admin/table_guru');
	}
	public function hapus_data_penugasan($id){
		$d['id']	= $id;
		$sql	= $this->db->delete('akses',array('id'=>$id));
		redirect('admin/Super_admin/data_penugasan');
	}
	public function hapus_data_penugasan_univ($id){
		$d['id']	= $id;
		$sql	= $this->db->delete('akses',array('id'=>$id));
		redirect('admin/Super_admin/data_penugasan_univ');
	}
	public function table_siswa(){
		$data['content'] = 'v_tabelsiswa';
		$data['data'] = $this->db->query("SELECT mahasiswa.*,akses.*,jurusan.jurusan from mahasiswa,akses,jurusan where mahasiswa.idAkses=akses.id and mahasiswa.idJurusan=jurusan.id_jurusan and akses.idLevel=6 order by akses.id DESC");
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function table_kelas(){
		$data['content'] = 'v_tabelkelas';
		$data['data'] = $this->db->get('kelompok');
		$this->load->view('view_super_admin',$data);
		unset($data);
	}

	public function table_jurusan(){
		$data['content'] = 'v_tabeljurusan';
		$data['data'] = $this->db->get('jurusan');
		$this->load->view('view_super_admin',$data);
	}

	public function table_tahun(){
		$data['content'] = 'v_tabeltahun';
		$data['data'] = $this->db->get('tahun');
		$this->load->view('view_super_admin',$data);
	}

	public function table_guru(){
		$data['content'] = 'v_tabelguru';
		$data['data'] = $this->db->query("SELECT akses.id,akses.username,akses.password,pendamping_univ.nama,pendamping_univ.foto,kelompok.kelompok from akses,pendamping_univ,kelompok where akses.idLevel=3 and pendamping_univ.idAkses=akses.id and pendamping_univ.idKelompok=kelompok.id_kelompok order by akses.id desc");
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function data_penugasan(){
		$data['content'] = 'admin/v_data_penugasan';
		$data['data'] = $this->db->query("SELECT akses.id,akses.username,akses.password,fakultas.namaFk from akses,penugasan_fk,fakultas where akses.idLevel=4 and penugasan_fk.idAkses=akses.id and penugasan_fk.idFk=fakultas.id order by akses.id desc");
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	public function data_penugasan_univ(){
		$data['content'] = 'admin/v_data_penugasan_univ';
		$data['data'] = $this->db->query("SELECT akses.id,akses.username,akses.password,penugasan_univ.idAkses from akses,penugasan_univ where akses.idLevel=2 and penugasan_univ.idAkses=akses.id order by akses.id DESC");
		$this->load->view('view_super_admin',$data);
		unset($data);
	}
	
	public function edit_siswa($id){
		$d['id'] = $id;
		$d['fakultas'] = $this->mod->dataFakultas();
		$d['content'] = 'v_editsiswa';
		$this->load->view('view_super_admin',$d);
		unset($data);
	}

	public function edit_tugas($id_tugas){
		$d['id_tugas'] = $id_tugas;
		$d['data'] = $this->db->get_where('tbl_tugas',$d);
		$d['content'] = 'edit_tugas';
		$this->load->view('view_super_admin',$d);
	}

	public function edit_tahun($id_tahun){
		$d['id_tahun'] = $id_tahun;
		$d['data'] = $this->db->get_where('tahun',$d);
		$d['content'] = 'v_edittahun';
		$this->load->view('view_super_admin',$d);
	}
	public function edit_jurusan($id_jurusan){
		$d['id_jurusan'] = $id_jurusan;
		$d['data'] = $this->db->get_where('jurusan',$d);
		$d['content'] = 'v_editjurusan';
		$this->load->view('view_super_admin',$d);
	}
	public function edit_mapel($id_matapelajaran){
		$d['id_matapelajaran'] = $id_matapelajaran;
		$d['data'] = $this->db->get_where('tbl_matapelajaran',$d);
		$d['content'] = 'v_editmapel';
		$this->load->view('view_super_admin',$d);
	}
	public function edit_kelas($id_kelas){
		$d['id_kelompok'] = $id_kelas;
		$d['data'] = $this->db->get_where('kelompok',$d);
		$d['content'] = 'v_editkelas';
		$this->load->view('view_super_admin',$d);
		unset($id_kelas,$data);
	}

	public function edit_guru($id){
		$d['id'] = $id;
		$d['content'] = 'v_editguru';
		$this->load->view('view_super_admin',$d);
		unset($id,$data);
	}
	public function edit_data_penugasan($id){
		$d['id'] = $id;
		$d['content'] = 'admin/v_edit_data_penugasan';
		$this->load->view('view_super_admin',$d);
		unset($id,$data);
	}
	public function edit_data_penugasan_univ($id){
		$d['id'] = $id;
		$d['content'] = 'admin/v_edit_data_penugasan_univ';
		$this->load->view('view_super_admin',$d);
		unset($data,$id);
	}
	
	public function pilih_siswa($nisn){
		$data['nisn'] = $nisn;
		$data['data'] = $this->db->get_where('pendaftaran',$data);
		$data['content'] = 'v_pilihsiswa';
		$this->load->view('view_super_admin',$data);

	}

	public function proses_edittugas(){
		$this->load->library('form_validation');
		
		$id_tugas 	= $this->input->post('id_tugas');
		$judul 		= $this->input->post('judul');
		$berlaku			= $this->input->post('berlaku');
		$m_file 			= $_FILES['userfile']['name'];
		$config['upload_path'] = './assets/images/penugasan';
		$config['allowed_types'] = 'pdf';  
		$config['overwrite'] = FALSE;  
		$config['max_size'] = '150000';  

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			/*$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);*/

			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Edit Tugas [format file : PDF ]</div>");
			redirect('admin/Super_admin/daftar_tugas','refresh');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());        
			$data = array(
				'judul' 		=> $judul,
				'berlaku'       => $berlaku,
				'm_file'   		=> $m_file
				);
			$this->db->where('id_tugas',$id_tugas);
			$sukses = $this->db->update('tbl_tugas', $data);
			if($sukses == TRUE){
				redirect('admin/Super_admin/daftar_tugas','refresh');
				return $sukses;
			}else{
				/*return FALSE;
				exit();
				redirect('super_admin/daftar_tugas','refresh');*/

				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Edit Tugas [format file : PDF ]</div>");
				redirect('admin/Super_admin/daftar_tugas','refresh');

			}
		}
	}

	public function proses_addtugas(){
		$this->load->library('form_validation');

		$judul 	= $this->input->post('judul');
		$tgl			= $this->input->post('tgl');
		$berlaku		= $this->input->post('berlaku');
		$m_file 		= $_FILES['userfile']['name'];
		$config['upload_path'] = './assets/images/penugasan';  
            //$config['upload_path'] = './assets/download/news/';  
		$config['allowed_types'] = 'pdf';  
		$config['overwrite'] = FALSE;  
		$config['max_size'] = '150000';  

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			/*$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);*/

			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
			redirect('admin/Super_admin/input_tugas','refresh');

		}
		else
		{
			$data = array('upload_data' => $this->upload->data());        
			$data = array(
				'judul'		=> $judul,
				'tgl' 		=> $tgl,
				'berlaku' 	=> $berlaku,
				'm_file' 	=> $m_file
				);
			$sukses = $this->db->insert('tbl_tugas', $data);
			if($sukses == TRUE){
				redirect('admin/Super_admin/daftar_tugas','refresh');
				return $sukses;
			}else{
				/*echo "gagal";
				return FALSE;
				exit();*/
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
				redirect('admin/Super_admin/input_tugas','refresh');
			}
		}

	}

	public function proses_addgaleri(){
		$this->load->library('form_validation');

		$nama 	= $this->input->post('nama');
		$m_file = $_FILES['userfile']['name'];

		$config['upload_path'] = './application/views/photo';  
		$config['allowed_types']        = 'gif|jpg|png|JPEG';
		$config['max_size']             = 1100;
		$config['max_width']            = 2024;
		$config['max_height']           = 1768; 

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());        
			$data = array(
				'nama' => $nama,
				'gambar'   => $m_file
				);
			$sukses = $this->db->insert('galeri', $data);
			if($sukses == TRUE){
				redirect('admin/Super_admin/table_galeri','refresh');
				return $sukses;
			}else{
				return FALSE;
				exit();
				redirect('admin/Super_admin/table_galeri','refresh');

			}
		}

	}
	public function table_galeri(){
		$data['data'] = $this->db->get('galeri');
		$data['content'] = 'v_galeri';
		$this->load->view('view_super_admin',$data);
	}


	public function table_mapel(){
		$data['data'] = $this->db->get('tbl_matapelajaran');
		$data['content'] = 'v_tabelmapel';
		$this->load->view('view_super_admin',$data);
	}
	public function hapus_galeri($id_galeri){

		$data['id_galeri']	= $id_galeri;
		$sql			= $this->db->delete('galeri',array('id_galeri'=>$id_galeri));
		if($sql){
			?>	
			<script type="text/javascript">
				alert("sukses di hapus data <?php echo $id_galeri ?>");window.location="<?php echo base_url(); ?>admin/Super_admin/table_galeri";
			</script>
			<?php	
		}else{
			?>	
			<script type="text/javascript">
				alert("GAGAL di hapus data <?php echo $id_kontak ?>");window.location="<?php echo base_url(); ?>kontak";
			</script>
			<?php	
		}
		
	}

	public function hapus_kontak($id_kontak){

		$data['id_kontak']	= $id_kontak;
		$sql			= $this->db->delete('kontak',array('id_kontak'=>$id_kontak));
		if($sql){
			?>	
			<script type="text/javascript">
				alert("sukses di hapus data <?php echo $id_kontak ?>");window.location="<?php echo base_url(); ?>index.php/ci_admin/kontak";
			</script>
			<?php	
		}else{
			?>	
			<script type="text/javascript">
				alert("GAGAL di hapus data <?php echo $id_kontak ?>");window.location="<?php echo base_url(); ?>index.php/kontak";
			</script>
			<?php	
		}
		
	}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */