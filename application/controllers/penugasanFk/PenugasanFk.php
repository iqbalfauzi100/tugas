<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class PenugasanFk extends CI_Controller {

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));

	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('penugasanFk/m_daftar_tugas_penugasan','mod');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		$this->load->library('upload');
		$this->load->helper(array('url'));
			$this->load->model('penugasanFk/m_daftar_tugas_penugasan','mod');
	}

	public function index()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'penugasanFk/v_homepenugasan';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function home()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'penugasanFk/v_homepenugasan';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function profile()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'penugasanFk/v_profile_penugasan';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function input_penugasan()
	{	
		$data['user']=$this->session->userdata('username');

		$id_sess = $this->session->userdata('id');
		$sql = "SELECT akses.*,penugasan_fk.* FROM penugasan_fk,akses where akses.id= penugasan_fk.idAkses and akses.id='$id_sess'";
		$getData = $this->db->query($sql)->row();
		$data['idFk']= $getData->idFk;

		$data['content'] 	 = 	'penugasanFk/v_input_penugasan';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function daftar_tugas()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'penugasanFk/daftar_penugasan';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function tugas_selesai()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'penugasanFk/tugas_selesai';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function tugas_belum_selesai()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'tugas_belum_selesai';
		$this->load->view('view_penugasan',$data);
	}
	public function total_nilai()
	{	
		$data['user']=$this->session->userdata('username');

		$data['content'] 	 = 	'penugasanFk/total_nilai';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function edit_penugasan($id_tugas){
		$data['id_tugas']   = $id_tugas;
		$data['data']       = $this->db->get_where('tbl_tugas',array('id_tugas' => $id_tugas));
		$data['user']		= $this->session->userdata('username');
		$data['content'] 	= 'penugasanFk/edit_penugasan';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function addPenugasan()
	{
		$this->load->library('form_validation');
		$judul 			= $this->input->post('judul');
		$tgl			= $this->input->post('mulai');
		$berlaku		= $this->input->post('berlaku');
		$nilai			= $this->input->post('nilai');
		$keterangan		= $this->input->post('idFk');

		$m_file 		= $_FILES['userfile']['name'];
		$config['upload_path'] = './assets/images/penugasan';  
            //$config['upload_path'] = './assets/download/news/';  
		$config['allowed_types'] = 'pdf';  
		$config['overwrite'] = FALSE;  
		$config['max_size'] = '150000';  

		// $this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload())
		{
			$error = $this->upload->display_errors();
			// $this->load->view('upload', $error);
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ] ".$error."</div>");
			redirect('penugasanFk/PenugasanFk/input_penugasan','refresh');

		}
		else
		{
			$data = array('upload_data' => $this->upload->data());        
			$data = array(
				'judul'		=> $judul,
				'tgl' 		=> $tgl,
				'berlaku' 	=> $berlaku,
				'nilai' 	=> $nilai,
				'm_file' 	=> $m_file,
				'keterangan' => $keterangan
				);
			$sukses = $this->db->insert('tbl_tugas', $data);
			if($sukses == TRUE){
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di simpan</div>");
				redirect('penugasanFk/PenugasanFk/daftar_tugas','refresh');
				return $sukses;
			}else{
				/*echo "gagal";
				return FALSE;
				exit();*/
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
				redirect('penugasanFk/PenugasanFk/input_penugasan','refresh');
			}
		}

	}
	public function tambahPenugasan()
	{
		$this->load->library('form_validation');
		$judul 			= $this->input->post('judul');
		$tgl			= $this->input->post('mulai');
		$berlaku		= $this->input->post('berlaku');
		$nilai			= $this->input->post('nilai');
		$keterangan		= $this->input->post('idFk');
		$m_file 		= $_FILES['userfile']['name'];

		if($m_file =='')
		{
			$data = array(
				'judul'		=> $judul,
				'tgl' 		=> $tgl,
				'berlaku' 	=> $berlaku,
				'nilai' 	=> $nilai,
				'keterangan' => $keterangan
				);
			$sukses = $this->db->insert('tbl_tugas', $data);
			if($sukses == TRUE){
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di simpan</div>");
				redirect('penugasanFk/PenugasanFk/daftar_tugas');
			}else{
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
				redirect('penugasanFk/PenugasanFk/input_penugasan');
			}

		}else{ 
			$config['upload_path'] = './assets/images/penugasan';
			$config['allowed_types'] = 'pdf';

			$this->upload->initialize($config);

			if ($this->upload->do_upload('userfile'))
			{
				$data = array('upload_data' => $this->upload->data());
				$gbr = $this->upload->data();
				$gambar = $gbr['file_name'];

				$data = array(
					'judul'		=> $judul,
					'tgl' 		=> $tgl,
					'berlaku' 	=> $berlaku,
					'nilai' 	=> $nilai,
					'm_file' 	=> $m_file,
					'keterangan' => $keterangan
					);
				$sukses = $this->db->insert('tbl_tugas', $data);
				if($sukses == TRUE){
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di simpan</div>");
					redirect('penugasanFk/PenugasanFk/daftar_tugas');
				}else{
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
					redirect('penugasanFk/PenugasanFk/input_penugasan');
				}
			}
			else
			{
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
				redirect('penugasanFk/PenugasanFk/input_penugasan');
			}
		}
	}

	public function proses_updatetugas()
{
	$this->load->library('form_validation');
	$id_tugas 	= $this->input->post('id_tugas');
	$judul 			= $this->input->post('judul');
	$tgl			= $this->input->post('mulai');
	$berlaku		= $this->input->post('berlaku');
	$nilai			= $this->input->post('nilai');
	$m_file 		= $_FILES['userfile']['name'];

	if($m_file =='')
	{
		$data = array(
			'judul'		=> $judul,
			'tgl' 		=> $tgl,
			'berlaku' 	=> $berlaku,
			'nilai' 	=> $nilai
			);

		$this->db->where('id_tugas',$id_tugas);
		$sukses = $this->db->update('tbl_tugas', $data);

		if($sukses == TRUE){
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Update</div>");
			redirect('penugasanFk/PenugasanFk/daftar_tugas','refresh');
		}else{
				/*return FALSE;
				exit();*/
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
				redirect('penugasanFk/PenugasanFk/daftar_tugas','refresh');
			}

		}else{ 
			$config['upload_path'] = './assets/images/penugasan';
			$config['allowed_types'] = 'pdf';

			$this->upload->initialize($config);

			if ($this->upload->do_upload('userfile'))
			{
				$data = array('upload_data' => $this->upload->data());
				$gbr = $this->upload->data();
				$gambar = $gbr['file_name'];

				$data = array(
					'judul'		=> $judul,
					'tgl' 		=> $tgl,
					'berlaku' 	=> $berlaku,
					'nilai' 	=> $nilai,
					'm_file' 	=> $m_file,
					);

				$row = $this->db->where('id_tugas',$id_tugas)->get('tbl_tugas')->row();
				unlink('assets/images/penugasan/'.$row->m_file);

				$this->db->where('id_tugas',$id_tugas);
				$sukses = $this->db->update('tbl_tugas', $data);

				if($sukses == TRUE){
					$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di update</div>");
					redirect('penugasanFk/PenugasanFk/daftar_tugas','refresh');
					// exit();
				}else{
				/*return FALSE;
				exit();*/
				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
				redirect('penugasanFk/PenugasanFk/daftar_tugas','refresh');
			}
		}
		else
		{
			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Upload File [format file : PDF ]</div>");
			redirect('penugasanFk/PenugasanFk/daftar_tugas','refresh');
		}
	}
}

	public function proses_edittugas(){
		$this->load->library('form_validation');
		
		$id_tugas 	= $this->input->post('id_tugas');
		$judul 		= $this->input->post('judul');
		$nilai 		= $this->input->post('nilai');
		$tgl		= $this->input->post('mulai');
		$berlaku	= $this->input->post('berlaku');
		$m_file 			= $_FILES['userfile']['name'];
		$config['upload_path'] = './assets/images/penugasan';
		$config['allowed_types'] = 'pdf';  
		$config['overwrite'] = FALSE;  
		$config['max_size'] = '150000';  

		// $this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload())
		{
			/*$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);*/

			$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Edit Tugas [format file : PDF ]</div>");
			redirect('penugasanFk/PenugasanFk/daftar_tugas');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());        
			$data = array(
				'judul' 		=> $judul,
				'tgl'       	=> $tgl,
				'berlaku'       => $berlaku,
				'nilai'       	=> $nilai,
				'm_file'   		=> $m_file
				);
			$this->db->where('id_tugas',$id_tugas);
			$sukses = $this->db->update('tbl_tugas', $data);
			if($sukses == TRUE){
				redirect('penugasanFk/PenugasanFk/daftar_tugas');
				return $sukses;
			}else{
				/*return FALSE;
				exit();
				redirect('super_admin/daftar_tugas','refresh');*/

				$this->session->set_flashdata("pesan", "<div class=\"alert alert-danger alert-dismissible\">Gagal Edit Tugas [format file : PDF ]</div>");
				redirect('penugasanFk/PenugasanFk/daftar_tugas');

			}
		}
	}

	public function laporan(){
		$data['user']=$this->session->userdata('username');
		$this->load->view('laporan_guru',$data);
	}

	public function edit_tugas($id_tugas){
		$data['id_tugas']    =  $id_tugas;
		$data['data']        =  $this->db->get_where('tbl_tugas',$data);
		$data['username']	 = $this->session->userdata('username');
		$data['content'] 	 = 	'edit_tugas';
		$this->load->view('view_penugasan',$data);
	}

	public function hapus_tugas($id)
	{
		//delete file
		$data = $this->mod->get_by_id($id);
		if(file_exists('assets/images/penugasan/'.$data->m_file) && $data->m_file)
			unlink('assets/images/penugasan/'.$data->m_file);
		
		$this->mod->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	public function hapus_kotakmasuk($id_kotakmasuk){

		$data['id_kotakmasuk']	= $id_kotakmasuk;
		$sql			= $this->db->delete('kotak_masuk',array('id_kotakmasuk'=>$id_kotakmasuk));
		if($sql){
			?>	
			<script type="text/javascript">
				alert("sukses di hapus data <?php echo $id_kotakmasuk ?>");window.location="<?php echo base_url(); ?>index.php/from_walikelas/kotak_masuk";
			</script>
			<?php	
		}else{
			?>	
			<script type="text/javascript">
				alert("GAGAL di hapus data <?php echo $id_kontak ?>");window.location="<?php echo base_url(); ?>index.php/from_walikelas/kotak_masuk";
			</script>
			<?php	
		}

	}

	public function update_profile()
	{	
		$id = $this->input->post('id');
		$data = array(
			'username' 	=> $this->input->post('username'),
			'password' 	=> $this->input->post('password')
			);

		$this->db->where('id',$id);
		$this->db->update('akses',$data);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di update</div>");
		redirect('penugasanFk/PenugasanFk/profile');
	}
	public function datatable_daftarTugas()
	{
		$list = $this->mod->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->judul;
			// $row[] = $model->kelompok;
			$row[] = $model->tgl;
			$row[] = $model->berlaku;
			$row[] = $model->nilai;

			$ket = $model->keterangan;
			if($ket==0){
				$row[] = '
				<span class="label label-success"><i class="fa fa-clock-o"></i> Universitas</span>
				';
			}else{
				$row[] = '
				<span class="label label-primary"><i class="fa fa-clock-o"></i> Fakultas</span>
				';
			}

			$row[] = '
			<a class="btn btn-sm btn-primary" href="assets/images/penugasan/'.$model->m_file.'" title="Lihat File" target="_blank">
				<i class="glyphicon glyphicon-zoom-in"></i> Lihat File
			</a>
			<a class="btn btn-sm btn-warning" href="penugasanFk/PenugasanFk/edit_penugasan/'.$model->id_tugas.'" title="Edit">
				<i class="glyphicon glyphicon-pencil"></i>
			</a>

			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$model->id_tugas."'".')">
				<i class="glyphicon glyphicon-trash"></i>
			</a>
			';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mod->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	
}


