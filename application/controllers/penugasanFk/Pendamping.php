<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Pendamping extends CI_Controller {

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));

	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('penugasanFk/m_pendamping','mod');
	// 		// $this->load->model('penugasanFk/m_daftar_tugas_penugasan','mod');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		$this->load->library('upload');
		$this->load->helper(array('url'));
		$this->load->model('penugasanFk/m_pendamping','mod');
			// $this->load->model('penugasanFk/m_daftar_tugas_penugasan','mod');
	}

	public function login(){
		$this->load->view('login_utama');
	}

	public function index()
	{	
		$data['user'] = $this->session->userdata('username');
		$data['content'] = 'penugasanFk/daftar_pendamping';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function input_pendamping()
	{	
		$data['user'] = $this->session->userdata('username');
		$data['content'] = 'penugasanFk/input_pendamping';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}

	public function datatable()
	{
		$list = $this->mod->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->username;
			$row[] = $model->password;
			$row[] = $model->nama;
			$row[] = $model->namaKelompok;

			$row[] = '
			<a class="btn btn-sm btn-warning" href="penugasanFk/Pendamping/edit_pendamping/'.$model->id.'" title="Edit">
				<i class="glyphicon glyphicon-pencil"></i>
			</a>

			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$model->idAkses."'".')">
				<i class="glyphicon glyphicon-trash"></i>
			</a>
			';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mod->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function hapus_pendamping($id)
	{	
		$this->mod->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	public function edit_pendamping($id_tugas){
		$data['id_tugas']   = $id_tugas;

		/*$this->db->select('pendamping_fk.*','akses.username','akses.password');
		$this->db->from('pendamping_fk');
		$this->db->join('akses', 'pendamping_fk.idAkses = akses.id', 'left');
		$this->db->where('pendamping_fk.id',$id_tugas);
		$data['data']       = $this->db->get();*/

		$sql ="select pendamping_fk.*,akses.username,akses.password from pendamping_fk,akses where pendamping_fk.idAkses = akses.id and pendamping_fk.id=$id_tugas";
		
		$data['data']       = $this->db->query($sql);

		$data['user']		= $this->session->userdata('username');

		$data['content'] 	= 'penugasanFk/edit_pendamping';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function proses_editpendamping(){
		$this->load->library('form_validation');
		
		$id 		= $this->input->post('id');
		$idAkses 	= $this->input->post('idAkses');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$nama 		= $this->input->post('nama');
		$kelompok 	= $this->input->post('kelompok');

		$data_akses = array(
			'username' => $username,
			'password' => $password
			);

		$this->mod->getUpdate(array('id' => $idAkses), $data_akses);

		$data = array(
			'nama' => $nama,
			'idKelompokFk' => $kelompok
			);

		$this->mod->pendamping_fk(array('id' => $id), $data);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Update</div>");
		redirect('penugasanFk/Pendamping');
	}
	public function proses_inputpendamping(){

		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$nama 		= $this->input->post('nama');
		$kelompok 	= $this->input->post('kelompok');

		$data_akses = array(
			'username' => $username,
			'password' => $password,
			'idLevel' => 5
			);

		$idAkses = $this->mod->getInsert($data_akses);

		$data = array(
			'idAkses' => $idAkses,
			'nama' => $nama,
			'idKelompokFk' => $kelompok
			);

		$this->mod->insertPendampingFk($data);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Simpan</div>");
		redirect('penugasanFk/Pendamping');
	}
	
}


