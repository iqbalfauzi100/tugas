<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Kelompok extends CI_Controller {

	// public function __construct()
	// {
	// 	parent::__construct();
	// 	$this->load->library('upload');
	// 	$this->load->helper(array('url'));

	// 	if($this->session->userdata('username') and $this->session->userdata('password') and $this->session->userdata('idLevel')){
	// 		$this->load->model('penugasanFk/m_kelompok','mod');
	// 		// $this->load->model('penugasanFk/m_daftar_tugas_penugasan','mod');

	// 	}else{
	// 		redirect(base_url('index.php/C_login'));
	// 	}
	// }
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Loginauth');
		$this->loginauth->view_page();
		$this->load->library('upload');
		$this->load->helper(array('url'));
			$this->load->model('penugasanFk/m_kelompok','mod');
			// $this->load->model('penugasanFk/m_daftar_tugas_penugasan','mod');
	}

	public function index()
	{	$data['user'] = $this->session->userdata('username');
		$data['content'] = 'penugasanFk/daftar_kelompokfk';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function input_kelompok()
	{	$data['user'] = $this->session->userdata('username');
		$data['content'] = 'penugasanFk/input_kelompok';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}

	public function datatable()
	{
		$list = $this->mod->make_datatables();
		$data = array();
		$no = 0;
		foreach ($list as $model) {
			$no+=1;
			$row = array();
			$row[] = $no;
			$row[] = $model->namaKelompok;
			$row[] = $model->namaFk;

			$row[] = '
			<a class="btn btn-sm btn-warning" href="penugasanFk/Kelompok/edit_kelompok/'.$model->id.'" title="Edit">
				<i class="glyphicon glyphicon-pencil"></i>
			</a>

			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$model->id."'".')">
				<i class="glyphicon glyphicon-trash"></i>
			</a>
			';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $no,
			"recordsFiltered" => $this->mod->get_filtered_data(),
			"data" => $data,
			);
		echo json_encode($output);
	}
	public function hapus_kelompok($id)
	{	
		$this->mod->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	public function edit_kelompok($id_tugas){
		$data['id_tugas']   = $id_tugas;
		$data['data']       = $this->db->get_where('kelompok_fk',array('id' => $id_tugas));
		$data['user']		= $this->session->userdata('username');

		$data['content'] 	= 'penugasanFk/edit_kelompok';
		$this->load->view('penugasanFk/view_penugasan',$data);
	}
	public function proses_editkelompok(){
		$this->load->library('form_validation');
		
		$id 			= $this->input->post('id');
		$namaKelompok 	= $this->input->post('namaKelompok');

		$data = array(
			'namaKelompok' 		=> $namaKelompok
			);
		$this->db->where('id',$id);
		$sukses = $this->db->update('kelompok_fk', $data);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Update</div>");
		redirect('penugasanFk/Kelompok');
	}
	public function proses_inputkelompok(){

		$namaKelompok 	= $this->input->post('namaKelompok');
		$idFk 			= $this->input->post('idFk');

		$data = array(
			'namaKelompok' => $namaKelompok,
			'idFk' => $idFk
			);

		$this->db->insert('kelompok_fk',$data);

		$this->session->set_flashdata("pesan", "<div class=\"alert alert-success alert-dismissible\">Data berhasil di Simpan</div>");
		redirect('penugasanFk/Kelompok');
	}
	
}


