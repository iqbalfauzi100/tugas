<div class="col-lg-12">
    <h3 class="page-header">TABEL DATA MAHASISWA </h3>
</div>
<div class="col-sm-12">
      <?php echo $this->session->flashdata('pesan');?></div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a  class="btn btn-primary  btn-sm" href="admin/Super_admin/input_siswa">TAMBAH</a>

            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                          <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Password</th>
                            <th>Tahun</th>
                            <th>Jurusan</th>
                            <th>Foto</th>
                            <th>Action</th>
                        </tr>   
                    </thead>
                    <tbody>
                        <?php foreach ($data->result() as $row) : ?>
                            
                          <tr>
                            <td><?php echo $row->username ?></td>
                            <td><?php echo $row->nama ?></td>
                            <td><?php echo $row->password?></td>
                            <td><?php echo $row->tahun ?></td>
                            <td><?php echo $row->jurusan ?></td>
                            <td>
                               <?php   
                               $image = array(
                                  'src' => 'assets/images/profile_mhs/'.($row->foto),
                                  'class' => 'photo',
                                  'width' => '90',
                                  'height' => '30',
                                  'rel' => 'lightbox',
                                  ); echo img($image); ?>
                              </td>
                              <td>
                                <a href="admin/Super_admin/edit_siswa/<?php echo $row->id ?>" title="">
                                    <li class="fa fa-pencil-square-o">
                                        EDIT
                                    </li>
                                </a> &nbsp; &nbsp; 
                                <a href="admin/Super_admin/hapus_siswa/<?php echo $row->username ?>" title="">
                                   <li class="fa fa-trash-o" onclick="return confirm('Anda Yakin ingin menghapus data ini ?')">
                                    HAPUS
                                </li>
                            </a> 
                        </td>
                    </tr>
                <?php endforeach; ?>
                

            </tbody>
        </table>
    </div>
                            <!-- /.table-responsive -->
                            <!-- onclick="hapus (1)" -->