<?php echo form_open_multipart('admin/Super_admin/proses_editsiswa') ?>
<div class="col-md-12 clearfix">
  <ul id="example-tabs" class="nav nav-tabs" data-toggle="tabs">
    <li class=""><a href="#biodata"></a></li>

  </ul>
  <?php 
  $query = "SELECT mahasiswa.*,akses.* from mahasiswa,akses where mahasiswa.idAkses=akses.id and akses.id='$id'";
  $res = $this->db->query($query);
  foreach ($res->result() as $edit) { ?>


  <div class="tab-content">
    <div class="tab-pane active" id="biodata">
      <table class="table table-bordered">
        <tr class="success">
          <th colspan="2">EDIT DATA MAHASISWA BARU</th>
        </tr>
        <tr>
          <td width="150">NIS, Nama</td>
          <td>
            <div class='col-sm-2'>
              <input type='text' name='id'  disabled="disabled" class='form-control' required='required' value='<?php echo $edit->username ?>'>

              <input type='hidden' name='id' value='<?php echo $edit->id ?>'>
            </div>             
            <div class='col-sm-8'><input type='text' name='nama' placeholder='Nama ..' class='form-control' required='required' value='<?php echo $edit->nama ?>'>
            </div>
            <tr>
              <td>Password</td>
              <td>
                <div class='col-sm-4'>
                  <input type='text' name='password' placeholder='password ..' class='form-control' required='required' value='<?php echo $edit->password ?>' >
                </div>
              </td>
            </tr>
            <tr>
              <td>Tahun Angkatan</td>
              <td>
                <div class='col-sm-2'>
                  <select name='tahun'  class='form-control'>
                    <?php
                    $tahun=$edit->tahun;
                    $this->db->from('tahun');
                    $this->db->order_by("tahun", "desc");
                    $sql = $this->db->get()->result(); 
                    foreach ($sql as $th) {
                      if ($tahun==$th->tahun) {
                        $selek = 'selected';
                      }else{
                        $selek = '';
                      }
                      echo '<option value="'.$th->tahun.'" '.$selek.'>'.$th->tahun.'</option>';
                    }
                    ?>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td>Fakultas</td>
              <td>
                <div class='col-sm-8'>
                  <select name='fakultas' id='fakultas' class='form-control' required='required'>
                    <option value="" disabled selected>Pilih Fakultas</option>
                    <?php
                    foreach ($fakultas as $row) { ?>
                    <option value='<?php echo $row->id ?>'><?php echo $row->namaFk ?></option>
                    <?php } ?>
                  </select>
                </div>
              </td>
            </tr>
            <tr>
              <td>Jurusan</td>
              <td>
                <div class='col-sm-8'>
                  <select name='jurusan' id='jurusan' class='form-control' required='required'>
                    <option value="" disabled selected>Pilih Jurusan</option>
                  </select>
                </div>
              </td>
            </tr>
          </div>
        </div>
        <tr>
          <td>Gender</td>
          <td>
            <div class="col-md-2">
              <select name="jenis_kelamin" class='form-control'>
                <option value="" disabled selected>~ Pilih Jenis Kelamin ~</option>
                <option <?=($edit->jenis_kelamin=='Laki - Laki')?'selected="selected"':''?>>Laki - Laki</option>
                <option <?=($edit->jenis_kelamin=='Perempuan')?'selected="selected"':''?>>Perempuan</option>
              </div>
            </td>
          </tr>

   


        </div>
      </div>
      <div class="col-sm-6">
       <tr>
         <td width="150">Foto</td>
         <td>
           <div class='col-sm-8'><input type='file' name='userfile' placeholder=' ..' class='form-control' required='required' value=''>
           </div>
         </td>
       </tr>                   
     </table>
   </div>
   <input type="submit" name="submit" value="UPDATE" class="btn btn-danger  btn-sm">
   <input type="reset"  value="RESET" class="btn btn-danger  btn-sm">
   <a href="admin/Super_admin/table_siswa" class="btn btn-danger btn-sm">KEMBALI</a></form>
 </form>

 <?php } ?>

 <script type="text/javascript">
  $(document).ready(function(){
   $('#fakultas').change(function(){
    var fakultas =  $('#fakultas').val();
    $.ajax({
      url: '<?php echo site_url('admin/Super_admin/getJurusan'); ?>',
      type: 'GET',
      data: "fakultas="+fakultas,
      dataType: 'json',
      success: function(data){
       var fakultas=`<select id="jurusan" name="jurusan">
       <option value="null">Pilih Jurusan</option>`;
       for (var i = 0; i < data.length; i++) {
        fakultas+='<option value="'+data[i].id_jurusan+'">'+data[i].jurusan+'</option>';
      }
      fakultas+=`</select>
      <label>Jurusan</label>`;
      $('#jurusan').html(fakultas);
    }
  });
  });
 });
</script>