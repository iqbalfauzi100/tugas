<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>Data Tugas</h3>
      <div class="col-sm-12">
      <?php echo $this->session->flashdata('pesan');?></div>
      <table id="dataTables-example" class="table table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
          <tr class="success">
           <th>No</th>
           <th>Judul</th>
           <th>Tanggal Kirim</th>
           <th>Berlaku</th>
           <th>Action</th>
         </tr>
       </thead>
       <tbody>
         <?php $no=1;
         $this->db->from('tbl_tugas');
         $this->db->order_by('id_tugas','desc');
         $sql = $this->db->get()->result();
         foreach ($sql as $row) : ?>
         <tr>
         <td><?php echo $no++; ?></td>
         <td><?php echo $row->judul ?></td>
         <td><?php echo $row->tgl ?></td>
         <td><?php echo $row->berlaku ?></td>
         <td>
         <a class="btn btn-sm btn-primary" href="assets/images/penugasan/<?php echo $row->m_file ?>" title="Lihat File" target="_blank"><li class="glyphicon glyphicon-zoom-in"></li>
          </a> &nbsp; &nbsp;
          <a class="btn btn-sm btn-warning" href="index.php/super_admin/edit_tugas/<?php echo $row->id_tugas ?>" title="Edit"><li class="fa fa-pencil-square-o"></li>
          </a> &nbsp; &nbsp; 
          <a class="btn btn-sm btn-danger" href="index.php/super_admin/hapus_tugas/<?php echo $row->id_tugas ?>" title="Hapus">
           <li class="glyphicon glyphicon-trash" onclick="hapus (1)"></li>
        </a> 
      </td>
      </tr>

    <?php endforeach; ?>
  </tbody>
  <tfoot>
    <tr class="success">
      <th>No</th>
      <th>Judul</th>
      <th>Tanggal Kirim</th>
      <th>Berlaku</th>
      <th>Action</th>
    </tr>
  </tfoot>
</table>
</div>
</div>
</section>