<?php echo form_open_multipart('admin/Super_admin/proses_inputsiswa') ?>
<div class="col-md-12 clearfix">
  <ul id="example-tabs" class="nav nav-tabs" data-toggle="tabs">
    <li class=""><a href="#biodata"></a></li>

  </ul>
  <div class="col-sm-12">
    <?php echo $this->session->flashdata('pesan');?></div>
    <div class="tab-content">
      <div class="tab-pane active" id="biodata">
        <table class="table table-bordered">
          <tr class="success">
            <th colspan="2">INPUT DATA MAHASISWA BARU</th>
          </tr>
          <tr>
            <td width="150">NIM, Nama</td>
            <td>
              <div class='col-sm-2'>
                <input type='text' name='nim' placeholder='NIM ..' class='form-control' required='required' value=''>
              </div>             
              <div class='col-sm-8'>
                <input type='text' name='nama' placeholder='Nama ..' class='form-control' required='required' value=''>
              </div>
              <tr>
                <td>Password </td>
                <td>
                  <div class='col-sm-4'>
                    <input type='text' name='password' placeholder='password ..' class='form-control' required='required' value='' >
                  </div>
                </td>
              </tr>                        
              <tr>
                <td>Tahun Angkatan</td>
                <td>
                  <div class='col-sm-8'>
                    <select name='tahun' class='form-control' required='required'>
                      <?php 
                      $this->db->from('tahun');
                      $this->db->order_by("tahun", "desc");
                      $sql = $this->db->get(); 
                      foreach ($sql->result() as $key => $value) { ?>
                      <option value='<?php echo $value->tahun ?>'><?php echo $value->tahun ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Fakultas</td>
                <td>
                  <div class='col-sm-8'>
                    <select name='fakultas' id='fakultas' class='form-control' required='required'>
                      <option value="" disabled selected>Pilih Fakultas</option>
                      <?php
                      foreach ($fakultas as $row) { ?>
                      <option value='<?php echo $row->id ?>'><?php echo $row->namaFk ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Jurusan</td>
                <td>
                  <div class='col-sm-8'>
                    <select name='jurusan' id='jurusan' class='form-control'>
                      <option value="" disabled selected>Pilih Jurusan</option>
                    </select>
                  </div>
                </td>
              </tr>
            </div>
          </div>
          <tr>
           <td>Jenis Kelamin</td>
           <td>
            <div class="col-md-8">
              <select name='jenis_kelamin' class='form-control' required='required'>
                <option value='Laki - Laki'>Laki - Laki</option>
                <option value='Perempuan'>Perempuan</option>
              </div>
            </td>
          </tr>
        </div>
      </div>
      <div class="col-sm-6">
       <tr>
         <td width="150"> Foto</td>
         <td>
           <div class='col-sm-8'><input type='file' name='userfile' placeholder=' ..' class='form-control' required='required' value=''>
           </div>
         </td>
       </tr>                   
     </table>
   </div>
   <input type="submit" name="submit" value="SIMPAN" class="btn btn-danger  btn-sm">
   <input type="reset"  value="RESET" class="btn btn-danger  btn-sm">
   <a href="admin/Super_admin/table_siswa" class="btn btn-danger btn-sm">KEMBALI</a></form>
 </form>
 <script type="text/javascript">
  $(document).ready(function(){
   $('#fakultas').change(function(){
    var fakultas =  $('#fakultas').val();
    $.ajax({
      url: '<?php echo site_url('admin/Super_admin/getJurusan'); ?>',
      type: 'GET',
      data: "fakultas="+fakultas,
      dataType: 'json',
      success: function(data){
       var fakultas=`<select id="jurusan" name="jurusan">
       <option value="null">Pilih Jurusan</option>`;
       for (var i = 0; i < data.length; i++) {
        fakultas+='<option value="'+data[i].id_jurusan+'">'+data[i].jurusan+'</option>';
      }
      fakultas+=`</select>
      <label>Jurusan</label>`;
      $('#jurusan').html(fakultas);
    }
  });
  });
 });
</script>