<?php echo form_open_multipart('admin/Super_admin/proses_input_data_penugasan') ?>
<div class="col-md-12 clearfix">
  <ul id="example-tabs" class="nav nav-tabs" data-toggle="tabs">
    <li class=""><a href="#biodata"></a></li>
  </ul>
  <div class="col-sm-12">
    <?php echo $this->session->flashdata('pesan');?></div>
    <div class="tab-content">
      <div class="tab-pane active" id="biodata">
        <table class="table table-bordered">
          <tr class="success"><th colspan="2">INPUT DATA PENUGASAN</th></tr>
          <tr><td width="150"> Username</td>
            <td>
             <div class='col-sm-8'><input type='text' name='username' placeholder='Username ..' class='form-control' required='required' value='' ></div></td>
           </tr>    
           <tr><td width="150"> Password</td>
             <td>
             <div class='col-sm-8'><input type='text' name='password' placeholder='Password ..' class='form-control' required='required' value='' ></div></td>
             </tr> 
             <tr><td>Fakultas</td><td>
              <div class='col-sm-8'>
               <select name='idFk'  class='form-control' >
                <?php 
                $sql = $this->db->get('fakultas'); 
                foreach ($sql->result() as $row) {
                  ?>
                  <option value='<?php echo $row->id ?>'><?php echo $row->namaFk ?></option>
                  <?php } ?>
                </select>
              </div>
            </td></tr>                 
          </table>
        </div>
        <input type="submit" name="submit" value="SIMPAN" class="btn btn-danger  btn-sm">
        <input type="reset"  value="RESET" class="btn btn-danger  btn-sm">
        <a href="admin/Super_admin/data_penugasan" class="btn btn-danger btn-sm">KEMBALI</a>
      </form>
    </form>