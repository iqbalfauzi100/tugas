<?php
$id_sess = $this->session->userdata('id');
$sql = "SELECT akses.*,penugasan_fk.* FROM penugasan_fk,akses where akses.id= penugasan_fk.idAkses and akses.id='$id_sess'";
$getData = $this->db->query($sql)->row();
$idFk = $getData->idFk;

//Nama Jurusan
$this->db->select('namaFk');
$this->db->from('fakultas');
$this->db->where('id',$idFk);
$query = $this->db->get()->row();
$fakultas =$query->namaFk;

?>
<script type="text/javascript">
  $(function () {
    $('#datetimepicker1').datetimepicker({
      format : 'YYYY-MM-DD HH:mm:ss'
    });
  });
</script>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Input Kelompok Fakultas <?php echo $fakultas;?></h3>
          </div>
          <div class="col-sm-12">
            <?php echo $this->session->flashdata('pesan');?>
          </div>
          <form class="form-horizontal" method="post" action="<?php echo base_url('penugasanFk/Kelompok/proses_inputkelompok');?>">
            <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama Kelompok</label>
                  <div class="col-sm-10">
                    <input type='text' name='namaKelompok' class='form-control' required='required'>
                  </div>
                  <input type='hidden' name='idFk' class='form-control' value="<?php echo $idFk?>">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="reset" class="btn btn-warning" id="reset">Reset</button>
                <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>