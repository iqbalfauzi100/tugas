<?php
$id_sess = $this->session->userdata('id');
$sql = "SELECT akses.*,penugasan_fk.* FROM penugasan_fk,akses where akses.id= penugasan_fk.idAkses and akses.id='$id_sess'";
$getData = $this->db->query($sql)->row();
$idFk = $getData->idFk;

//Nama Jurusan
$this->db->select('namaFk');
$this->db->from('fakultas');
$this->db->where('id',$idFk);
$query = $this->db->get()->row();
$fakultas =$query->namaFk;

?>
<script type="text/javascript">
  $(function () {
    $('#datetimepicker1').datetimepicker({
      format : 'YYYY-MM-DD HH:mm:ss'
    });
  });
</script>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Pendamping Fakultas <?php echo $fakultas;?></h3>
          </div>
          <div class="col-sm-12">
            <?php echo $this->session->flashdata('pesan');?>
          </div>
          <form class="form-horizontal" method="post" action="<?php echo base_url('penugasanFk/Pendamping/proses_editpendamping');?>">
            <div class="box-body">
              <?php foreach ($data->result() as $row) {
                ?>
                <input type='hidden' name='id'  class='form-control' value='<?php echo $row->id; ?>'>
                <input type='hidden' name='idAkses'  class='form-control' value='<?php echo $row->idAkses; ?>'>
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                    <input type='text' name='username' class='form-control' required='required' value="<?php echo $row->username?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input type='text' name='password' class='form-control' required='required' value="<?php echo $row->password?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type='text' name='nama' class='form-control' required='required' value="<?php echo $row->nama?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Kelompok</label>
                  <div class="col-sm-10">
                  <select class="form-control" name="kelompok" required="required">
                      <option value="" disabled selected>~ Pilih Kelompok ~</option>
                      <?php
                      $this->db->from('kelompok_fk');
                      $this->db->where('kelompok_fk.idFk',$idFk);
                      $this->db->order_by('kelompok_fk.id','DESC');
                      $kelompok = $this->db->get()->result();

                      foreach ($kelompok as $kelompok) {
                        if ($row->idKelompokFk == $kelompok->id) {
                          $selek = 'selected';
                        }else{
                          $selek = '';
                        }
                        echo '<option value="'.$kelompok->id.'" '.$selek.'>'.$kelompok->namaKelompok.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="reset" class="btn btn-warning" id="reset">Reset</button>
                <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
              </div>
              <!-- /.box-footer -->
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>