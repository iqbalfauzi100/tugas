<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>Data Tugas Mahasiswa</h3>
      <br />
      <div class="col-sm-12">
        <?php echo $this->session->flashdata('pesan');?></div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kelompok</label>
                  <select class="form-control" style="width: 100%;" id="id_kelompok" name="id_kelompok" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih Kelompok</option>
                    <?php
                    // $kelompok = $this->db->get('kelompok')->result();

                    $id_sess = $this->session->userdata('id');
                    $sql = "SELECT akses.*,penugasan_fk.* FROM penugasan_fk,akses where akses.id= penugasan_fk.idAkses and akses.id='$id_sess'";
                    $getData = $this->db->query($sql)->row();
                    $idFk = $getData->idFk;

                    $this->db->select('*');
                    $this->db->from('kelompok_fk');
                    $this->db->where('kelompok_fk.idFk',$idFk);
                    $this->db->order_by('kelompok_fk.id','DESC');
                    $kelompok = $this->db->get()->result();

                    foreach ($kelompok as $row) { ?>
                    <option value="<?php echo $row->id;?>"><?php echo $row->namaKelompok;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Judul Tugas</label>
                  <select class="form-control" style="width: 100%;" id="id_tugas" name="id_tugas" onChange="viewTabel()">
                    <option value="" disabled selected>Pilih Judul</option>
                    <?php
                    $id_sess = $this->session->userdata('id');
                    $sql = "SELECT akses.*,penugasan_fk.* FROM penugasan_fk,akses where akses.id= penugasan_fk.idAkses and akses.id='$id_sess'";
                    $getData = $this->db->query($sql)->row();
                    $idFk = $getData->idFk;


                    $this->db->select('*');
                    $this->db->from('tbl_tugas');
                    $this->db->where('tbl_tugas.keterangan',$idFk);
                    $this->db->order_by('tbl_tugas.id_tugas','DESC');
                    $tugas = $this->db->get()->result();
                    foreach ($tugas as $row) { ?>
                    <option value="<?php echo $row->id_tugas;?>"><?php echo $row->judul;?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body" style="overflow-x: auto;">
          <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
              <tr class="success">
               <th>No</th>
               <th>NIM</th>
               <th>Nama</th>
               <th>Judul</th>
               <th>Rangkuman</th>
               <th>Aksi</th>
               <th>Nilai</th>
             </tr>
           </thead>
           <tbody>
           </tbody>
           <tfoot>
            <tr class="success">
             <th>No</th>
             <th>NIM</th>
             <th>Nama</th>
             <th>Judul</th>
             <th>Rangkuman</th>
             <th>Aksi</th>
             <th>Nilai</th>
           </tr>
         </tfoot>
       </table>
     </div>
   </div>
 </div>
</section>
<script type="text/javascript">
  var save_method;
  var table;
  var id_kelompok;
  var id_tugas;
  document.addEventListener("DOMContentLoaded", function (event) {
    // datatable();
  });

  function viewTabel() {
   id_kelompok = $("#id_kelompok").val();
   id_tugas = $("#id_tugas").val();

   datatable();
 }
 // function datatable() {
 //   table = $('#table').DataTable({ 
 //    "processing": true,
 //    "serverSide": true,
 //    "ajax": {
 //      "url": "<?php echo base_url('index.php/penugasan/tugas_selesai/datatable'); ?>",
 //      "type": "POST",
 //      data:{'id_kelompok':id_kelompok,'id_tugas':id_tugas}
 //    },
 //    "columnDefs": [
 //    { 
 //      "targets": [ -1 ],
 //      "orderable": false,
 //    },
 //    ],

 //  });
 // }

 function datatable() {
   table = $('#table').DataTable({
    "destroy": true,
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax":{
     url: "<?php echo base_url('penugasanFk/tugas_selesai/datatable'); ?>",
     type: "POST",
     data:{'id_kelompok':id_kelompok,'id_tugas':id_tugas}
   },
   "columnDefs": [
   {
    "targets": [2,-1],
    "orderable":false,
  },
  ],
  // "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>'
});
 }
  // $(document).ready(function() {

  // });

  function add_person()
  {
    save_method = 'add';
    $('#form')[0].reset();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data Agama');
  }

  function edit_person(id)
  {

    $.ajax({
      url : "<?php echo site_url('penugasan/tugas_selesai/ajax_edit/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        $('[name="id"]').val(data.id_kotakmasuk);
        $('[name="nilai"]').val(data.nilai);

        $('#modal_form').modal('show');
        $('.modal-title').text('UBAH NILAI');
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function reload_table()
  {
    table.ajax.reload(null,false);
  }

  function save()
  {
    $('#btnSave').text('saving...');
    $('#btnSave').attr('disabled',true);
    var url;
    url = "<?php echo site_url('penugasan/tugas_selesai/ajax_update')?>";
    $.ajax({
      url : url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {
       $('#modal_form').modal('hide');
       reload_table();
       $('#btnSave').text('save');
       $('#btnSave').attr('disabled',false);
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
      alert('Error adding / update data');
      $('#btnSave').text('save');
      $('#btnSave').attr('disabled',false);
    }
  });
  }

  function delete_person(id)
  {
    if(confirm('Apakah anda yakin ingin menghapus data ini?'))
    {
        // ajax delete data to database
        $.ajax({
          url : "<?php echo site_url('penugasan/penugasan/hapus_tugas')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
                //if success reload ajax table
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error deleting data');
              }
            });
      }
    }

    function view_detail_upload(nim,id_tugas) {
      var url = "<?php echo site_url('penugasanFk/tugas_selesai/view_detail_upload')?>";
      $.ajax({
        url : url+"/"+nim+"/"+id_tugas,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          detail = '';
          no=0;
          for (var i = 0; i < data.length; i++) {
            no+=1;
            detail += `
            <tr>
              <td>`+no+`</td>
              <td>`+data[i].title+`</td>
              <td><a href="assets/images/kirim_tugas/`+data[i].title+`" target="_blank"><img src="assets/images/kirim_tugas/kaca.png" width="50" height="50"/></a></td>
            </tr>
            `;
          }
          $('#nim').html(data[0].nim);
          $('#detail_upload').html(detail);
          $('#modal1').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data!');
        }
      });
    }

  </script>

  <!-- ====================================== Untuk Input Mask ======================================= -->
  <!-- Select2 -->
  <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
  <!-- InputMask -->
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
  <!-- ================================================================================================ -->

  <!-- Page script -->
  <script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>

    <!-- Bootstrap modal -->
    <!-- <div class="modal modal-primary"> -->
    <div class="modal modal-primary" id="modal_form" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Person Form</h3>
          </div>
          <div class="modal-body form">
            <form id="form" class="form-horizontal">
              <input name="id" class="form-control" type="hidden">
              <div class="form-body">
                <div class="form-group">
                  <label class="control-label col-md-3">Nilai</label>
                  <div class="col-md-9">
                    <input name="nilai" placeholder="Input Nilai" class="form-control" type="number" maxlength="3" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
                    <span class="help-block"></span>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-success">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End Bootstrap modal -->


    <!-- Bootstrap modal -->
    <!-- <div class="modal modal-primary"> -->
    <div class="modal fade" id="modal1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Detail file upload NIM : <span id="nim"></span></h3>
          </div>
          <div class="modal-body form">
            <table class="table table-bordered table-hover" cellspacing="0" width="100%">
             <thead>
              <tr class="success">
                <td>No</td>
                <td>Nama Berkas</td>
                <td>File</td>
              </tr>
            </thead>
            <tbody id="detail_upload">

            </tbody>
            <tfoot>
              <tr class="success">
               <td>No</td>
               <td>Nama Berkas</td>
               <td>File</td>
             </tr>
           </tfoot>
         </table>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->