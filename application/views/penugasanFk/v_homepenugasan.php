  <?php
  $id = $this->session->userdata('id');
  $query = "SELECT penugasan_fk.idFk FROM penugasan_fk,akses where penugasan_fk.idAkses=akses.id and akses.id='$id'";
  $sql   = $this->db->query($query)->row();
  $idFk = $sql->idFk;
  ?>
  <section class="content-header">
    <h1>
      Selamat datang, <?php echo $this->session->userdata('username'); ;?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <?php 
            $this->db->select('mahasiswa.nim');
            $this->db->from('mahasiswa');
            $this->db->join('jurusan', 'mahasiswa.idJurusan = jurusan.id_jurusan', 'left');
            $this->db->where('jurusan.idFk',$idFk);
            $count_mhs = $this->db->get()->num_rows();
            ?>
            <h3><?php echo $count_mhs;?></h3>

            <p>Mahasiswa</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <?php
            $this->db->select('tbl_tugas.id_tugas');
            $this->db->from('tbl_tugas');
            $this->db->where('tbl_tugas.keterangan',$idFk);
            $count_tugas = $this->db->get()->num_rows();
            ?>
            <h3><?php echo $count_tugas;?></h3>
            <p>Daftar Tugas</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="penugasanFk/PenugasanFk/daftar_tugas" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
           <?php
           $this->db->from('kotak_masuk');
           $this->db->where(array('status_tugas'=>$idFk));
           $count_data_tgs = $this->db->get()->num_rows();
           ?>
           <h3><?php echo $count_data_tgs;?></h3>

           <p>Data Tugas Mahasiswa</p>
         </div>
         <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="penugasanFk/PenugasanFk/tugas_selesai" class="small-box-footer" target="_blank">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
  <!-- <div class="col-lg-3 col-xs-6">
    small box
    <div class="small-box bg-red">
      <div class="inner">
        <h3>65</h3>
  
        <p>Unique Visitors</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div> -->
  <!-- ./col -->
</div>