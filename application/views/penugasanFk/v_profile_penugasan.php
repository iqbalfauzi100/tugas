<?php
$id =  $this->session->userdata('id');

$this->db->from('akses');
$this->db->join('penugasan_fk', 'akses.id = penugasan_fk.idAkses', 'left');
$this->db->where('akses.id',$id);
$query = $this->db->get()->result();
foreach ($query as $row) {

}
//Nama Fakultas
$this->db->select('namaFk');
$this->db->from('fakultas');
$this->db->where('id',$row->idFk);
$query = $this->db->get()->row();
$namaFk =$query->namaFk;
?>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Profile Pengguna Laman Penugasan Fakultas</h3>
          </div>
          <div class="col-sm-12">
            <?php echo $this->session->flashdata('pesan');?></div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('penugasanFk/PenugasanFk/update_profile');?>">
              <div class="box-body">
              <input type="hidden" name="id" value="<?php echo $this->session->userdata('id');?>">
               <div class="form-group">                
                  <label class="col-sm-2 control-label"><b>Fakultas</b></label>
                  <div class="col-sm-10">
                    <input type='text' name='fakultas' placeholder="Fakultas" class='form-control' required='required' disabled="disabled" value='<?php echo $namaFk;?>'>
                  </div>
                </div>
                <div class="form-group">                
                  <label class="col-sm-2 control-label"><font color="red"> <b>Username *</b></font></label>
                  <div class="col-sm-10">
                    <input type='text' name='username' placeholder="Username" class='form-control' required='required' value='<?php echo $row->username;?>' >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><font color="red"> <b>Password *</b></font></label>
                  <div class="col-sm-10">
                    <input type='text' name='password' placeholder="Password" class='form-control' required='required' value='<?php echo $row->password;?>' >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="reset" class="btn btn-warning" id="reset">Reset</button>
                <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>