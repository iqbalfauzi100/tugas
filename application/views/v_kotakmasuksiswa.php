
<div class="box">
  <div class="box-header">
    <h3 class="box-title">Daftar Tugas</h3>
  </div><!-- /.box-header -->
  <div class="box-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Tanggal Kirim</th>
          <th>Deadline</th>
          <th>File</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <?php 
          $no=1;
          $sql = "SELECT * FROM siswa where nis='$nis' ";
          $data=$this->db->query($sql);         
          foreach ($data->result() as $row) {
          }
          
          $id_kelompok=$row->id_kelompok;
          $query = "SELECT * from tbl_tugas order by id_tugas desc";
          $rs = $this->db->query($query);
          foreach ($rs->result() as $key) {
            ?>
            <td> <?php echo $no++ ?></td>
            <td> <?php echo $key->judul ?></td>
            <td><?php echo $key->tgl?></td>
            <td><?php echo $key->berlaku ?></td>

            <td>
              <a class="btn btn-sm btn-primary" href="<?php echo base_url() ?>assets/images/penugasan/<?php echo $key->m_file ?>" target="_blank">
                <i class="glyphicon glyphicon-zoom-in"></i> Lihat File
              </a>
            </td>
          </tr>
        </tbody>
        <?php } ?>
        <tfoot>
          <tr class="success">
            <th>No</th>
            <th>Judul</th>
            <th>Tanggal Kirim</th>
            <th>Deadline</th>
            <th>File</th>
          </tr>
        </tfoot>
      </table>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->
</div>