  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    $( function() {
      $("#datepicker").datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true, 
      });
    } );
  </script>
  <?php echo form_open_multipart('super_admin/proses_edittugas') ?>
  <div class="col-md-12 clearfix">
    <ul id="example-tabs" class="nav nav-tabs" data-toggle="tabs">
      <li class=""><a href="#biodata"></a></li>

    </ul>

      <?php foreach ($data->result() as $key => $value) {
        ?>
        <div class="tab-content">
          <div class="tab-pane active" id="biodata">
            <table class="table table-bordered">
              <tr class="success"><th colspan="2">EDIT TUGAS</th></tr>
              <tr><td width="150"> Judul</td>
                <td>

                 <div class='col-sm-8'>
                   <input type='text' name='judul' value="<?php echo $value->judul ?>"  class='form-control' required='required'  >
                   <input type='hidden' name='id_tugas'  class='form-control' value='<?php echo $value->id_tugas; ?>'>
                   <input type="hidden" name='kelas' value="<?php echo $this->session->userdata('id_kelompok');?>">
                 </div></td>
               </tr>
             <!-- <tr><td width="150"> Nama Pendamping</td>
              <td>
             
               <div class='col-sm-8'><input type='text' disabled="disabled" name='nama_guru' placeholder='Username ..' class='form-control' required='required' value='<?php echo $username; ?>' ></div></td>
             </tr> -->
             <tr><td width="150"> Berlaku</td>
              <td>

               <div class='col-sm-8'><input type='text' name='berlaku' class='form-control' required='required' id="datepicker" value="<?php echo ($value->berlaku == '0000-00-00') ? '' : $value->berlaku;?>"></div>

             </td>
           </tr>
         </tr> 
         <tr><td width="150"> File</td>
          <td>
           <div class='col-sm-8'><input type='file' name='userfile' placeholder='Pelajaran ..' class='form-control' required='required' value='' >
            <b style="color:red">* Format file PDF</b>
          </div></td>
        </tr>                   
      </table>
    </div>
    <input type="submit" name="submit" value="SIMPAN" class="btn btn-danger  btn-sm">
    <input type="reset"  value="RESET" class="btn btn-danger  btn-sm">
  </form>

</form>

<?php
} ?>
