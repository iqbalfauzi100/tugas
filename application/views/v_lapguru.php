<div class="col-lg-12">
  <h1 class="page-header">LAPORAN TUGAS PENDAMPING</h1>
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">

      <!-- /.panel-heading -->
      <div class="panel-body">
        <div class="dataTable_wrapper">
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Pendamping</th>
                <th>Username</th>
                <th>Password</th>
                <th>Kelompok</th>
                <th>Foto</th>
                <th>Action</th>
              </tr>   
            </thead>
            <tbody>
              <?php
              $no=1;
              $sql = $this->db->query("SELECT guru.*,kelompok.kelompok FROM guru 
                LEFT JOIN kelompok ON kelompok.id_kelompok = guru.id_kelompok");
                foreach ($sql->result() as $row) : ?>

                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $row->nama ?></td>
                  <td><?php echo $row->username ?></td>
                  <td><?php echo $row->pass ?></td>
                  <td><?php echo $row->kelompok ?></td>
                  <td>
                   <?php   
                   $image = array(
                    'src' => 'assets/images/profile_pendamping/'.($row->foto),
                    'class' => 'photo',
                    'width' => '90',
                    'height' => '30',
                    'rel' => 'lightbox',
                    ); echo img($image); ?>
                  </td>
                  <td>
                    <a href="index.php/super_admin/download_guru/<?php echo $row->id_guru ?>" title="">
                      <li class="fa fa-pencil-square-o">
                        DOWNLOAD
                      </li>
                    </a> 
                  </td>
                </tr>
              <?php endforeach; ?>


            </tbody>
          </table>
        </div>
                            <!-- /.table-responsive -->