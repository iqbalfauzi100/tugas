<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Profile Mahasiswa</h3>
          </div>
          <div class="col-sm-12">
            <?php echo $this->session->flashdata('pesan');?></div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('Siswa/update_profile');?>" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="nama" class="col-sm-2 control-label">NIM</label>
                  <div class="col-sm-10">
                    <input type='text' name='nim' disabled class='form-control' required='required' value='<?php echo $nim ?>' >
                  </div>
                  <input type="hidden" name="nim" value="<?php echo $nim ?>">
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Angkatan</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="angkatan" required="required">
                      <option value="" disabled selected>~ Pilih Angkatan ~</option>
                      <?php
                      $this->db->from('tahun');
                      $this->db->order_by('tahun.id_tahun','DESC');
                      $angkatan= $this->db->get()->result();

                      foreach ($angkatan as $ang) {
                        if ($tahun == $ang->tahun) {
                          $selek = 'selected';
                        }else{
                          $selek = '';
                        }
                        echo '<option value="'.$ang->tahun.'" '.$selek.'>'.$ang->tahun.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type='text' name='nama' placeholder="Nama" class='form-control' required='required' value='<?php echo $nama ?>' >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Jurusan</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="jurusan1" required="required" disabled>
                      <option value="" disabled selected>~ Pilih Jurusan ~</option>
                      <?php
                      $get_jur= $this->db->get('jurusan')->result();
                      foreach ($get_jur as $jur) {
                        if ($jur_siswa == $jur->id_jurusan) {
                          $selek = 'selected';
                        }else{
                          $selek = '';
                        }
                        echo '<option value="'.$jur->id_jurusan.'" '.$selek.'>'.$jur->jurusan.'</option>';
                      }
                      ?>
                    </select>
                  </div>
                  <input type='hidden' name='jurusan' class='form-control' value='<?php echo $jur_siswa ?>' >
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Jenis Kelamin</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="jenis_kelamin" required="required">
                      <option value="" disabled selected>~ Pilih Jenis Kelamin ~</option>
                      <option <?=($jenis_kelamin=='Laki - Laki')?'selected="selected"':''?>>Laki - Laki</option>
                      <option <?=($jenis_kelamin=='Perempuan')?'selected="selected"':''?>>Perempuan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Kelompok (UNIVERSITAS)</label>
                  <div class="col-sm-10">
                    <?php 
                    $this->db->from('kelompok');
                    $this->db->where('kelompok.id_kelompok',$id_kelompok);
                    $query = $this->db->get()->row();
                    $nama_kelompok =$query->kelompok;
                    ?>
                    <input type='text' name='kelompok'  class='form-control' disabled value='<?php echo $nama_kelompok ?>' >
                  </div>
                  <input type="hidden" name="kelompok" value="<?php echo $id_kelompok ?>">
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Pendamping (UNIVERSITAS)</label>
                  <div class="col-sm-10">
                    <?php 
                    $this->db->from('pendamping_univ');
                    $this->db->where('pendamping_univ.id',$id_pendamping);
                    $query = $this->db->get()->row();
                    $nama_pendamping =$query->nama;
                    ?>
                    <input type='text' name='pendamping'  class='form-control' disabled value='<?php echo $nama_pendamping ?>' >
                  </div>
                  <input type="hidden" name="pendamping" value="<?php echo $id_pendamping ?>">
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">Kelompok (FAKULTAS)</label>
                  <div class="col-sm-10">
                    <?php 
                    $this->db->from('kelompok_fk');
                    $this->db->where('kelompok_fk.id',$idKelompokFk);
                    $query = $this->db->get()->row();
                    $nama_kelompok_fk =$query->namaKelompok;
                    ?>
                    <input type='text' name='kelompok_fk'  class='form-control' disabled value='<?php echo $nama_kelompok_fk ?>' >
                  </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">Pendamping (FAKULTAS)</label>
                  <div class="col-sm-10">
                    <?php 
                    $this->db->from('pendamping_fk');
                    $this->db->where('pendamping_fk.id',$idPendampungFk);
                    $query = $this->db->get()->row();
                    $nama_pendamping_fk =$query->nama;
                    ?>
                    <input type='text' name='pendamping_fk'  class='form-control' disabled value='<?php echo $nama_pendamping_fk ?>' >
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><font color="red"> <b>Password *</b></font></label>
                  <div class="col-sm-10">
                    <input type='text' name='password' placeholder="Password" class='form-control' value='<?php echo $password ?>' required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Foto</label>
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-sm-2" align="center">
                        <a href="assets/images/profile_mhs/<?php echo $foto ?>" target="_blank">
                          <img src="assets/images/profile_mhs/<?php echo $foto ?>" alt="Foto Mahasiswa" height="100" width="100"></a>
                        </div>
                        <div class="col-sm-10">
                          <input type='file' name='filefoto' placeholder=' ..' class='form-control'>
                          <span><font color="red">*Max size 200 kb</font></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button type="reset" class="btn btn-warning" id="reset">Reset</button>
                  <button type="submit" name="submit" class="btn btn-success pull-left">Simpan</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>