<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>VERIFIKASI TUGAS</h3>
      <br />
      <div class="box-body" style="overflow-x: auto;">
        <table id="table" class="table table-bordered table-hover" cellspacing="0" width="100%">
          <thead>
            <tr class="success">
             <th>No.</th>
             <th>NIM</th>
             <th>Nama Siswa</th>
             <th>Kelompok</th>
             <th>Judul Tugas</th>
             <th>Rangkuman Materi</th>
             <th>Aksi</th>
             <th>Verifikasi</th>
           </tr>
         </thead>
         <tbody>
         </tbody>
         <tfoot>
          <tr class="success">
            <th>No.</th>
            <th>NIM</th>
            <th>Nama Siswa</th>
            <th>Kelompok</th>
            <th>Judul Tugas</th>
            <th>Rangkuman Materi</th>
            <th>Aksi</th>
            <th>Verifikasi</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header with-border">
      <!-- <h1>Selamat Datang di Halaman Kesiswaan</h1> -->

      <h3>DATA TUGAS MAHASISWA TERVERIFIKASI</h3>
      <br />
      <div class="box-body" style="overflow-x: auto;">
        <table id="table2" class="table table-bordered table-hover" cellspacing="0" width="100%">
          <thead>
            <tr class="success">
             <th>No.</th>
             <th>NIM</th>
             <th>Nama Siswa</th>
             <th>Kelompok</th>
             <th>Judul Tugas</th>
             <th>Rangkuman Materi</th>
           </tr>
         </thead>
         <tbody>
         </tbody>
         <tfoot>
          <tr class="success">
            <th>No.</th>
            <th>NIM</th>
            <th>Nama Siswa</th>
            <th>Kelompok</th>
            <th>Judul Tugas</th>
            <th>Rangkuman Materi</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  var save_method;
  var table;
  var table2;
  $(document).ready(function() {
    table = $('#table').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('pendampingFk/PendampingFk/datatable_kotak_masuk')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ -1 ],
        "orderable": false,
      },
      ],

    });

    table2 = $('#table2').DataTable({ 
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?php echo site_url('pendampingFk/PendampingFk/datatable_data_kotak_masuk')?>",
        "type": "POST"
      },
      "columnDefs": [
      { 
        "targets": [ -1 ],
        "orderable": false,
      },
      ],
    });
  });

  function add_person()
  {
    save_method = 'add';
    $('#form')[0].reset();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Data Agama');
  }

  function edit_person(idAgama)
  {
    save_method = 'update';
    $('#form')[0].reset();
    $.ajax({
      url : "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_edit/')?>/" + idAgama,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        $('[name="idAgama"]').val(data.idAgama);
        $('[name="namaAgama"]').val(data.namaAgama);

        $('#modal_form').modal('show');
        $('.modal-title').text('Edit Data Agama');
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

  function reload_table()
  {
    table.ajax.reload(null,false);
  }
  function reload_table2()
  {
    table2.ajax.reload(null,false);
  }

  function save()
  {
    var url;
    if(save_method == 'add') 
    {
      url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_add')?>";
    }
    else
    {
      url = "<?php echo site_url('kesiswaan/Con_Master_Agama/ajax_update')?>";
    }
    $.ajax({
      url : url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {
       $('#modal_form').modal('hide');
       reload_table();
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
      alert('Error adding / update data');
    }
  });
  }

  function delete_person(id,nim,id_tugas)
  {
    if(confirm('Apakah anda yakin ingin menghapus data ini?'))
    {
        // ajax delete data to database
        $.ajax({
          url : "<?php echo site_url('pendampingFk/PendampingFk/hapus_kelompok')?>/"+id+"/"+nim+"/"+id_tugas,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
                //if success reload ajax table
                reload_table();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                alert('Error deleting data');
              }
            });
      }
    }

    function seleksi(userId, status)
    {
      var url = "<?php echo site_url('pendampingFk/VerifikasiTugas/changeKeterangan')?>";
      $.ajax({
        url : url+"/"+userId+"/"+status,
        type: "POST",
        data: $('#formInput').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          reload_table();
          reload_table2();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error adding/update data');
        }
      });
    }

      function view_detail_upload(nim,id_tugas) {
    var url = "<?php echo site_url('pendampingFk/PendampingFk/view_detail_upload')?>";
    $.ajax({
      url : url+"/"+nim+"/"+id_tugas,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        detail = '';
        no=0;
        for (var i = 0; i < data.length; i++) {
          no+=1;
          detail += `
          <tr>
            <td>`+no+`</td>
            <td>`+data[i].title+`</td>
            <td><a href="assets/images/kirim_tugas/`+data[i].title+`" target="_blank"><img src="assets/images/kirim_tugas/kaca.png" width="50" height="50"/></a></td>
          </tr>
          `;
        }
        $('#nim').html(data[0].nim);
        $('#detail_upload').html(detail);
        $('#modal1').modal('show');
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data!');
      }
    });
  }

  </script>

  <!-- ====================================== Untuk Input Mask ======================================= -->
  <!-- Select2 -->
  <script src="<?=base_url('assets/plugins/select2/select2.full.min.js')?>"></script>
  <!-- InputMask -->
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.js')?>"></script>
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')?>"></script>
  <script src="<?=base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js')?>"></script>
  <!-- ================================================================================================ -->

  <!-- Page script -->
  <script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
        //Money Euro
        $("[data-mask]").inputmask();
      });
    </script>


        <!-- Bootstrap modal -->
    <!-- <div class="modal modal-primary"> -->
    <div class="modal fade" id="modal1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Detail file upload NIM : <span id="nim"></span></h3>
          </div>
          <div class="modal-body form">
            <table class="table table-bordered table-hover" cellspacing="0" width="100%">
             <thead>
              <tr class="success">
                <td>No</td>
                <td>Nama Berkas</td>
                <td>File</td>
              </tr>
            </thead>
            <tbody id="detail_upload">

            </tbody>
            <tfoot>
              <tr class="success">
               <td>No</td>
               <td>Nama Berkas</td>
               <td>File</td>
             </tr>
           </tfoot>
         </table>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->