<div class="col-lg-12">
                    <h1 class="page-header">TABEL DATA TAHUN</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a  class="btn btn-primary  btn-sm" href="index.php/super_admin/input_tahun">TAMBAH</a>

                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                      <tr>
                                    <th>No</th>
                                    <th>Tahun</th>
                                    <th>Action</th>
                                      </tr>   
                                    </thead>
                                    <tbody>
                                    <?php $no=1; foreach ($data->result() as $row) : ?>
                                    
                                                 <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $row->tahun ?></td>
                                <td>
                                <a href="index.php/super_admin/edit_tahun/<?php echo $row->id_tahun ?>" title="">
                                    <li class="fa fa-pencil-square-o">
                                    EDIT
                                    </li>
                                </a> &nbsp; &nbsp; 
                                <a href="index.php/super_admin/hapus_tahun/<?php echo $row->id_tahun ?>" title="">
                                     <li class="fa fa-trash-o" onclick="hapus (1)">
                                    HAPUS
                                    </li>
                                </a> 
                                </td>
                                     </tr>
                            <?php endforeach; ?>
                                                

                                                                                </tbody>
                                    </table>
                            </div>
                            <!-- /.table-responsive -->