<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginauth{

  // public $favicon = 'assets/template/img/favicon.png';

  protected $ci;

  public function __construct()
  {
    $this->ci =& get_instance();
  }

  public function view_page()
  {
    if($this->ci->session->userdata('idLevel')=='1'){
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="admin") {
        redirect('Super_admin');
      }
    }elseif ($this->ci->session->userdata('idLevel')=='2') {
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="penugasan") {
        redirect('penugasan/penugasan');
      }
    }elseif ($this->ci->session->userdata('idLevel')=='3') {
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="From_walikelas") {
        redirect('From_walikelas');
      }
    }elseif ($this->ci->session->userdata('idLevel')=='4') {
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="penugasanFk") {
        redirect('penugasanFk/PenugasanFk');
      }
    }elseif ($this->ci->session->userdata('idLevel')=='5') {
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="pendampingFk") {
        redirect('pendampingFk/PendampingFk');
      }
    }elseif ($this->ci->session->userdata('idLevel')=='6') {
      $url =  explode("/",$_SERVER["REQUEST_URI"]);
      if ($url[2]!="Siswa") {
        redirect('Siswa');
      }
    }else{
      $url =  $_SERVER["REQUEST_URI"];
      if ($url!='/e.tugas/C_login') {
        redirect('C_login');
      }
    }
  }

}
?>
