-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 29 Okt 2018 pada 16.20
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_etugas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `username` text,
  `password` text,
  `idLevel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `username`, `password`, `idLevel`) VALUES
(1, 'admin', 'admin', 1),
(60, '1465001', '1465001', 6),
(61, '1465002', '1465002', 6),
(62, '1465003', '1465003', 6),
(63, '1465004', '1465004', 6),
(64, '1465005', '1465005', 6),
(66, 'universitas', 'universitas', 2),
(67, 'munir', 'munir', 3),
(68, 'fahmi', 'fahmi', 3),
(69, 'imdad', 'imdad', 3),
(71, 'saintek', 'saintek', 4),
(72, 'humaniora', 'humaniora', 4),
(73, 'pst1', 'pst1', 5),
(74, 'pst2', 'pst2', 5),
(75, 'hum1', 'hum1', 5),
(76, 'hum2', 'hum2', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE `fakultas` (
  `id` int(11) NOT NULL,
  `namaFk` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`id`, `namaFk`) VALUES
(1, 'Tarbiyah dan Ilmu Keguruan'),
(2, 'Syari’ah'),
(3, 'Humaniora'),
(4, 'Psikologi'),
(5, 'Ekonomi'),
(6, 'Sains dan Teknologi'),
(7, 'Kedokteran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hp`
--

CREATE TABLE `hp` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hp`
--

INSERT INTO `hp` (`id`, `nama`, `no`) VALUES
(1, 'Fahmi', '085814294695'),
(2, 'Iqbal', '085231633962'),
(3, 'Muslim', '085735307194');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `jurusan` text NOT NULL,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `jurusan`, `idFk`) VALUES
(1, 'Matematika', 6),
(2, 'Biologi', 6),
(3, 'Bahasa Sastra Inggris', 3),
(4, 'Hukum Bisnis Syariah', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok`
--

CREATE TABLE `kelompok` (
  `id_kelompok` int(3) NOT NULL,
  `kelompok` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelompok`
--

INSERT INTO `kelompok` (`id_kelompok`, `kelompok`) VALUES
(1, '1. Kucing'),
(2, '2. Kuda'),
(3, '3. Kambing'),
(4, '4. Gajah'),
(5, '5. Kerbau'),
(6, '6. Beruang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelompok_fk`
--

CREATE TABLE `kelompok_fk` (
  `id` int(11) NOT NULL,
  `namaKelompok` text,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelompok_fk`
--

INSERT INTO `kelompok_fk` (`id`, `namaKelompok`, `idFk`) VALUES
(1, 'humaniora 1', 3),
(2, 'humaniora 2', 3),
(3, 'Saintek 1', 6),
(5, 'Saintek 2', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kotak_masuk`
--

CREATE TABLE `kotak_masuk` (
  `id_kotakmasuk` int(11) NOT NULL,
  `id_tugas` int(11) NOT NULL,
  `nis` text,
  `nama_siswa` text,
  `jurusan` int(30) NOT NULL,
  `tahun_angkatan` varchar(30) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_file` text,
  `judul` text NOT NULL,
  `judul_tugas` text NOT NULL,
  `pokok_1` text NOT NULL,
  `nilai` int(3) NOT NULL COMMENT 'Nilai antara 1-100',
  `keterangan` int(3) NOT NULL COMMENT '0 = Belum_verifikasi ; 1 = verifikasi',
  `status_tugas` int(11) DEFAULT '0' COMMENT '0 = Universitas; selain itu untuk fakultas',
  `id_kelompok` int(12) DEFAULT NULL,
  `nama_guru` text,
  `idKelompokFk` int(11) DEFAULT NULL,
  `idPendampungFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kotak_masuk`
--

INSERT INTO `kotak_masuk` (`id_kotakmasuk`, `id_tugas`, `nis`, `nama_siswa`, `jurusan`, `tahun_angkatan`, `tanggal`, `m_file`, `judul`, `judul_tugas`, `pokok_1`, `nilai`, `keterangan`, `status_tugas`, `id_kelompok`, `nama_guru`, `idKelompokFk`, `idPendampungFk`) VALUES
(163, 70, '1465001', 'fauzi', 2, '2018', '2018-08-26 07:42:48', NULL, '', 'universitas2', 'universitas2', 10, 1, 0, 1, '7', NULL, NULL),
(164, 69, '1465001', 'fauzi', 2, '2018', '2018-08-26 07:43:27', NULL, '', 'universitas1', 'universitas1', 10, 1, 0, 1, '7', NULL, NULL),
(165, 70, '1465002', 'bayu', 1, '2018', '2018-08-26 07:44:10', NULL, '', 'universitas2', 'universitas2', 10, 1, 0, 1, '7', NULL, NULL),
(166, 69, '1465002', 'bayu', 1, '2018', '2018-08-26 07:44:24', NULL, '', 'universitas1', 'universitas1', 10, 1, 0, 1, '7', NULL, NULL),
(167, 69, '1465003', 'muslim', 4, '2018', '2018-08-26 07:45:15', NULL, '', 'universitas1', '', 10, 1, 0, 2, '8', NULL, NULL),
(168, 70, '1465004', 'burhan', 4, '2017', '2018-08-26 07:46:17', NULL, '', 'universitas2', 'universitas2', 10, 1, 0, 2, '8', NULL, NULL),
(169, 70, '1465005', 'galang', 1, '2017', '2018-08-26 07:46:50', NULL, '', 'universitas2', 'universitas2', 10, 0, 0, 3, '9', NULL, NULL),
(170, 73, '1465003', 'muslim', 3, '2018', '2018-08-26 08:34:24', NULL, '', 'humaniora2', 'humaniora2', 14, 1, 3, NULL, NULL, 1, 109),
(171, 72, '1465003', 'muslim', 3, '2018', '2018-08-26 08:34:41', NULL, '', 'humaniora1', 'humaniora1', 13, 1, 3, NULL, NULL, 1, 109),
(172, 73, '1465004', 'burhan', 3, '2017', '2018-08-26 08:35:34', NULL, '', 'humaniora2', 'humaniora2', 14, 1, 3, NULL, NULL, 2, 110),
(173, 72, '1465004', 'burhan', 3, '2017', '2018-08-26 08:35:50', NULL, '', 'humaniora1', 'humaniora1', 13, 1, 3, NULL, NULL, 2, 110),
(174, 74, '1465001', 'fauzi', 2, '2018', '2018-08-26 08:48:32', NULL, '', 'saintek1', 'saintek1', 12, 0, 6, NULL, NULL, 3, 107),
(175, 74, '1465002', 'bayu', 1, '2018', '2018-08-26 08:49:00', NULL, '', 'saintek1', 'saintek 1', 12, 1, 6, NULL, NULL, 5, 108);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kotak_masuk_upload`
--

CREATE TABLE `kotak_masuk_upload` (
  `id` int(11) NOT NULL,
  `nim` int(11) DEFAULT NULL,
  `id_tugas` int(11) DEFAULT NULL,
  `title` text,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kotak_masuk_upload`
--

INSERT INTO `kotak_masuk_upload` (`id`, `nim`, `id_tugas`, `title`, `token`) VALUES
(9, 1465001, 70, 'Selection_001.png', '0.25278939595416294'),
(10, 1465001, 69, 'Selection_002.png', '0.5128322448448417'),
(11, 1465002, 70, 'Selection_003.png', '0.06310167603199357'),
(12, 1465002, 69, 'Selection_004.png', '0.45078244346519103'),
(13, 1465003, 69, 'Selection_005.png', '0.009411590563951577'),
(14, 1465004, 70, '33375371_1810781592550373_7214734493872029696_n.jpg', '0.9432793081617905'),
(15, 1465005, 70, '36660887_240285363454125_5712164189938647040_n.jpg', '0.6353498866831448'),
(16, 1465003, 73, 'Selection_0011.png', '0.717421352263178'),
(17, 1465003, 72, 'Selection_0021.png', '0.5153282870175422'),
(18, 1465004, 73, 'Selection_0012.png', '0.45550978985555624'),
(19, 1465004, 72, 'Selection_0022.png', '0.6698974727518658'),
(20, 1465001, 74, 'Selection_0013.png', '0.20822833592064915'),
(21, 1465002, 74, 'Selection_0023.png', '0.8536480872479739');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id`, `level`) VALUES
(1, 'Admin'),
(2, 'PenugasanUniversitas'),
(3, 'PendampingUniversitas'),
(4, 'PenugasanFakultas'),
(5, 'PendampingFakultas'),
(6, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nim` int(30) DEFAULT NULL,
  `nama` text,
  `tahun` tinytext,
  `jenis_kelamin` tinytext,
  `foto` text,
  `idJurusan` int(11) DEFAULT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `idKelompokU` int(11) DEFAULT '0',
  `idPendampungU` int(11) DEFAULT '0',
  `idKelompokFk` int(11) DEFAULT '0',
  `idPendampungFk` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nim`, `nama`, `tahun`, `jenis_kelamin`, `foto`, `idJurusan`, `idAkses`, `idKelompokU`, `idPendampungU`, `idKelompokFk`, `idPendampungFk`) VALUES
(35, 1465001, 'fauzi', '2018', 'Laki - Laki', '33375371_1810781592550373_7214734493872029696_n.jpg', 2, 60, 1, 7, 3, 107),
(36, 1465002, 'bayu', '2018', 'Laki - Laki', '36660887_240285363454125_5712164189938647040_n.jpg', 1, 61, 1, 7, 5, 108),
(37, 1465003, 'muslim', '2018', 'Perempuan', 'IMG-20180717-WA0006.jpg', 3, 62, 2, 8, 1, 109),
(38, 1465004, 'burhan', '2017', 'Laki - Laki', 'itsnasyahadah-20180716-0001.jpg', 3, 63, 2, 8, 2, 110),
(39, 1465005, 'galang', '2017', 'Perempuan', 'iwan.jpg', 1, 64, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendamping_fk`
--

CREATE TABLE `pendamping_fk` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `nama` tinytext,
  `idKelompokFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendamping_fk`
--

INSERT INTO `pendamping_fk` (`id`, `idAkses`, `nama`, `idKelompokFk`) VALUES
(107, 73, 'fakultas saintek', 3),
(108, 74, 'fakultas saintek', 5),
(109, 75, 'fakultas humaniora', 1),
(110, 76, 'fakultas humaniora', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendamping_univ`
--

CREATE TABLE `pendamping_univ` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `nama` tinytext NOT NULL,
  `idKelompok` int(12) DEFAULT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendamping_univ`
--

INSERT INTO `pendamping_univ` (`id`, `idAkses`, `nama`, `idKelompok`, `foto`) VALUES
(7, 67, 'Misbahul Munir Setiawan', 1, 'Selection_006.png'),
(8, 68, 'Fahmi Ferdiansyah', 2, 'Selection_005.png'),
(9, 69, 'Imdad Rabbani', 3, 'Selection_004.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penugasan_fk`
--

CREATE TABLE `penugasan_fk` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penugasan_fk`
--

INSERT INTO `penugasan_fk` (`id`, `idAkses`, `idFk`) VALUES
(9, 71, 6),
(10, 72, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penugasan_univ`
--

CREATE TABLE `penugasan_univ` (
  `id` int(11) NOT NULL,
  `idAkses` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penugasan_univ`
--

INSERT INTO `penugasan_univ` (`id`, `idAkses`) VALUES
(6, 66);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting_sistem`
--

CREATE TABLE `setting_sistem` (
  `id` int(11) NOT NULL,
  `fitur` text NOT NULL,
  `status` int(3) NOT NULL COMMENT '0 = Tidak Aktif ; 1 = Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setting_sistem`
--

INSERT INTO `setting_sistem` (`id`, `fitur`, `status`) VALUES
(1, 'Tambah Anggota Kelompok (Laman Pendamping - UNIVERSITAS)', 1),
(2, 'Tambah Anggota Kelompok (Laman Pendamping - FAKULTAS)', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun`
--

CREATE TABLE `tahun` (
  `id_tahun` int(3) NOT NULL,
  `tahun` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun`
--

INSERT INTO `tahun` (`id_tahun`, `tahun`) VALUES
(1, '2017'),
(3, '2018');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tugas`
--

CREATE TABLE `tbl_tugas` (
  `id_tugas` int(23) NOT NULL,
  `judul` text,
  `tgl` datetime DEFAULT NULL,
  `m_file` text,
  `berlaku` datetime DEFAULT NULL,
  `nilai` int(3) NOT NULL,
  `keterangan` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_tugas`
--

INSERT INTO `tbl_tugas` (`id_tugas`, `judul`, `tgl`, `m_file`, `berlaku`, `nilai`, `keterangan`) VALUES
(69, 'universitas1', '2018-08-26 14:38:02', NULL, '2018-08-27 14:38:15', 10, 0),
(70, 'universitas2', '2018-08-26 14:38:34', NULL, '2018-08-28 14:38:37', 10, 0),
(72, 'humaniora1', '2018-08-26 15:32:05', NULL, '2018-08-27 15:32:07', 13, 3),
(73, 'humaniora2', '2018-08-26 15:32:21', NULL, '2018-08-27 15:32:23', 14, 3),
(74, 'saintek1', '2018-08-07 15:45:14', NULL, '2018-08-29 15:45:15', 12, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idLevel` (`idLevel`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hp`
--
ALTER TABLE `hp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`),
  ADD KEY `idFk` (`idFk`);

--
-- Indexes for table `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indexes for table `kelompok_fk`
--
ALTER TABLE `kelompok_fk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idFk` (`idFk`);

--
-- Indexes for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  ADD PRIMARY KEY (`id_kotakmasuk`);

--
-- Indexes for table `kotak_masuk_upload`
--
ALTER TABLE `kotak_masuk_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idJurusan` (`idJurusan`),
  ADD KEY `idAkses` (`idAkses`);

--
-- Indexes for table `pendamping_fk`
--
ALTER TABLE `pendamping_fk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAkses` (`idAkses`),
  ADD KEY `pendamping_fk_ibfk_1` (`idKelompokFk`);

--
-- Indexes for table `pendamping_univ`
--
ALTER TABLE `pendamping_univ`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAkses` (`idAkses`);

--
-- Indexes for table `penugasan_fk`
--
ALTER TABLE `penugasan_fk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penugasan_fk_ibfk_1` (`idAkses`),
  ADD KEY `idFk` (`idFk`);

--
-- Indexes for table `penugasan_univ`
--
ALTER TABLE `penugasan_univ`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idAkses` (`idAkses`);

--
-- Indexes for table `setting_sistem`
--
ALTER TABLE `setting_sistem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tahun`
--
ALTER TABLE `tahun`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  ADD PRIMARY KEY (`id_tugas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hp`
--
ALTER TABLE `hp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `id_kelompok` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `kelompok_fk`
--
ALTER TABLE `kelompok_fk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kotak_masuk`
--
ALTER TABLE `kotak_masuk`
  MODIFY `id_kotakmasuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT for table `kotak_masuk_upload`
--
ALTER TABLE `kotak_masuk_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `pendamping_fk`
--
ALTER TABLE `pendamping_fk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `pendamping_univ`
--
ALTER TABLE `pendamping_univ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `penugasan_fk`
--
ALTER TABLE `penugasan_fk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `penugasan_univ`
--
ALTER TABLE `penugasan_univ`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `setting_sistem`
--
ALTER TABLE `setting_sistem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tahun`
--
ALTER TABLE `tahun`
  MODIFY `id_tahun` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_tugas`
--
ALTER TABLE `tbl_tugas`
  MODIFY `id_tugas` int(23) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD CONSTRAINT `akses_ibfk_1` FOREIGN KEY (`idLevel`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `jurusan_ibfk_1` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelompok_fk`
--
ALTER TABLE `kelompok_fk`
  ADD CONSTRAINT `kelompok_fk_ibfk_1` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pendamping_fk`
--
ALTER TABLE `pendamping_fk`
  ADD CONSTRAINT `pendamping_fk_ibfk_1` FOREIGN KEY (`idKelompokFk`) REFERENCES `kelompok_fk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendamping_fk_ibfk_2` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pendamping_univ`
--
ALTER TABLE `pendamping_univ`
  ADD CONSTRAINT `pendamping_univ_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penugasan_fk`
--
ALTER TABLE `penugasan_fk`
  ADD CONSTRAINT `penugasan_fk_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `penugasan_fk_ibfk_2` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penugasan_univ`
--
ALTER TABLE `penugasan_univ`
  ADD CONSTRAINT `penugasan_univ_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
